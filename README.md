## README

[![build status](http://gitlab.it-machine.ru/darenta/Darenta-Rails/badges/master/build.svg)](http://gitlab.it-machine.ru/darenta/Darenta-Rails/commits/master)

### Как поднять рабочее окружение
```
git clone git@gitlab.it-machine.ru:darenta/Darenta-Rails.git darenta_backend
cd darenta_backend

docker-compose build
docker-compose run web rails db:create db:migrate db:seed
docker-compose up # после запуска приложение доступно по адресу http://localhost:3000
```

Вход в админку - http://localhost:3000/admin (email: admin@darenta.ru, password: 12345678)

Для корректной работы необходим файл /config/secrets.yml (необходимо запрашивать лично).
