//= require jquery2
//= require jquery_ujs
//= require admin/lib/perfect-scrollbar.jquery.min
//= require admin/main
//= require admin/lib/bootstrap.min
//= require admin/lib/jquery.dataTables.min
//= require admin/lib/dataTables.bootstrap.min
//= require admin/lib/buttons/dataTables.buttons
//= require admin/lib/buttons/buttons.html5
//= require admin/lib/buttons/buttons.flash
//= require admin/lib/buttons/buttons.print
//= require admin/lib/buttons/buttons.colVis
//= require admin/lib/buttons/buttons.bootstrap
//= require admin/lib/select2.min
//= require admin/index

//= require_tree ./datatables
