jQuery(document).ready(function() {
  $('select').select2();

  // flash info closer
  let flashCloseBtn = document.getElementById('flashCloseBtn');
  if (flashCloseBtn) {
    flashCloseBtn.addEventListener('click', function (e) {
      e.target.closest('div.flash-header').remove();
    })
  }
});
