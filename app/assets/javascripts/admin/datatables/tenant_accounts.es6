jQuery(document).ready(function() {
    let tenant_accounts_table = $('#tenant_accounts_table');

    $.extend( true, $.fn.dataTable.defaults, {
        dom:
        "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row be-datatable-body'<'col-sm-12'tr>>" +
        "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    } );

    tenant_accounts_table.dataTable({
        aoColumnDefs: [
            { aTargets: [0], bSortable: true  },
            { aTargets: [1], bSortable: true  },
            { aTargets: [2], bSortable: true  },
            { aTargets: [3], bSortable: true  },
            { aTargets: [4], bSortable: false }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": tenant_accounts_table.data('source'),
        "pagingType": "full_numbers",
        "columns": [
            { "data": "id" },
            { "data": "uid" },
            { "data": "provider" },
            { "data": "tenant_id" },
            { "data": "manage" }
        ],
        "lengthMenu": [ 25, 50, 100, 500 ],
        "order": [[ 0, "desc" ]]
    });
});
