jQuery(document).ready(function() {
  let bodies_table = $('#bodies_table');

  $.extend( true, $.fn.dataTable.defaults, {
    dom:
    "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
    "<'row be-datatable-body'<'col-sm-12'tr>>" +
    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
  } );

  bodies_table.dataTable({
    "lengthMenu": [ 25, 50, 100, 500 ],
    "order": [[ 0, "desc" ]]
  });
});
