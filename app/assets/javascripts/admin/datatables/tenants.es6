jQuery(document).ready(function() {
    let tenants_table = $('#tenants_table');

    $.extend( true, $.fn.dataTable.defaults, {
        dom:
        "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row be-datatable-body'<'col-sm-12'tr>>" +
        "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    } );

    tenants_table.dataTable({
        aoColumnDefs: [
            { aTargets: [0], bSortable: true  },
            { aTargets: [1], bSortable: false },
            { aTargets: [2], bSortable: false },
            { aTargets: [3], bSortable: false },
            { aTargets: [4], bSortable: true  },
            { aTargets: [5], bSortable: false },
            { aTargets: [6], bSortable: false },
            { aTargets: [7], bSortable: false },
            { aTargets: [8], bSortable: false }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": tenants_table.data('source'),
        "pagingType": "full_numbers",
        "columns": [
            { "data": "id" },
            { "data": "t_first_name" },
            { "data": "t_last_name" },
            { "data": "emails_with_provider" },
            { "data": "locale" },
            { "data": "created_at" },
            { "data": "updated_at" },
            { "data": "last_sign_in" },
            { "data": "manage" }
        ],
        "lengthMenu": [ 25, 50, 100, 500 ],
        "order": [[ 0, "desc" ]]
    });
});
