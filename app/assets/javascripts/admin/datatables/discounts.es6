jQuery(document).ready(function() {
    let discounts_table = $('#discounts_table');

    $.extend( true, $.fn.dataTable.defaults, {
        dom:
        "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
        "<'row be-datatable-body'<'col-sm-12'tr>>" +
        "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    } );

    discounts_table.dataTable({
        aoColumnDefs: [
            { aTargets: [0], bSortable: true  },
            { aTargets: [1], bSortable: true  },
            { aTargets: [2], bSortable: false  },
            { aTargets: [3], bSortable: false }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": discounts_table.data('source'),
        "pagingType": "full_numbers",
        "columns": [
            { "data": "id" },
            { "data": "type_id" },
            { "data": "value" },
            { "data": "manage" }
        ],
        "lengthMenu": [ 25, 50, 100, 500 ],
        "order": [[ 0, "desc" ]]
    });
});
