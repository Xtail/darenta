jQuery(document).ready(function() {
  let acriss_code_table = $('#acriss_codes_table');

  $.extend( true, $.fn.dataTable.defaults, {
    dom:
    "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
    "<'row be-datatable-body'<'col-sm-12'tr>>" +
    "<'row be-datatable-footer'<'col-sm-6'i><'col-sm-6'p>>"
  } );

  acriss_code_table.dataTable({
    aoColumnDefs: [
      { aTargets: [0], bSortable: true },
      { aTargets: [1], bSortable: true },
      { aTargets: [2], bSortable: true },
      { aTargets: [3], bSortable: true },
      { aTargets: [4], bSortable: false }
    ],
    "processing": true,
    "serverSide": true,
    "ajax": acriss_code_table.data('source'),
    "pagingType": "full_numbers",
    "columns": [
      { "data": "id" },
      { "data": "acriss_code" },
      { "data": "group" },
      { "data": "type_of_car" },
      { "data": "manage" }
    ],
    "lengthMenu": [ 25, 50, 100, 500 ],
    "order": [[ 0, "desc" ]]
  });
});
