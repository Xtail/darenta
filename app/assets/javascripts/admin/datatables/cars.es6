jQuery(document).ready(function() {
  let cars_table = $('#cars_table');

  $.extend( true, $.fn.dataTable.defaults, {
    dom:
    "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
    "<'row be-datatable-body'<'col-sm-12'tr>>" +
    "<'row be-datatable-footer'<'col-sm-6'i><'col-sm-6'p>>"
  } );

  cars_table.dataTable({
    aoColumnDefs: [
      { aTargets: [0], bSortable: true },
      { aTargets: [1], bSortable: true },
      { aTargets: [2], bSortable: true },
      { aTargets: [3], bSortable: true },
      { aTargets: [4], bSortable: true },
      { aTargets: [5], bSortable: true },
      { aTargets: [6], bSortable: true },
      { aTargets: [7], bSortable: true },
      { aTargets: [8], bSortable: true },
      { aTargets: [9], bSortable: true },
      { aTargets: [10], bSortable: false }
    ],
    "processing": true,
    "serverSide": true,
    "ajax": cars_table.data('source'),
    "pagingType": "full_numbers",
    "columns": [
      { "data": "id" },
      { "data": "full_name" },
      { "data": "is_moderated" },
      { "data": "price_rub" },
      { "data": "price_usd" },
      { "data": "price_krw" },
      { "data": "price_eur" },
      { "data": "is_baby_chair" },
      { "data": "tenant_info" },
      { "data": "updated_at" },
      { "data": "manage" }
    ],
    "lengthMenu": [ 25, 50, 100, 500 ],
    "order": [[ 0, "desc" ]]
  });
});
