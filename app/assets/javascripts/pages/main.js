var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('personal'),
    'padding': 300,
    'tolerance': 70
});
document.getElementById("menu-toggle").addEventListener('click', function() {
    slideout.toggle();
});

$(function () {
   $('#unauthorized_order').click(function () {
       $.magnificPopup.open({
           items: {
               src: '#auth',
               type: 'inline'
           }
       });
       return false;
   });

    /*$('#authorized_order').click(function () {
        $.magnificPopup.open({
            items: {
                src: '#success',
                type: 'inline'
            }
        });
    });*/

    $.fn.datepicker.language['ru'] =  {
        days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
        daysShort: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
        daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        today: 'Сегодня',
        clear: 'Очистить',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    };

    $('.date-input').datepicker({
        dateFormat: "D, d M",
        minDate: new Date(),
        onSelect: function onSelect(fd, date, picker) {
            $(picker.$el).data('date', date.toString());
            $(picker.$el).children('span').text(fd);
        }
    });


    $('.date-input').click(function () {
       $(this).find('.datepicker-inline').show();
    });

    $('.text-insert').click(function () {
        $(this).find('.text-input').show();
    });

    $(document).click(function () {
        if( $(event.target).closest(".date-input").length )
            return;

        $('.datepicker-inline').hide();

        if( $(event.target).closest(".text-insert").length )
            return;

        $('.text-input').hide();
    });

    $('.text-insert button').click(function () {
        var el = $("#"+$(this).data('href'));
        $(this).parents('.text-insert').children('span').text(el.val());
    });
});