class CarDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "Car.id", cond: :eq },
      full_name: { source: "Car.full_name", cond: :like },
      is_moderated: { source: "Car.is_moderated", cond: :like },
      price_rub: { source: "Car.price_rub", cond: :like },
      price_usd: { source: "Car.price_usd", cond: :like },
      price_krw: { source: "Car.price_krw", cond: :like },
      price_eur: { source: "Car.price_eur", cond: :like },
      is_baby_chair: { source: "Car.is_baby_chair", cond: :like },
      tenant_info: { source: "Car.tenant.id", cond: :like },
      updated_at: { source: "Car.updated_at", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::CarDecorator.new(record)

      {
        id: record.id,
        full_name: record.full_name,
        is_moderated: record.check_boolean('is_moderated'),
        price_rub: record.price_rub,
        price_usd: record.price_usd,
        price_krw: record.price_krw,
        price_eur: record.price_eur,
        is_baby_chair: record.check_boolean('is_baby_chair'),
        tenant_info: record.tenant.name_with_email_and_provider,
        updated_at: record.updated_at,
        manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    Car.includes(:tenant).references(:tenant)
  end
end
