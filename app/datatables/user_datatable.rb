class UserDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "User.id", cond: :eq },
      email: { source: "User.email", cond: :like },
      full_name: { source: "UserTranslation.full_name", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::UserDecorator.new(record)
      {
        id: record.id,
        email: record.email,
        full_name: record.full_name,
        manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    User.with_translations
  end
end
