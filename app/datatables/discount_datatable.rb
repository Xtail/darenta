class DiscountDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "Discount.id", cond: :eq },
        type_id: { source: "Discount.type_id", cond: :eq },
        value: { source: "Discount.value", cond: :like}
    }
  end

  def data
    records.map do |record|
      record = Admin::DiscountDecorator.new(record)
      {
          id: record.id,
          type_id: record.type_id,
          value: record.value,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    Discount
    # User.with_translations
  end
end