class OrderDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "Order.id", cond: :eq },
        tenant_full_name: { source: "Order.tenant_full_name", cond: :like },
        car_name: { source: "Order.car_name", cond: :like },
        price_value: { source: "Order.price_value", cond: :like },
        price_currency: { source: "Order.price_currency", cond: :like },
        source: { source: "Order.source", cond: :eq }
    }
  end

  def data
    records.map do |record|
      record = Admin::OrderDecorator.new(record)
      {
          id: record.id,
          tenant_full_name: record.tenant_full_name,
          car_name: record.car_name,
          price_value: record.price_value,
          price_currency: record.price_currency,
          source: record.source,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    Order
    # User.with_translations
  end
end