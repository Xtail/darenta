class PartnerOfficeDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "PartnerOffice.id", cond: :eq },
      location_id: { source: "PartnerOffice.location_id", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::PartnerOfficeDecorator.new(record)
      {
        id: record.id,
        location_id: record.location_id,
        manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    PartnerOffice
  end
end
