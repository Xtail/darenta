class DiscountTypeDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "DiscountType.id", cond: :eq },
        title: { source: "DiscountType.title", cond: :like}
    }
  end

  def data
    records.map do |record|
      record = Admin::DiscountTypeDecorator.new(record)
      {
          id: record.id,
          title: record.title,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    DiscountType
    # User.with_translations
  end
end