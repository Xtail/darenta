class AcrissCodeDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "AcrissCode.id", cond: :eq },
      acriss_code: { source: "AcrissCode.acriss_code", cond: :like },
      group: { source: "AcrissCode.group", cond: :eq },
      type_of_car: { source: "AcrissCode.type_of_car", cond: :eq },
    }
  end

  def data
    records.map do |record|
      record = Admin::AcrissCodeDecorator.new(record)
      {
        id: record.id,
        acriss_code: record.acriss_code,
        group: record.group,
        type_of_car: record.type_of_car,
        manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    AcrissCode
  end
end
