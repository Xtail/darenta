class TenantDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format

    @view_columns ||= {
        id: { source: "Tenant.id", cond: :eq },
        t_first_name: { source: "TenantTranslation.t_first_name", cond: :like },
        t_last_name: { source: "TenantTranslation.t_last_name", cond: :like },
        emails_with_provider: { source: "TenantAccount.email", cond: :like },
        locale: { source: "Tenant.locale", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::TenantDecorator.new(record)
      {
          id: record.id,
          t_first_name: record.t_first_name,
          t_last_name: record.t_last_name,
          emails_with_provider: record.emails_with_provider,
          locale: record.locale,
          created_at: record.created_at.to_s,
          updated_at: record.updated_at.to_s,
          last_sign_in: record.last_sign_in.to_s,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon} #{record.merge_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records  
    Tenant.select(
      "tenants.id,
      tenants.provider,
      tenants.created_at,
      tenants.updated_at,
      tenants.is_blocked,
      tenants.locale").joins(:tenant_translations).joins(:tenant_accounts).distinct
  end
end
