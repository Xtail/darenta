class SamplerDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "Sampler.id", cond: :eq },
        mark_id: { source: "Sampler.mark_id", cond: :eq },
        name: { source: "Sampler.name", cond: :eq }
    }
  end

  def data
    records.map do |record|
      record = Admin::SamplerDecorator.new(record)
      {
          id: record.id,
          mark_id: record.mark_id,
          name: record.name,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    Sampler
  end
end