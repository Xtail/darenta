class MailMessageDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "MailMessage.id", cond: :eq },
        title: { source: "MailMessage.title", cond: :like },
        key: { source: "MailMessage.key", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::MailMessageDecorator.new(record)
      {
          id: record.id,
          title: record.title,
          key: record.key,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    MailMessage
  end
end
