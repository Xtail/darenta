class TenantAccountDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "TenantAccount.id", cond: :eq },
        uid: { source: "TenantAccount.uid", cond: :like },
        provider: { source: "TenantAccount.provider", cond: :like },
        tenant_id: { source: "TenantAccount.tenant_id", cond: :like }
        # full_name: { source: "UserTranslation.full_name", cond: :like }
    }
  end

  def data
    records.map do |record|
      record = Admin::TenantAccountDecorator.new(record)
      {
          id: record.id,
          uid: record.uid,
          provider: record.provider,
          tenant_id: record.tenant_id,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    TenantAccount
  end
end
