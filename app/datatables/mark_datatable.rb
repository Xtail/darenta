class MarkDatatable < AjaxDatatablesRails::Base
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        id: { source: "Mark.id", cond: :eq },
        name: { source: "Mark.name", cond: :eq }
    }
  end

  def data
    records.map do |record|
      record = Admin::MarkDecorator.new(record)
      {
          id: record.id,
          name: record.name,
          manage: "#{record.edit_link_icon} #{record.destroy_link_icon}".html_safe
      }
    end
  end

  private

  def get_raw_records
    Mark
  end
end