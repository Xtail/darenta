require 'dry/transaction'

class Site::V1::Order::Create::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  VALIDATOR = Dry::Validation.Form do
    required(:car_id).filled(:int?)
    required(:customer_id).filled(:int?)
    required(:date_from).filled(format?: ISO_8601_REGEX)
    required(:date_to).filled(format?: ISO_8601_REGEX)
    optional(:where_to_pickup).maybe(:str?)
    optional(:where_to_leave).maybe(:str?)
    optional(:discount_type).maybe(:str?)
    optional(:discount_value).maybe(:str?)
    optional(:locale).value(included_in?: %w(ru en ko))
    optional(:is_paid).maybe(:bool?)
  end

  step :symbolize
  step :merge
  step :cast
  step :validate
  step :check_car
  step :check_landlord
  step :check_landlord_account
  step :check_customer_account
  step :calculate_price
  step :create_attributes
  step :create_order
  step :generate_tokens
  step :create_form

  def symbolize(params_raw)
    Success params_raw.symbolize_keys
  end

  def merge(params_raw)
    order = params_raw[:order].to_h

    params = {
        car_id: params_raw[:car_id],
        customer_id: params_raw[:current_tenant_id],
        date_from: order['date_from'],
        date_to: order['date_to'],
        where_to_pickup: order['where_to_pickup'],
        where_to_leave: order['where_to_leave'],
        discount_type: nil,
        discount_value: '',
        locale: 'ru',
        is_paid: false
    }

    Success(params)
  end

  def cast(params)
    Success Site::V1::Order::Create::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_car(hash)
    ::Car.exists?(id: hash[:car_id]) ? Success(hash) : Failure({ car_id: ['not present in database'] })
  end

  def check_landlord(hash)
    car = ::Car.find_by(id: hash[:car_id])
    ::Tenant.exists?(id: car.tenant_id) ? Success(hash) : Failure({ landlord: ['not present in database'] })
  end

  def check_landlord_account(hash)
    car = ::Car.find_by(id: hash[:car_id])
    ::TenantAccount.exists?(tenant_id: car.tenant_id) ? Success(hash) : Failure({ landlord_account: ['not present in database'] })
  end

  def check_customer_account(hash)
    ::TenantAccount.exists?(tenant_id: hash[:customer_id]) ? Success(hash) : Failure({ customer_account: ['not present in database'] })
  end

  def calculate_price(hash)
    hash = hash.to_h

    car = ::Car.find_by(id: hash[:car_id])

    unless car.price_usd.present?
      return Failure({ car_price_usd: ['not present in database'] })
    end

    date_from = hash[:date_from]
    date_to = hash[:date_to]

    car_price = car.daily_price_by_dates(date_from, date_to, 'rub')
    price = ::Order.new.calculate_comission(car_price, date_from, date_to)

    hash[:car_price] = car_price
    hash[:price_value] = price
    hash[:price_currency] = 'rub'

    Success(hash)
  end

  def create_attributes(hash)
    car = ::Car.find_by(id: hash[:car_id])
    landlord = ::Tenant.find_by(id: car.tenant_id)
    landlord_accounts = ::TenantAccount.where(tenant_id: landlord.id)
    landlord_emails = []
    landlord_accounts.each do |account|
      landlord_emails << account.email
    end

    customer = ::Tenant.find_by(id: hash[:customer_id])
    customer_accounts = ::TenantAccount.where(tenant_id: customer.id)
    customer_emails = []
    customer_accounts.each do |account|
      customer_emails << account.email
    end

    attributes = {
        car_id: car.id,
        car_name: car.full_name,
        car_price: hash[:car_price],
        landlord_id: landlord.id,
        tenant_id: customer.id,
        tenant_first_name: customer.first_name,
        tenant_last_name: customer.last_name,
        tenant_full_name: customer.full_name,
        tenant_accounts: customer_emails.to_s,
        tenant_phone: customer.telephone,
        price_value: hash[:price_value],
        price_currency: hash[:price_currency],
        is_paid: hash[:is_paid],
        discount_id: nil,
        rent_from: hash[:date_from],
        rent_til: hash[:date_to],
        where_to_pickup: hash[:where_to_pickup],
        where_to_leave: hash[:where_to_leave],
        source: 'site',
        locale: 'en'
    }

    hash[:attributes] = attributes
    Success(hash)
  end

  def create_order(hash)
    order = ::Order.new(hash[:attributes])
    unless order.save
      Failure({ order: ['not saved'] })
    end

    hash[:order_id] = order.id

    Success(hash)
  end

  def generate_tokens(hash)
    order = ::Order.find_by(id: hash[:order_id])

    car = ::Car.find_by(id: order.car_id)
    landlord = ::Tenant.find_by(id: car.tenant_id)
    landlord_emails = landlord.emails

    order.accept_token = ::Order.new.generate_accept_token(landlord_emails.to_s)
    order.decline_token = ::Order.new.generate_decline_token(landlord_emails.to_s)
    order.save

    Success(hash)
  end

  def create_form(hash)
    order = ::Order.find_by(id: hash[:order_id])

    car = ::Car.find_by(id: order.car_id)
    tenant = ::Tenant.find_by(id: order.tenant_id)
    landlord = ::Tenant.find_by(id: order.landlord_id)

    form = {
        order: order,
        car: car,
        tenant: tenant,
        landlord: landlord,
        locale: order.locale
    }

    hash[:form] = form

    Success(hash)
  end
end
