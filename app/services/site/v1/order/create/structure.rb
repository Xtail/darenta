require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Site::V1::Order::Create::Structure < Dry::Struct
  constructor_type :schema

  attribute :car_id, Types::Coercible::Int.optional
  attribute :customer_id, Types::Coercible::Int.optional
  attribute :date_from, Types::Coercible::String.optional
  attribute :date_to, Types::Coercible::String.optional
  attribute :where_to_pickup, Types::Coercible::String.optional
  attribute :where_to_leave, Types::Coercible::String.optional
  attribute :discount_type, Types::Coercible::String.optional
  attribute :discount_value, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.default('ru')
  attribute :is_paid, Types::Form::Bool.optional
end