require 'dry/transaction'

class Api::V1::Tenant::UpdateCar2::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:car_id).filled(:int?)

    optional(:city_name).maybe(:str?)
    optional(:engine_id).maybe(:int?)
    optional(:color_id).maybe(:int?)
    optional(:transmission_id).maybe(:int?)
    optional(:body_id).maybe(:int?)

    optional(:mark_name).maybe(:str?)
    optional(:sampler_name).maybe(:str?)
    optional(:production_year).maybe(:int?)

    optional(:address).maybe(:str?)
    optional(:link_to_car).maybe(:str?)
    optional(:is_delivery_to_airport).maybe(:bool?)
    optional(:is_delivery_to_you).maybe(:bool?)
    optional(:is_baby_chair).maybe(:bool?)
    optional(:is_wifi).maybe(:bool?)
    optional(:is_navigator).maybe(:bool?)
    optional(:is_current_offer).maybe(:bool?)

    optional(:latitude).maybe(:float?)
    optional(:longitude).maybe(:float?)

    optional(:price).maybe(:float?)
    optional(:price_few_days).maybe(:float?)
    optional(:price_week).maybe(:float?)
    optional(:price_month).maybe(:float?)
    optional(:deposit).maybe(:float?)
    optional(:currency).maybe(:str?)

    optional(:image_ids).maybe(:str?)
    optional(:contract_image_ids).maybe(:str?)
    optional(:insurances_json).maybe(:str?)
    optional(:comments).maybe(:str?)
  end

  step :symbolize
  step :merge_defaults
  step :cast
  step :validate
  step :check_car
  step :check_color
  step :check_engine
  step :check_transmission
  step :check_city
  step :create_city
  step :check_body
  step :resolve_images
  step :resolve_contracts
  step :resolve_insurances
  step :create_attributes
  step :update_car
  step :mail_to_tenant
  step :mail_to_admin

  def symbolize(params)
    Success params.symbolize_keys
  end

  def merge_defaults(params)
    #params[:locale] = 'ru' unless params[:locale].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Tenant::UpdateCar2::Structure.new(params).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_car(hash)
    ::Car.exists?(id: hash[:car_id]) ? Success(hash) : Failure({ car_id: ['not present in database'] })
  end

  def check_color(hash)
    hash[:color_id].present? ? ::Color.exists?(id: hash[:color_id]) ? Success(hash) : Failure({ color_id: ['not present in database'] }) : Success(hash)
  end

  def check_engine(hash)
    hash[:engine_id].present? ? ::Engine.exists?(id: hash[:engine_id]) ? Success(hash) : Failure({ engine_id: ['not present in database'] }) : Success(hash)
  end

  def check_transmission(hash)
    hash[:transmission_id].present? ? ::Transmission.exists?(id: hash[:transmission_id]) ? Success(hash) : Failure({ transmission_id: ['not present in database'] }) : Success(hash)
  end

  def check_city(hash)
    unless hash[:city_name].present?
      Success(hash)
    end

    hash = hash.to_h

    city_name = hash[:city_name]
    city_id = nil

    translation = ::CityTranslation.find_by(name: city_name)
    if translation.present?
      city_id = translation.city_id
    end

    hash[:city_id] = city_id
    Success(hash)
  end

  def create_city(hash)
    return Success(hash) if hash[:city_id].present?

    attributes = {}

    I18n.available_locales.each do |l|
      attributes["name_#{l}"] = hash[:city_name]
    end

    city = ::City.create(attributes)

    hash[:city_id] = city.id
    Success(hash)
  end

  def check_body(hash)
    hash[:body_id].present? ? ::Body.exists?(id: hash[:body_id]) ? Success(hash) : Failure({ body_id: ['not present in database'] }) : Success(hash)
  end

  def resolve_images(hash)
    raw_image_ids = hash[:image_ids]
    hash.delete(:image_ids)

    if raw_image_ids.present?
      image_ids_json = ::Resource.new.ids_to_json_array(raw_image_ids)
      if image_ids_json.present?
        hash[:images_ids] = image_ids_json
      end
    end

    Success(hash)
  end

  def resolve_contracts(hash)
    raw_contract_image_ids = hash[:contract_image_ids]
    hash.delete(:contract_image_ids)

    if raw_contract_image_ids.present?
      contract_image_ids_json = ::Resource.new.ids_to_json_array(raw_contract_image_ids)
      if contract_image_ids_json.present?
        hash[:contract_images_ids] = contract_image_ids_json
      end
    end

    Success(hash)
  end

  def resolve_insurances(hash)
    insurances = []

    json = hash[:insurances_json]
    hash.delete(:insurances_json)

    if json.present?
      begin
        json.gsub! "=>", ":"
        parsed_json = JSON.parse json
        parsed_json.each do |t|
          insurance = {
            name: t["name"],
            images_ids: t["img_ids"]
          }
          insurances << insurance
        end

        hash[:insurances] = insurances
      rescue
        return Failure({ insurances_json: ['invalid format'] })
      end
    end
    Success(hash)
  end

  def create_attributes(hash)
    car = ::Car.find_by(id: hash[:car_id])
    return Failure({ car: ['not found'] }) unless car.present?

    car_attributes = {
        is_delivery_to_airport: hash[:is_delivery_to_airport] || car.is_delivery_to_airport,
        is_delivery_to_you: hash[:is_delivery_to_you] || car.is_delivery_to_you,
        is_baby_chair: hash[:is_baby_chair] || car.is_baby_chair,
        is_wifi: hash[:is_wifi] || car.is_wifi,
        is_navigator: hash[:is_navigator] || car.is_navigator,
        color_id: hash[:color_id] || car.color_id,
        engine_id: hash[:engine_id] || car.engine_id,
        transmission_id: hash[:transmission_id] || car.transmission_id,
        city_id: hash[:city_id] || car.city_id,
        tenant_id: car.tenant_id,
        mark_name: hash[:mark_name] || car.mark_name,
        sampler_name: hash[:sampler_name] || car.sampler_name,
        body_id: hash[:body_id] || car.body_id,
        production_year: hash[:production_year] || car.production_year,
        is_current_offer: hash[:is_current_offer] || car.is_current_offer,
        address: hash[:address] || car.address,
        latitude: hash[:latitude] || car.latitude,
        longitude: hash[:longitude] || car.longitude,
        price_rub: hash[:price] || car.price_rub,
        price_usd: hash[:price] || car.price_usd,
        price_krw: hash[:price] || car.price_krw,
        price_eur: hash[:price] || car.price_eur,
        price_few_days_rub: hash[:price_few_days] || car.price_few_days_rub,
        price_few_days_usd: hash[:price_few_days] || car.price_few_days_usd,
        price_few_days_krw: hash[:price_few_days] || car.price_few_days_krw,
        price_few_days_eur: hash[:price_few_days] || car.price_few_days_eur,
        price_week_rub: hash[:price_week] || car.price_week_rub,
        price_week_usd: hash[:price_week] || car.price_week_usd,
        price_week_krw: hash[:price_week] || car.price_week_krw,
        price_week_eur: hash[:price_week] || car.price_week_eur,
        price_month_rub: hash[:price_month] || car.price_month_rub,
        price_month_usd: hash[:price_month] || car.price_month_usd,
        price_month_krw: hash[:price_month] || car.price_month_krw,
        price_month_eur: hash[:price_month] || car.price_month_eur,
        deposit_rub: hash[:deposit] || car.deposit_rub,
        deposit_usd: hash[:deposit] || car.deposit_usd,
        deposit_krw: hash[:deposit] || car.deposit_krw,
        deposit_eur: hash[:deposit] || car.deposit_eur,
        link_to_car: hash[:link_to_car] || car.link_to_car,
        images_ids: hash[:images_ids] || car.images_ids,
        contract_images_ids: hash[:contract_images_ids] || car.contract_images_ids,
        insurances: hash[:insurances] || car.insurances,
        comments: hash[:comments] || car.comments,
        is_moderated: false
    }

    result = {
        car_id: hash[:car_id],
        attributes: car_attributes
    }

    Success(result)
  end

  def update_car(hash)

    car = ::Car.find_by(id: hash[:car_id])
    return Failure({ car: ['not found'] }) unless car.present?
    car.update_attributes(hash[:attributes])
    car.save

    Success(car)
  end

  def mail_to_tenant(car)
    form = {
        car: car,
        source: 'api'
    }

    TenantsUpdateCarInfoJob.perform_later(form)

    Success(car)
  end

  def mail_to_admin(car)
    form = {
        car: car,
        moderation_request_at: DateTime.now.strftime('%d.%m.%Y %I:%M%p'),
        source: 'api'
    }

    AdminsTenantAddCarRequestJob.perform_later(form)

    Success(car)
  end
end
