class Api::V1::Tenant::Oauth::Providers::Email
  include HTTParty

  class << self
    def authenticate(access_token)
      pull_user_profile()
    end

    def pull_user_profile()
      #options = { query: { access_token: access_token, fields: 'id,email,first_name,last_name,picture.width(500)' } }
      #response = get('/me', options)

      #return nil unless response.success?
      #raw = response.parsed_response

      {
        oauth_uid: raw['id'],
        oauth_provider: 'email',
        oauth_email: raw['email'],
        oauth_first_name: raw['first_name'],
        oauth_last_name: raw['last_name'],
        oauth_avatar: raw.dig('picture', 'data', 'url')
      }
    end
  end
end
