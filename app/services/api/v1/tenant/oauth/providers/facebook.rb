require 'httparty'

class Api::V1::Tenant::Oauth::Providers::Facebook
  include HTTParty

  base_uri 'https://graph.facebook.com/v3.2'

  class << self
    def authenticate(access_token)
      pull_user_profile(access_token)
    end

    def pull_user_profile(access_token)
      options = { query: { access_token: access_token, fields: 'id,email,first_name,last_name,middle_name,gender,picture.width(500)' } }
      response = get('/me', options)

      return nil unless response.success?
      raw = response.parsed_response

      {
        oauth_uid: raw['id'],
        oauth_provider: 'facebook',
        oauth_email: raw['email'],
        oauth_first_name: raw['first_name'],
        oauth_last_name: raw['last_name'],
        oauth_middle_name: raw['middle_name'],
        oauth_gender: raw['gender'],
        oauth_avatar: raw.dig('picture', 'data', 'url')
      }
    end
  end
end
