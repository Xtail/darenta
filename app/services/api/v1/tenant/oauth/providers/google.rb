require 'httparty'

class Api::V1::Tenant::Oauth::Providers::Google
  include HTTParty

  base_uri 'https://www.googleapis.com/oauth2/v3'
  #base_uri 'https://www.googleapis.com/oauth2/v2'


  class << self
    def authenticate(id_token)
      pull_user_profile(id_token)
    end

    def pull_user_profile(id_token)
      options = { query: { id_token: id_token } }
      response = get('/tokeninfo', options)
      #options = { query: { key: Rails.application.secrets.google_api_key, alt:'json', access_token: id_token, fields: 'id,email,given_name,family_name,picture,gender' } }
      #response = get('/userinfo', options)

      return nil unless response.success?
      raw = response.parsed_response

      result = {
          oauth_uid: raw['email'],
          oauth_provider: 'google',
          oauth_email: raw['email'],
          oauth_first_name: raw['given_name'],
          oauth_last_name: raw['family_name'],
          oauth_gender: raw['gender'],
          oauth_avatar: raw['picture']
      }

      if !result[:oauth_first_name].present?
        result[:oauth_first_name] = 'given_name'
      end

      if !result[:oauth_last_name].present?
        result[:oauth_last_name] = 'family_name'
      end

      result
    end
  end
end
