class Api::V1::Tenant::Oauth::TokenManager
  class << self
    def generate(user_account, client_id)
      client_id ||= SecureRandom.urlsafe_base64(nil, false)
      user_account.create_new_auth_token(client_id)
    end
  end
end
