require 'dry/transaction'

class Api::V1::Tenant::Oauth::Interactor
  include Dry::Transaction

  AUTH_PROVIDERS = %w(facebook google)

  AVALIABLE_LOCALES = %w(ru en ko)

  AUTH_VALIDATOR = Dry::Validation.Form do
    required(:access_token).filled(:str?)
    required(:provider).value(included_in?: AUTH_PROVIDERS)
    optional(:email).filled(format?: ::Regex::Email.new.plain)
    optional(:app).maybe(:str?)
  end

  REG_VALIDATOR = Dry::Validation.Form do
    required(:uid).filled(:str?)
    required(:provider).filled(:str?)
    required(:email).filled(format?: ::Regex::Email.new.plain)
    optional(:darenta_uid).maybe(:str?)
    optional(:first_name).maybe(:str?)
    optional(:last_name).maybe(:str?)
    optional(:middle_name).maybe(:str?)
    optional(:telephone).maybe(:str?)
    optional(:gender).maybe(:str?)
    optional(:avatar).maybe(:str?)
    optional(:locale).value(included_in?: AVALIABLE_LOCALES)
    optional(:app).maybe(:str?)
  end

  ADD_SERVICE_VALIDATOR = Dry::Validation.Form do
    required(:uid).filled(:str?)
    required(:provider).filled(:str?)
    required(:email).filled(format?: ::Regex::Email.new.plain)
    required(:tenant_id).filled(:int?)
    optional(:app).maybe(:str?)
  end

  step :auth_validate
  step :call_oauth
  step :find_tenant
  step :add_service
  step :prepare_attrs
  step :register_user
  step :update_locale
  step :update_last_sign_in
  step :resend_confirmation

  def auth_validate(hash)
    result = AUTH_VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def call_oauth(hash)
    hash = hash.output.to_h.symbolize_keys
    provider = "Api::V1::Tenant::Oauth::Providers::#{hash[:provider].capitalize}".constantize
    profile = provider.authenticate(hash[:access_token])
    profile.present? ? Success(profile.merge(hash)) : Failure({ provider: ['not response'] })
  end

  def find_tenant(hash)
    if hash[:darenta_uid].present?
      tenant = find_tenant_by_darenta_uid(hash[:darenta_uid])
      if tenant.present?
        Success(tenant: tenant, attrs: hash)
      else
        account = find_account_by(hash[:oauth_uid])
        Success(account: account, attrs: hash)
      end
    else
      if hash[:darenta_token].present? && hash[:client].present?
        tenant = find_tenant_by_client_and_darenta_token(hash[:client], hash[:darenta_token])
        if tenant.present?
          Success(tenant: tenant, attrs: hash)
        else
          return Failure({ darenta_token: ['can not find tenant'] })
        end
      else
        account = find_account_by(hash[:oauth_uid])
        Success(account: account, attrs: hash)
      end
    end
  end

  def add_service(hash)
    return Success(hash) unless hash[:tenant].present?

    attributes = hash[:attrs]
    tenant = hash.delete(:tenant)
    account_attributes = {
        uid: attributes[:oauth_uid],
        provider: attributes[:oauth_provider],
        email: attributes[:oauth_email] || attributes[:email],
        tenant_id: tenant.id
    }

    result = ADD_SERVICE_VALIDATOR.call(account_attributes)
    return Failure(result.errors) unless result.success?

    account = ::TenantAccount.find_by(email: account_attributes[:email])
    if account.present?
      account.tenant_id = tenant.id
      account.save
    else
      account = ::TenantAccount.new(account_attributes)
      unless account.save
        return Failure({ tenant_account: ['can not create (add service)'] })
      end
      #TenantsAddServiceJob.perform_later(account.id)
    end

    Success(account: account, attrs: hash)
  end

  def prepare_attrs(hash)
    return Success(hash) if hash[:account].present?

    ::Rails.logger.debug "prepare_attrs #{hash}"

    raw = hash[:attrs]
    attributes = {
        uid: raw[:oauth_uid],
        provider: raw[:oauth_provider],
        email: raw[:email] || raw[:oauth_email],
        first_name: raw[:oauth_first_name] || raw[:first_name],
        last_name: raw[:oauth_last_name] || raw[:last_name],
        middle_name: raw[:oauth_middle_name] || raw[:middle_name],
        gender: raw[:oauth_gender] || raw[:gender],
        telephone: raw[:telephone],
        avatar: raw[:oauth_avatar],
        darenta_uid: raw[:darenta_uid],
        locale: raw[:locale],
        app: raw[:app]
    }

    Success(account: hash[:account], attrs: Api::V1::Tenant::Oauth::Structure.new(attributes).to_h)
  end

  def register_user(hash)

    return Success(hash) if hash[:account].present?

    ::Rails.logger.debug "register_user #{hash}"

    attrs = hash[:attrs]
    attrs[:locale] = 'ru' unless attrs[:locale].present?

    attributes = REG_VALIDATOR.call(attrs)
    return Failure(attributes.errors) unless attributes.success?

    attributes = attributes.output
    avatar_url = attributes.delete(:avatar)
    locale = attributes.delete(:locale)

    tenant_attributes = {
      t_first_name_ru: attributes[:first_name],
      t_last_name_ru: attributes[:last_name],
      t_middle_name_ru: attributes[:middle_name],
      t_first_name_en: attributes[:first_name],
      t_last_name_en: attributes[:last_name],
      t_middle_name_en: attributes[:middle_name],
      t_first_name_ko: attributes[:first_name],
      t_last_name_ko: attributes[:last_name],
      t_middle_name_ko: attributes[:middle_name],
      telephone: attributes[:telephone],
      gender_ru: attributes[:gender],
      gender_en: attributes[:gender],
      gender_ko: attributes[:gender],
      darenta_uid: attributes[:darenta_uid]
    }

    tenant = ::Tenant.new(tenant_attributes)
    unless tenant.save
      return Failure({ tenant: ['not created'] })
    end

    if locale.present?
      tenant.locale = locale
      tenant.save
    end

    if avatar_url.present?
      tenant.remote_avatar_url = avatar_url
      tenant.save
    end

    account_attributes = {
      tenant_id: tenant.id,
      uid: attributes[:uid],
      provider: attributes[:provider],
      email: attributes[:email]
    }

    account = ::TenantAccount.new(account_attributes)
    unless account.save
      tenant.delete
      return Failure({ account: ['email already exist'] })
    end

    form = {
        tenant_account: account,
        device: attrs[:app],
        source: 'api'
    }
    AdminsRegisterTenantJob.perform_later(form)
    hash[:account] = account

    Success(hash)
  end

  def update_locale(hash)
    account = hash[:account]
    attrs = hash[:attrs]

    if attrs[:locale].present?
      tenant = ::Tenant.find_by(id: account.tenant_id)
      tenant.locale = attrs[:locale]
      tenant.save
    end

    Success(account)
  end

  def update_last_sign_in(account)
    tenant = ::Tenant.find_by(id: account.tenant_id)
    tenant.update_last_sign_in

    Success(account)
  end

  def resend_confirmation(account)
    unless account.confirmed_at.present?
      tenant = ::Tenant.find_by(id: account.tenant_id)
      unless tenant.is_email_rejected
        account.confirmation_sent_from = 'api'
        account.save
        TenantsConfirmationJob.perform_later(account.id, 'api')
      end
    end

    Success(account)
  end

private
  def find_tenant_by_darenta_uid(darenta_uid)
    tenant = nil

    if darenta_uid.present? && darenta_uid != ""
      tenant = ::Tenant.find_by(darenta_uid: darenta_uid)
    end

    return tenant
  end

  def find_tenant_by_client_and_darenta_token(client, darenta_token)
    tenant = nil

    tenant_account = ::TenantAccount.new.find_by_client_and_token(client, darenta_token)
    if tenant_account.present?
      tenant = ::Tenant.find_by(id: tenant_account.tenant_id)
    end

    return tenant
  end

  def find_account_by(uid)
    tenant_account = ::TenantAccount.find_by(uid: uid)
    return tenant_account
  end
end
