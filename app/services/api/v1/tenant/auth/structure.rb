require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::Auth::Structure < Dry::Struct
  constructor_type :schema

  attribute :uid, Types::Coercible::String.optional
  attribute :provider, Types::Coercible::String.optional
  attribute :email, Types::Coercible::String.optional
  attribute :telephone, Types::Coercible::String.optional
  attribute :first_name, Types::Coercible::String.optional
  attribute :last_name, Types::Coercible::String.optional
  attribute :middle_name, Types::Coercible::String.optional
  attribute :password, Types::Coercible::String.optional
  attribute :gender, Types::Coercible::String.optional
  attribute :avatar, Types::Any
  attribute :darenta_uid, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.optional
  attribute :app, Types::Coercible::String.optional
end
