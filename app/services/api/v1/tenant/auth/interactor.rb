require 'dry/transaction'

class Api::V1::Tenant::Auth::Interactor
  include Dry::Transaction

  AVALIABLE_LOCALES = %w(ru en ko)

  AUTH_VALIDATOR = Dry::Validation.Form do
    required(:email).filled(format?: ::Regex::Email.new.plain)
    required(:password).filled(:str?)
    optional(:app).maybe(:str?)
  end

  REG_VALIDATOR = Dry::Validation.Form do
    required(:provider).filled(:str?)
    required(:email).filled(format?: ::Regex::Email.new.plain)
    required(:password).filled(:str?)
    required(:first_name).filled(:str?)
    required(:last_name).filled(:str?)
    optional(:middle_name).maybe(:str?)
    optional(:gender).maybe(:str?)
    optional(:darenta_uid).maybe(:str?)
    optional(:telephone).maybe(:str?)
    optional(:locale).value(included_in?: AVALIABLE_LOCALES)
    optional(:app).maybe(:str?)

    configure do
      def upload_file?(value)
        value.class == ActionDispatch::Http::UploadedFile
      end

      def self.messages
        super.merge(
            en: { errors: { upload_file?: 'this is not a file' } }
        )
      end
    end
    optional(:avatar).maybe(:upload_file?)

  end

  step :auth_validate
  step :call_auth
  step :find_account
  step :check_password
  step :add_service
  step :prepare_attrs
  step :register_tenant
  step :update_locale
  step :update_last_sign_in
  step :resend_confirmation

  def auth_validate(hash)
    result = AUTH_VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def call_auth(hash)
    hash = hash.output.to_h.symbolize_keys
    Success(hash)
  end

  def find_account(hash)
    account = ::TenantAccount.find_by(email: hash[:email])
    Success(account: account, attrs: hash)
  end

  def check_password(hash)
    return Success(hash) unless hash[:account].present?

    account = hash[:account]
    password = hash[:attrs].to_h[:password]

    account.verify_password?(password) ? Success(hash) : Failure('invalid password')
  end

  def add_service(hash)
    return Success(hash) if hash[:account].present?
    #puts(hash[:attrs][:darenta_uid])
    return Success(hash) unless hash[:attrs][:darenta_uid].present?

    tenant = find_tenant_by_darenta_uid(hash[:attrs][:darenta_uid])
    return Success(hash) unless tenant.present?

    attributes = hash[:attrs]
    account_attributes = {
        tenant_id: tenant.id,
        provider: attributes[:provider] || 'email',
        email: attributes[:email],
        uid: attributes[:email],
        password: attributes[:password]
    }

    account = ::TenantAccount.new(account_attributes)
    unless account.save
      return Failure({ account: ['email already exist'] })
    end

    Success(account: account, attrs: hash[:attrs])
  end

  def prepare_attrs(hash)
    return Success(hash) if hash[:account].present?

    raw = hash[:attrs]
    attributes = {
        provider: 'email',
        email: raw[:email],
        first_name: raw[:first_name],
        last_name: raw[:last_name],
        middle_name: raw[:middle_name],
        telephone: raw[:telephone],
        gender: raw[:gender],
        avatar: raw[:avatar],
        password: raw[:password],
        darenta_uid: raw[:darenta_uid],
        locale: raw[:locale],
        app: raw[:app]
    }

    Success(account: hash[:account], attrs: Api::V1::Tenant::Auth::Structure.new(attributes).to_h)
  end

  def register_tenant(hash)
    return Success(hash) if hash[:account].present?

    attrs = hash[:attrs]
    attrs[:locale] = 'ru' unless attrs[:locale].present?

    attributes = REG_VALIDATOR.call(attrs)
    return Failure(attributes.errors) unless attributes.success?

    attributes = attributes.output
    locale = attributes.delete(:locale)

    tenant_attributes = {
        t_first_name_ru: attributes[:first_name],
        t_last_name_ru: attributes[:last_name],
        t_middle_name_ru: attributes[:middle_name],
        t_first_name_en: attributes[:first_name],
        t_last_name_en: attributes[:last_name],
        t_middle_name_en: attributes[:middle_name],
        t_first_name_ko: attributes[:first_name],
        t_last_name_ko: attributes[:last_name],
        t_middle_name_ko: attributes[:middle_name],
        telephone: attributes[:telephone],
        gender_ru: attributes[:gender],
        gender_en: attributes[:gender],
        gender_ko: attributes[:gender],
        avatar: attributes[:avatar],
        darenta_uid: attributes[:darenta_uid]
    }

    tenant = ::Tenant.new(tenant_attributes)
    unless tenant.save
      return Failure({ tenant: ['not created'] })
    end

    if locale.present?
      tenant.locale  = locale
      tenant.save
    end

    account_attributes = {
      tenant_id: tenant.id,
      provider: attributes[:provider],
      email: attributes[:email],
      uid: attributes[:email],
      password: attributes[:password]
    }

    account = ::TenantAccount.new(account_attributes)
    unless account.save
      tenant.delete
      return Failure({ account: ['email already exist'] })
    end

    form = {
        tenant_account: account,
        device: attrs[:app],
        source: 'api'
    }
    AdminsRegisterTenantJob.perform_later(form)

    hash[:account] = account

    Success(hash)
  end

  def update_locale(hash)
    account = hash[:account]
    attrs = hash[:attrs]

    if attrs[:locale].present?
      tenant = ::Tenant.find_by(id: account.tenant_id)
      if tenant.present?
        tenant.locale = attrs[:locale]
        tenant.save
      end
    end

    Success(account)
  end

  def update_last_sign_in(account)
    tenant = ::Tenant.find_by(id: account.tenant_id)
    tenant.update_last_sign_in

    Success(account)
  end

  def resend_confirmation(account)
    unless account.confirmed_at.present?
      tenant = ::Tenant.find_by(id: account.tenant_id)
      unless tenant.is_email_rejected
        account.confirmation_sent_from = 'api'
        account.save
        TenantsConfirmationJob.perform_later(account.id, 'api')
      end
    end

    Success(account)
  end

  private

  def find_tenant_by_darenta_uid(darenta_uid)
    tenant = nil

    if darenta_uid.present? && darenta_uid != ""
      tenant = ::Tenant.find_by(darenta_uid: darenta_uid)
    end

    return tenant
  end

end
