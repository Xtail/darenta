require 'dry/transaction'

class Api::V1::Tenant::Update::Interactor
  include Dry::Transaction

  AVALIABLE_LOCALES = %w(ru en ko)

  VALIDATOR = Dry::Validation.Form do
    configure do
      def upload_file?(value)
        value.class == ActionDispatch::Http::UploadedFile
      end

      def self.messages
        super.merge(
            en: { errors: { upload_file?: 'this is not a file' } }
        )
      end
    end

    required(:tenant_id).maybe(:int?)
    required(:provider).maybe(:str?)
    optional(:email).maybe(format?: ::Regex::Email.new.plain)
    optional(:first_name).maybe(:str?)
    optional(:last_name).maybe(:str?)
    optional(:middle_name).maybe(:str?)
    optional(:telephone).maybe(:str?)
    optional(:avatar).maybe(:upload_file?)
    optional(:facebook_email).maybe(format?: ::Regex::Email.new.plain)
    optional(:google_email).maybe(format?: ::Regex::Email.new.plain)
    optional(:gender).maybe(:str?)
    optional(:locale).maybe(included_in?: AVALIABLE_LOCALES)
  end

  step :symbolize
  step :cast
  step :validate
  step :resolve
  step :mail_to_tenant

  def symbolize(params)
    Success params.symbolize_keys
  end

  def cast(params)
    Success Api::V1::Tenant::Update::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    # hash[:tenant_id] = current_tenant_id
    # hash[:provider] = current_tenant_provider

    send_reconfirmation_email = false
    tenant = ::Tenant.find_by(id: hash[:tenant_id])
    current_account = ::TenantAccount.find_by(tenant_id: tenant.id, provider: hash[:provider])

    if hash[:locale].present?
      tenant = ::Tenant.find_by(id: current_account.tenant_id)
      tenant.locale = hash[:locale]
      tenant.save
    end

    attributes = hash.output.compact
    attributes.delete(:tenant_id)
    attributes.delete(:provider)

    # if hash[:email] present => change current_account.email
    if attributes[:email].present? && need_reconfirm?(current_account, attributes[:email])
      attributes[:unconfirmed_email] = attributes.delete(:email)
      send_reconfirmation_email = true
    else
      attributes.delete(:email)
    end

    attributes[:first_name] = tenant.t_first_name unless attributes[:first_name].present?
    attributes[:last_name] = tenant.t_last_name unless attributes[:last_name].present?
    attributes[:middle_name] = tenant.t_middle_name unless attributes[:middle_name].present?

    tenant_attributes = {
        t_first_name: attributes[:first_name],
        t_last_name: attributes[:last_name],
        t_middle_name: attributes[:middle_name],
        telephone: attributes[:telephone],
        gender: attributes[:gender],
        avatar: attributes[:avatar]
    }

    tenant = Api::V1::Tenant::Update::QueryBuilder.new.call(tenant, tenant_attributes)

    account_attributes = {
      unconfirmed_email: attributes[:unconfirmed_email]
    }

    account = Api::V1::Tenant::Update::QueryBuilder.new.call(current_account, account_attributes)
    #account.send_reconfirmation_instructions if send_reconfirmation_email
    TenantsConfirmationJob.perform_later(account.id, 'api') if send_reconfirmation_email

    if not send_reconfirmation_email

      if attributes[:facebook_email].present?
        send_reconfirmation_to(tenant, attributes[:facebook_email], :facebook) 
      else
        attributes.delete(:facebook_email)
      end

      if attributes[:google_email].present?
        send_reconfirmation_to(tenant, attributes[:google_email], :google) 
      else
        attributes.delete(:google_email)
      end

    end

    Success(account)
  end

  def mail_to_tenant(account)
    tenant = ::Tenant.find_by(id: account.tenant_id)

    if tenant.present?
      form = {
          tenant: tenant.t_first_name,
          recipient: account.email,
          locale: tenant.locale,
          source: 'api'
      }

      TenantMailer.with(form: form).update_personal_info_notify.deliver_later
    end

    Success(account)
  end

  protected

  def need_reconfirm?(tenant_account, new_email)
    tenant_account.email == new_email || tenant_account.unconfirmed_email == new_email ? false : true
  end

  def send_reconfirmation_to(tenant, new_email, provider)
    account = ::TenantAccount.find_by(tenant_id: tenant.id, provider: provider)

    if account.present?

      if need_reconfirm?(account, new_email)

        account_attributes = {
          unconfirmed_email: new_email
        }

        account = Api::V1::Tenant::Update::QueryBuilder.new.call(account, account_attributes)
        #account.send_reconfirmation_instructions
        TenantsConfirmationJob.perform_later(account.id, 'api')

      end
    end

  end


end
