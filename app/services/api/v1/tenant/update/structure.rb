require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::Update::Structure < Dry::Struct
  constructor_type :schema

  attribute :tenant_id,  Types::Coercible::Int
  attribute :email,      Types::Coercible::String.optional
  attribute :first_name, Types::Coercible::String.optional
  attribute :last_name,  Types::Coercible::String.optional
  attribute :middle_name,  Types::Coercible::String.optional
  attribute :telephone,  Types::Coercible::String.optional
  attribute :gender,  Types::Coercible::String.optional
  attribute :avatar,     Types::Any
  attribute :provider,   Types::Coercible::String.optional
  attribute :facebook_email, Types::Coercible::String.optional
  attribute :google_email, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.optional
end
