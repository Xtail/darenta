class Api::V1::Tenant::Update::QueryBuilder
  def call(tenant, attributes)
    return tenant unless attributes.present?
    update_personal_info(tenant, attributes)

    attributes.delete(:t_first_name)
    attributes.delete(:t_last_name)
    attributes.delete(:t_middle_name)
    attributes.delete(:telephone)
    attributes.delete(:gender)

    update!(tenant, attributes)
  end

  private

  def update_personal_info(tenant, attributes)
    new_t_first_name = attributes[:t_first_name]
    if new_t_first_name.present? && tenant.t_first_name != new_t_first_name
      tenant.unconfirmed_t_first_name = new_t_first_name
    end

    new_t_last_name = attributes[:t_last_name]
    if new_t_last_name.present? && tenant.t_last_name != new_t_last_name
      tenant.unconfirmed_t_last_name = new_t_last_name
    end

    new_t_middle_name = attributes[:t_middle_name]
    if new_t_middle_name.present? && tenant.t_middle_name != new_t_middle_name
      tenant.unconfirmed_t_middle_name = new_t_middle_name
    end

    new_telephone = attributes[:telephone]
    if new_telephone.present?
      if tenant.telephone.present?
        if tenant.telephone != new_telephone
          tenant.unconfirmed_telephone = new_telephone
        end
      else
        tenant.telephone = new_telephone
      end
    end

    new_gender = attributes[:gender]
    if new_gender.present? && tenant.gender != new_gender
      tenant.unconfirmed_gender = new_gender
    end

    tenant.save
    tenant
  end

  def update!(tenant, attributes)
    tenant.update_attributes(attributes)
    tenant
  end

end
