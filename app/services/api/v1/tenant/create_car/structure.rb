require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::CreateCar::Structure < Dry::Struct
  constructor_type :schema

  attribute :current_tenant_id, Types::Coercible::Int.optional
  attribute :city_name, Types::Coercible::String.optional
  attribute :engine_id, Types::Coercible::Int.optional
  attribute :color_id, Types::Coercible::Int.optional
  attribute :transmission_id, Types::Coercible::Int.optional
  attribute :body_id, Types::Coercible::Int.optional

  attribute :mark_name, Types::Coercible::String.optional
  attribute :sampler_name, Types::Coercible::String.optional
  attribute :name, Types::Coercible::String.optional
  attribute :full_name, Types::Coercible::String.optional

  attribute :production_year, Types::Coercible::Int.optional
  attribute :address, Types::Coercible::String.optional
  attribute :link_to_car, Types::Coercible::String.optional

  attribute :is_delivery_to_airport, Types::Form::Bool.optional
  attribute :is_delivery_to_you, Types::Form::Bool.optional
  attribute :is_baby_chair, Types::Form::Bool.optional
  attribute :is_wifi, Types::Form::Bool.optional
  attribute :is_navigator, Types::Form::Bool.optional
  attribute :is_current_offer, Types::Form::Bool.optional

  attribute :latitude, Types::Coercible::Float.optional
  attribute :longitude, Types::Coercible::Float.optional

  attribute :price, Types::Coercible::Float.optional
  attribute :price_few_days, Types::Coercible::Float.optional
  attribute :price_week, Types::Coercible::Float.optional
  attribute :price_month, Types::Coercible::Float.optional
  attribute :deposit, Types::Coercible::Float.optional
  attribute :currency, Types::Coercible::String.optional

  attribute :image, Types::Any

  attribute :insurance_images, Types::Coercible::Array do
    attribute :image, Types::Any
  end

  attribute :contract_images, Types::Coercible::Array do
    attribute :image, Types::Any
  end
end
