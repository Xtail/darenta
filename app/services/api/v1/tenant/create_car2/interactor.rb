require 'dry/transaction'

# Обработчик создания автомобиля через API
class Api::V1::Tenant::CreateCar2::Interactor
  include Dry::Transaction

  # Обеспечивает валидацию передаваемых полей при создании автомобиля
  VALIDATOR = Dry::Validation.Form do
    required(:current_tenant_id).filled(:int?)
    required(:city_name).filled(:str?)
    required(:engine_id).filled(:int?)
    required(:color_id).filled(:int?)
    required(:transmission_id).filled(:int?)
    required(:body_id).filled(:int?)

    required(:mark_name).filled(:str?)
    required(:sampler_name).filled(:str?)
    required(:production_year).filled(:int?)

    required(:price).filled(:float?)
    required(:price_few_days).filled(:float?)
    required(:price_week).filled(:float?)
    required(:price_month).filled(:float?)
    required(:deposit).filled(:float?)
    required(:currency).filled(:str?)

    optional(:address).maybe(:str?)
    optional(:link_to_car).maybe(:str?)
    optional(:is_delivery_to_airport).maybe(:bool?)
    optional(:is_delivery_to_you).maybe(:bool?)
    optional(:is_baby_chair).maybe(:bool?)
    optional(:is_wifi).maybe(:bool?)
    optional(:is_navigator).maybe(:bool?)
    optional(:is_current_offer).maybe(:bool?)

    optional(:latitude).maybe(:float?)
    optional(:longitude).maybe(:float?)

    optional(:image_ids).maybe(:str?)
    #optional(:insurance_names).maybe { each(:str?) }
    #optional(:insurance_image_ids).any
    optional(:contract_image_ids).maybe(:str?)
    optional(:insurances_json).maybe(:str?)
    optional(:comments).maybe(:str?)
  end

  # Последовательность выполнения методов
  step :symbolize
  step :merge_defaults
  step :cast
  step :validate
  step :check_tenant
  step :check_color
  step :check_engine
  step :check_transmission
  step :check_city
  step :create_city
  step :check_body
  step :resolve_images
  step :resolve_contracts
  step :resolve_insurances
  step :create_attributes
  step :create_car
  step :mail_to_tenant
  step :mail_to_admin

  # Преобразует строки в символы
  def symbolize(params) 
    Success params.symbolize_keys
  end

  # Заглушка, для установки параметров по умолчанию
  def merge_defaults(params)
    Success params
  end

  # Преобразование параметров в объект Dry::Struct
  def cast(params)
    Success Api::V1::Tenant::CreateCar2::Structure.new(params).to_h
  end

  # Валидация параметров автомобиля
  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  # Проверка существования владельца в базе
  def check_tenant(hash)
    ::Tenant.exists?(id: hash[:current_tenant_id]) ? Success(hash) : Failure({ tenant_id: ['not present in database'] })
  end

  # Проверка существования цвета кузова в базе
  def check_color(hash)
    ::Color.exists?(id: hash[:color_id]) ? Success(hash) : Failure({ color_id: ['not present in database'] })
  end

  # Проверка существования типа двигателя в базе
  def check_engine(hash)
    ::Engine.exists?(id: hash[:engine_id]) ? Success(hash) : Failure({ engine_id: ['not present in database'] })
  end

  # Проверка существования коробки передач в базе
  def check_transmission(hash)
    ::Transmission.exists?(id: hash[:transmission_id]) ? Success(hash) : Failure({ transmission_id: ['not present in database'] })
  end

  # Проверка существования города в базе
  def check_city(hash)
    hash = hash.to_h

    city_name = hash[:city_name]
    city_id = nil

    translation = ::CityTranslation.find_by(name: city_name)
    if translation.present?
      city_id = translation.city_id
    end

    hash[:city_id] = city_id
    Success(hash)
  end

  # При отсутсвии города в базе, создает новый город
  def create_city(hash)
    return Success(hash) if hash[:city_id].present?

    attributes = {}

    I18n.available_locales.each do |l|
      attributes["name_#{l}"] = hash[:city_name]
    end

    city = ::City.create(attributes)

    hash[:city_id] = city.id
    Success(hash)
  end

  # Проверка существования типа кузова в базе
  def check_body(hash)
    ::Body.exists?(id: hash[:body_id]) ? Success(hash) : Failure({ body_id: ['not present in database'] })
  end

  # Парсинг переданных картинок автомобиля
  def resolve_images(hash)
    raw_image_ids = hash[:image_ids]
    hash.delete(:image_ids)

    if raw_image_ids.present?
      image_ids_json = ::Resource.new.ids_to_json_array(raw_image_ids)
      if image_ids_json.present?
        hash[:images_ids] = image_ids_json
      end
    end

    Success(hash)
  end

  # Парсинг переданных картинок договоров
  def resolve_contracts(hash)
    raw_contract_image_ids = hash[:contract_image_ids]
    hash.delete(:contract_image_ids)

    if raw_contract_image_ids.present?
      contract_image_ids_json = ::Resource.new.ids_to_json_array(raw_contract_image_ids)
      if contract_image_ids_json.present?
        hash[:contract_images_ids] = contract_image_ids_json
      end
    end

    Success(hash)
  end

  # Парсинг переданных страховок
  def resolve_insurances(hash)
    insurances = []

    json = hash[:insurances_json]
    hash.delete(:insurances_json)

    if json.present?
      begin
        json.gsub! "=>", ":"
        parsed_json = JSON.parse(json)
        
        parsed_json.each do |t|
          insurance = {
            name: t["name"],
            images_ids: t["img_ids"]
          }
          insurances << insurance
        end

        hash[:insurances] = insurances
      rescue
        return Failure({ insurances_json: ['invalid format'] })
      end
    end
    
    Success(hash)
  end

  # Создание объекта для записи в базу
  def create_attributes(hash)
    currency = hash[:currency]
    price_day = hash[:price]
    price_few_days = hash[:price_few_days]
    price_week = hash[:price_week]
    price_month = hash[:price_month]
    deposit = hash[:deposit]

    car_attributes = {
      is_delivery_to_airport: hash[:is_delivery_to_airport] || false,
      is_delivery_to_you: hash[:is_delivery_to_you] || false,
      is_baby_chair: hash[:is_baby_chair] || false,
      is_wifi: hash[:is_wifi] || false,
      is_navigator: hash[:is_navigator] || false,
      color_id: hash[:color_id],
      engine_id: hash[:engine_id],
      transmission_id: hash[:transmission_id],
      city_id: hash[:city_id],
      tenant_id: hash[:current_tenant_id],
      mark_name: hash[:mark_name],
      sampler_name: hash[:sampler_name],
      name: hash[:mark_name] + ' ' + hash[:sampler_name],
      full_name: hash[:mark_name] + ' ' + hash[:sampler_name],
      body_id: hash[:body_id],
      production_year: hash[:production_year],
      is_current_offer: hash[:is_current_offer] || false,
      address: hash[:address],
      latitude: hash[:latitude],
      longitude: hash[:longitude],
      price_rub: ::Car.new.convert_price(price_day, currency, 'rub'),
      price_usd: ::Car.new.convert_price(price_day, currency, 'usd'),
      price_krw: ::Car.new.convert_price(price_day, currency, 'krw'),
      price_eur: ::Car.new.convert_price(price_day, currency, 'eur'),
      price_few_days_rub: ::Car.new.convert_price(price_few_days, currency, 'rub'),
      price_few_days_usd: ::Car.new.convert_price(price_few_days, currency, 'usd'),
      price_few_days_krw: ::Car.new.convert_price(price_few_days, currency, 'krw'),
      price_few_days_eur: ::Car.new.convert_price(price_few_days, currency, 'eur'),
      price_week_rub: ::Car.new.convert_price(price_week, currency, 'rub'),
      price_week_usd: ::Car.new.convert_price(price_week, currency, 'usd'),
      price_week_krw: ::Car.new.convert_price(price_week, currency, 'krw'),
      price_week_eur: ::Car.new.convert_price(price_week, currency, 'eur'),
      price_month_rub: ::Car.new.convert_price(price_month, currency, 'rub'),
      price_month_usd: ::Car.new.convert_price(price_month, currency, 'usd'),
      price_month_krw: ::Car.new.convert_price(price_month, currency, 'krw'),
      price_month_eur: ::Car.new.convert_price(price_month, currency, 'eur'),
      deposit_rub: ::Car.new.convert_price(deposit, currency, 'rub'),
      deposit_usd: ::Car.new.convert_price(deposit, currency, 'usd'),
      deposit_krw: ::Car.new.convert_price(deposit, currency, 'krw'),
      deposit_eur: ::Car.new.convert_price(deposit, currency, 'eur'),
      link_to_car: hash[:link_to_car],
      images_ids: hash[:images_ids],
      contract_images_ids: hash[:contract_images_ids],
      insurances: hash[:insurances],
      comments: hash[:comments],
      is_moderated: false
    }

    result_hash = {
      car_attributes: car_attributes
    }

    Success(result_hash)
  end

  # Сохранение автомобиля в базе
  def create_car(hash)
    car = ::Car.new(hash[:car_attributes])
    car.save
    car.present? ? Success(car) : Failure({ car: ['can not create'] })
  end

  # Отправка сообщения пользователю
  def mail_to_tenant(car)
    form = {
      car: car,
      source: 'api'
    }
    TenantsUpdateCarInfoJob.perform_later(form)
    Success(car)
  end

  # Отправка сообщения администратору
  def mail_to_admin(car)
    form = {
      car: car,
      moderation_request_at: DateTime.now.strftime('%d.%m.%Y %I:%M%p'),
      source: 'api'
    }
    AdminsTenantAddCarRequestJob.perform_later(form)
    Success(car)
  end
end
