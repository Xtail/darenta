require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::Resend_Confirmation::Structure < Dry::Struct
  constructor_type :schema

  attribute :tenant_id, Types::Coercible::Int.optional
  attribute :provider, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.optional
end
