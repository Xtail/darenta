require 'dry/transaction'

class Api::V1::Tenant::Resend_Confirmation::Interactor
  include Dry::Transaction

  AVALIABLE_LOCALES = %w(ru en ko)

  VALIDATOR = Dry::Validation.Form do
    required(:tenant_id).filled(:int?)
    required(:provider).filled(:str?)
    optional(:locale).value(included_in?: AVALIABLE_LOCALES)
  end

  step :validate
  step :find_account
  step :update_locale
  step :resend_email

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def find_account(hash)
    hash = hash.to_h
    current_account = ::TenantAccount.find_by(tenant_id: hash[:tenant_id], provider: hash[:provider])

    Success(account: current_account, attrs: hash)
  end

  def update_locale(hash)
    account = hash[:account]
    attrs = hash[:attrs]

    if attrs[:locale].present?
      tenant = ::Tenant.find_by(id: account.tenant_id)
      tenant.locale = attrs[:locale]
      tenant.save
    end

    Success(hash)
  end

  def resend_email(hash)
    return Failure('can not find an account') if not hash[:account].present?

    account = hash[:account]
    #ConfirmationMailer.add_sevice_instructions(account, account.confirmation_token).deliver_now
    #account.send_reconfirmation_instructions
    TenantsConfirmationJob.perform_later(account.id, 'api')

    Success(account)
  end
end
