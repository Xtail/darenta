require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::UpdatePassword::Structure < Dry::Struct
  constructor_type :schema

  attribute :tenant_account_id, Types::Coercible::Int
  attribute :old_password, Types::Coercible::String.optional
  attribute :new_password, Types::Coercible::String.optional
end
