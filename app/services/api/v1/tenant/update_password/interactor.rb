require 'dry/transaction'

class Api::V1::Tenant::UpdatePassword::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:tenant_account_id).filled(:int?)
    required(:old_password).filled(:str?)
    required(:new_password).filled(:str?)
  end

  step :symbolize
  step :cast
  step :validate
  step :resolve

  def symbolize(params)
    Success params.symbolize_keys
  end

  def cast(params)
    Success Api::V1::Tenant::UpdatePassword::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    current_tenant_account = TenantAccount.find_by(id: hash[:tenant_account_id])

    if hash[:old_password] == hash[:new_password]
      return Success(current_tenant_account)
    end

    unless current_tenant_account.verify_password?(hash[:old_password])
      return Failure({ account: ['invalid old password'] })
    end

    attributes = {
        password: hash[:new_password]
    }

    current_tenant_account = Api::V1::Tenant::UpdatePassword::QueryBuilder.new.call(current_tenant_account, attributes)

    Success(current_tenant_account)
  end

end
