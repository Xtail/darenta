require 'dry/transaction'

class Api::V1::Tenant::RemoveCar::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:car_id).filled(:int?)
  end

  step :symbolize
  step :cast
  step :validate
  step :check_car
  step :remove_car

  def symbolize(params)
    Success params.symbolize_keys
  end

  def cast(params)
    Success Api::V1::Tenant::UpdateCar::Structure.new(params).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_car(hash)
    ::Car.exists?(id: hash[:car_id]) ? Success(hash) : Failure({ car_id: ['not present in database'] })
  end

  def remove_car(hash)

    car = ::Car.find_by(id: hash[:car_id])
    return Failure({ car: ['can not find'] }) if not car.present?
    car.delete

    Success('ok')
  end
end
