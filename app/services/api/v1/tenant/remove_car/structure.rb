require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::RemoveCar::Structure < Dry::Struct
  constructor_type :schema

  attribute :car_id, Types::Coercible::Int.optional
end
