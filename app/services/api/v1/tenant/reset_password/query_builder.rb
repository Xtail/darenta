class Api::V1::Tenant::ResetPassword::QueryBuilder
  def call(tenant_account, attributes)
    return tenant_account unless attributes.present?
    update!(tenant_account, attributes)
  end

  private

  def update!(tenant_account, attributes)
    tenant_account.update_attributes(attributes)
    tenant_account
  end
end
