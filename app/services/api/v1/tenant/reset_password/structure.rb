require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Tenant::ResetPassword::Structure < Dry::Struct
  constructor_type :schema

  attribute :email, Types::Coercible::String.optional
  attribute :provider, Types::Coercible::String.optional
end
