require 'dry/transaction'

class Api::V1::Tenant::ResetPassword::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:email).filled(:str?)
    optional(:provider).maybe(:str?)
  end

  step :symbolize
  step :cast
  step :validate
  step :resolve

  def symbolize(params)
    Success params.symbolize_keys
  end

  def cast(params)
    Success Api::V1::Tenant::ResetPassword::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    current_tenant_account = TenantAccount.find_by(email: hash[:email])

    unless current_tenant_account.present?
      return Failure({ tenant_account: ['not present in database'] })
    end

    current_tenant = ::Tenant.find_by(id: current_tenant_account.tenant_id)

    unless current_tenant.present?
      return Failure({ tenant: ['not present in database'] })
    end

    #new_password = generate_new_password
    reset_password_hash = generate_reset_password_hash

    attributes = {
        reset_password_hash: reset_password_hash
    }

    current_tenant_account = Api::V1::Tenant::ResetPassword::QueryBuilder.new.call(current_tenant_account, attributes)

    form = {
        recipient: current_tenant_account.email,
        locale: current_tenant.locale,
        reset_password_link: generate_reset_password_link(reset_password_hash),
        source: 'api'
    }

    TenantsResetPasswordJob.perform_later(form)

    Success(current_tenant_account)
  end

  private

  def generate_new_password
    Devise.friendly_token[0,7]
  end

  def generate_reset_password_hash
    Devise.friendly_token[0,18]
  end

  def generate_reset_password_link(token)
    Rails.application.secrets.full_host + '/password_reset_form?token=' + token
  end

end
