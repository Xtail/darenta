class Api::V1::Tenant::MyCars::QueryBuilder
  attr_accessor :initial_scope

  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def call(hash)
    scoped = initial(initial_scope)
    scoped = filter_by_name(scoped, hash[:name])
    scoped = filter_by_body(scoped, hash[:body_id])
    scoped = filter_by_price_gteq(scoped, hash[:price_type], hash[:price_gteq])
    scoped = filter_by_price_lteq(scoped, hash[:price_type], hash[:price_lteq])
    scoped = filter_by_engine(scoped, hash[:engine_id])
    scoped = filter_by_transmission(scoped, hash[:transmission_id])
    scoped = filter_by_color(scoped, hash[:color_id])
    scoped = filter_by_city(scoped, hash[:city_name], hash[:locale])
    scoped = filter_by_current_offer(scoped, hash[:is_current_offer])
    scoped = filter_by_delivery_to_airport(scoped, hash[:is_delivery_to_airport])
    scoped = filter_by_delivery_to_you(scoped, hash[:is_delivery_to_you])
    scoped = filter_by_baby_chair(scoped, hash[:is_baby_chair])
    scoped = filter_by_wifi(scoped, hash[:is_wifi])
    scoped = filter_by_navigator(scoped, hash[:is_navigator])
    sort_by_updated_at(scoped)
  end

  private

  def initial(scoped)
    scoped.includes(tenant: :translations,
                    color: :translations,
                    engine: :translations,
                    transmission: :translations,
                    city: :translations,
                    body: :translations)
  end

  def filter_by_name(scoped, name)
    name.present? ? scoped.where('full_name ILIKE ?', "%#{name}%") : scoped
  end

  def filter_by_body(scoped, body_id)
    body_id.present? ? scoped.where(bodies: { id: body_id }) : scoped
  end

  def filter_by_price_gteq(scoped, price_type, price_gteq)
    price_gteq.present? ? scoped.where("price_#{price_type} >= ?", price_gteq) : scoped
  end

  def filter_by_price_lteq(scoped, price_type, price_lteq)
    price_lteq.present? ? scoped.where("price_#{price_type} <= ?", price_lteq) : scoped
  end

  def filter_by_engine(scoped, engine_id)
    engine_id.present? ? scoped.where(engines: { id: engine_id }) : scoped
  end

  def filter_by_transmission(scoped, transmission_id)
    transmission_id.present? ? scoped.where(transmissions: { id: transmission_id }) : scoped
  end

  def filter_by_color(scoped, color_id)
    color_id.present? ? scoped.where(colors: { id: color_id }) : scoped
  end

  def filter_by_city(scoped, city_name, locale)
    locale ||= %w[ru en]

    if city_name.present?
      scoped.where(cities: { id: CityTranslation.select(:city_id).where(locale: locale).where('name ILIKE ?', "%#{city_name}%") })
    else
      scoped
    end
  end

  def filter_by_current_offer(scoped, bool)
    bool ? scoped.where(is_current_offer: true) : scoped
  end

  def filter_by_delivery_to_airport(scoped, bool)
    bool ? scoped.where(is_delivery_to_airport: true) : scoped
  end

  def filter_by_delivery_to_you(scoped, bool)
    bool ? scoped.where(is_delivery_to_you: true) : scoped
  end

  def filter_by_baby_chair(scoped, bool)
    bool ? scoped.where(is_baby_chair: true) : scoped
  end

  def filter_by_wifi(scoped, bool)
    bool ? scoped.where(is_wifi: true) : scoped
  end

  def filter_by_navigator(scoped, bool)
    bool ? scoped.where(is_navigator: true) : scoped
  end

  def sort_by_updated_at(scoped)
    scoped.sort_by { |x| x.updated_at }.reverse
  end
end
