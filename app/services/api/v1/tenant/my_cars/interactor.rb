require 'dry/transaction'

class Api::V1::Tenant::MyCars::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  AVALIABLE_LOCALES = %w(ru en ko)

  VALIDATOR = Dry::Validation.Form do
    required(:current_tenant_id).filled(:int?)
    optional(:name).maybe(:str?)
    optional(:locale).maybe(:str?)
    optional(:price_type).value(included_in?: %w(rub usd krw))
    optional(:price_gteq).maybe(:float?)
    optional(:price_lteq).maybe(:float?)
    optional(:is_current_offer).maybe(:bool?)
    optional(:is_delivery_to_airport).maybe(:bool?)
    optional(:is_delivery_to_you).maybe(:bool?)
    optional(:is_baby_chair).maybe(:bool?)
    optional(:is_wifi).maybe(:bool?)
    optional(:is_navigator).maybe(:bool?)
    optional(:body_id).maybe(:int?)
    optional(:engine_id).maybe(:int?)
    optional(:transmission_id).maybe(:int?)
    optional(:color_id).maybe(:int?)
    optional(:city_name).maybe(:str?)
  end

  step :merge_defaults
  step :cast
  step :validate
  step :resolve

  def merge_defaults(params)
    params[:price_type] = 'rub' unless params[:price_type].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Tenant::MyCars::Structure.new(params).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    puts("hash", hash.to_h)
    Success(Api::V1::Tenant::MyCars::QueryBuilder.new(::Car.where(tenant_id: hash[:current_tenant_id])).call(hash))
  end
end
