require 'dry/transaction'

class Api::V1::Resource::LoadFile::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    configure do
      def upload_file?(value)
        value.class == ActionDispatch::Http::UploadedFile
      end

      def self.messages
        super.merge(
            en: { errors: { upload_file?: 'this is not a file' } }
        )
      end
    end

    optional(:file).maybe(:upload_file?)
  end

  step :cast
  step :validate
  step :resolve

  def cast(params)
    Success Api::V1::Resource::LoadFile::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    hash = hash.to_h
    res = ::Resource.new
    res.file = hash[:file]
    unless res.save
      return Failure({ res: ['not uploaded'] })
    end

    Success(res)
  end
end