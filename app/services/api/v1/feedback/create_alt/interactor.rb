require 'dry/transaction'

class Api::V1::Feedback::CreateAlt::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:name).filled(:str?)
    required(:email).filled(format?: ::Regex::Email.new.plain)
    required(:text).filled(:str?)
    optional(:device_type_id).maybe(:int?)
  end

  step :merge_defaults
  step :cast
  step :validate
  step :check_device_type
  step :send_mail

  def merge_defaults(hash)
    current_tenant_account = ::TenantAccount.find_by(id: hash[:tenant_account_id])
    unless current_tenant_account.present?
      Failure({ current_tenant_account: ['not present in database'] })
    end

    current_tenant = ::Tenant.find_by(id: current_tenant_account.tenant_id)
    unless current_tenant.present?
      Failure({ current_tenant: ['not present in database'] })
    end

    tenant_email = current_tenant_account.email
    tenant_name = current_tenant.t_first_name

    hash[:name] = tenant_name
    hash[:email] = hash[:email] || tenant_email
    Success(hash)
  end

  def cast(params)
    Success Api::V1::Feedback::CreateAlt::Structure.new(params).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_device_type(hash)
    return Success(hash) unless hash[:device_type_id].present?
    ::DeviceType.exists?(id: hash[:device_type_id]) ? Success(hash) : Failure({ device_type_id: ['not present in database'] })
  end

  def send_mail(hash)
    FeedbackMailer.with(form: hash.to_h).notify.deliver_now
    Success(:ok)
  end
end
