require 'dry/transaction'

class Api::V1::Feedback::Create::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:name).filled(:str?)
    required(:email).filled(format?: ::Regex::Email.new.plain)
    required(:text).filled(:str?)
    optional(:device_type_id).maybe(:int?)
  end

  step :cast
  step :validate
  step :check_device_type
  step :send_mail

  def cast(params)
    Success Api::V1::Feedback::Create::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_device_type(hash)
    return Success(hash) unless hash[:device_type_id].present?
    ::DeviceType.exists?(id: hash[:device_type_id]) ? Success(hash) : Failure({ device_type_id: ['not present in database'] })
  end

  def send_mail(hash)
    FeedbackMailer.with(form: hash.to_h).notify.deliver_now
    Success(:ok)
  end
end
