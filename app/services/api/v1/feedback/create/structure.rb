require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Feedback::Create::Structure < Dry::Struct
  constructor_type :schema

  attribute :name,  Types::Coercible::String.optional
  attribute :email, Types::Coercible::String.optional
  attribute :text,  Types::Coercible::String.optional
  attribute :device_type_id, Types::Coercible::Int.optional
end
