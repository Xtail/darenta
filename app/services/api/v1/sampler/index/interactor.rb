require 'dry/transaction'

class Api::V1::Sampler::Index::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:mark).filled(:str?)
  end

  step :merge_defaults
  step :cast
  step :validate
  step :find_mark
  step :resolve

  def merge_defaults(params)
    Success params
  end

  def cast(params)
    Success Api::V1::Sampler::Index::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def find_mark(hash)
    hash = hash.to_h
    mark = ::Mark.find_by(name: hash[:mark])

    return Failure ({ mark: ['not present in database'] }) unless mark.present?
    hash[:mark_id] = mark.id
    Success hash
  end

  def resolve(hash)
    Success ::Sampler.where(mark_id: hash[:mark_id])
  end
end
