require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Car::Show::Structure < Dry::Struct
  constructor_type :schema

  attribute :id, Types::Coercible::Int.optional
  attribute :app, Types::Coercible::String.optional
end
