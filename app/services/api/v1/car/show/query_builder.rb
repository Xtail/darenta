class Api::V1::Car::Show::QueryBuilder
  def call(hash)
    scoped = initial
    scoped = filter_by_id(scoped, hash[:id])
    take(scoped)
  end

  private

  def initial
    ::Car.includes(tenant: :translations,
                   color: :translations,
                   engine: :translations,
                   transmission: :translations,
                   city: :translations,
                   body: :translations)
  end

  def filter_by_id(scoped, id)
    scoped.where(id: id)
  end

  def take(scoped)
    scoped.take
  end
end
