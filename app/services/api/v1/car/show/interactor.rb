require 'dry/transaction'

class Api::V1::Car::Show::Interactor
  include Dry::Transaction

  VALIDATOR = Dry::Validation.Form do
    required(:id).filled(:int?)
  end

  step :cast
  step :validate
  step :resolve

  def cast(params)
    Success Api::V1::Car::Show::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def resolve(hash)
    result = Api::V1::Car::Show::QueryBuilder.new.call(hash)
    result.present? ? Success(result) : Failure({ id: ['not present in database'] })
  end
end
