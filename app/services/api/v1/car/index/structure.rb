require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Car::Index::Structure < Dry::Struct
  constructor_type :schema

  attribute :app, Types::Coercible::String.optional
  attribute :name, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.optional
  attribute :price_type, Types::Coercible::String.default('rub')
  attribute :price_gteq, Types::Coercible::Int.optional
  attribute :price_lteq, Types::Coercible::Int.optional
  attribute :is_current_offer, Types::Form::Bool.optional
  attribute :is_delivery_to_airport, Types::Form::Bool.optional
  attribute :is_delivery_to_you, Types::Form::Bool.optional
  attribute :is_baby_chair, Types::Form::Bool.optional
  attribute :is_wifi, Types::Form::Bool.optional
  attribute :is_navigator, Types::Form::Bool.optional
  attribute :body_id, Types::Coercible::Int.optional
  attribute :engine_id, Types::Coercible::Int.optional
  attribute :transmission_id, Types::Coercible::Int.optional
  attribute :color_id, Types::Coercible::Int.optional
  attribute :city_name, Types::Coercible::String.optional

  attribute :is_partner_cars, Types::Form::Bool.optional
  attribute :date_from, Types::Coercible::String.optional
  attribute :date_to, Types::Coercible::String.optional
  attribute :platform, Types::Coercible::String.optional
end
