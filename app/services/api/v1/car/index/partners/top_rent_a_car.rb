require 'httparty'

class Api::V1::Car::Index::Partners::TopRentACar
  include HTTParty

  base_uri 'http://toprentacar.bg/xml_gate/'

  class << self
    def get_cars(location, date_from, date_to)

      date_from = date_from + 'T12:00:00'
      date_to = date_to + 'T12:00:00'

      pull_cars(location, date_from, date_to)
    end

    def pull_cars(location, date_from, date_to)

      current_location = ''

      current_locale = I18n.locale
      I18n.available_locales.each do |l|
        I18n.locale = l
        city = City.find_by(name: location)
        if city.present?
          current_location = city.location_code
          break
        end
      end
      I18n.locale = current_locale

      #puts("location_code", current_location)
      
      headers = { Authorization: 'Basic ZGFyZW50YTpkQHJlbnRANTYzMw=='}

      body = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        request = xml.Request do
          xml.ResRates do
            xml.Partner.Code="darenta"
            
            pickup = xml.Pickup
            pickup.dateTime=date_from
            pickup.locationCode=current_location

            ret = xml.Return
            ret.dateTime=date_to
            ret.locationCode=location
          end
        end
        request.referenceNumber="23de0a9186adf56b3e580b7eb2c9ab45"
        request.version="3.0"
        request.xmlns="http://toprentacar.bg/xml/"
      end

      options = { headers: headers, body: body.to_xml }

      response = post('/', options)

      return nil unless response.success?

      result = parse_response(response.body)
      result = create_cars(result, date_from, date_to, current_location)
      #result = convert_to_darenta_format(result, date_from, date_to)

      result
    end

    def parse_response(response)
      partner_cars = []

      xml_doc = Nokogiri::XML(response)
      xml_doc.search('Response ResRates Rate').each do |car|

        attributes = {
          RateID: car.at('RateID').text,
          ACRISS: car.at('ACRISS').text,
          Availability: car.at('Availability').text,
          TotalCost: car.at('TotalCost').text,
          CurrencyCode: car.at('CurrencyCode').text,
          CarName: car.at('CarName').text,
          Gearbox: car.at('Gearbox').text,
          Fuel: car.at('Fuel').text,
          RequestRate: car.at('RequestRate').text
        }

        net_rate_xml = car.at('NetRate')
        if net_rate_xml.present?
          attributes[:NetRate] = net_rate_xml.text
        end

        delivery_xml = car.at('Delivery')
        if delivery_xml.present?
          attributes[:Delivery] = delivery_xml.text
        end

        out_of_hours_xml = car.at('OutOfHours')
        if out_of_hours_xml.present?
          attributes[:OutOfHours] = out_of_hours_xml.text
        end

        seats_xml = car.at('Seats')
        if seats_xml.present?
          attributes[:Seats] = seats_xml.text
        end

        doors_xml = car.at('Doors')
        if doors_xml.present?
          attributes[:Doors] = doors_xml.text
        end

        bags_big_xml = car.at('BagsBig')
        if bags_big_xml.present?
          attributes[:BagsBig] = bags_big_xml.text
        end

        air_con_xml = car.at('AirCon')
        if air_con_xml.present?
          attributes[:AirCon] = air_con_xml.text
        end

        wheel_drive_xml = car.at('WheelDrive')
        if wheel_drive_xml.present?
          attributes[:WheelDrive] = wheel_drive_xml.text
        end

        partner_cars << attributes
      end

      partner_cars
    end

    def create_cars(partner_cars, date_from, date_to, location_code)
      days = (Date.parse(date_to.to_s) - Date.parse(date_from.to_s)).to_i

      cars = []

      partner_cars.each do |car|
        price_rub = PartnerCar.new.rub_from_eur(car[:TotalCost])
        price_usd = PartnerCar.new.rub_to_usd(price_rub)
        price_krw = PartnerCar.new.rub_to_krw(price_rub)

        attributes = {
          rate_id: car[:RateID],
          acriss_code: car[:ACRISS],
          availability: car[:Availability],
          net_rate: car[:NetRate],
          delivery: car[:Delivery],
          out_of_hours: car[:OutOfHours],
          total_cost: car[:TotalCost],
          currency_code: car[:CurrencyCode],
          car_name: car[:CarName],
          seats: car[:Seats],
          doors: car[:Doors],
          gearbox: car[:Gearbox],
          bags_big: car[:BagsBig],
          air_con: car[:AirCon],
          wheel_drive: car[:WheelDrive],
          fuel: car[:Fuel],
          request_rate: car[:RequestRate],

          price_rub: (price_rub.to_f / days).round(2),
          price_usd: (price_usd.to_f / days).round(2),
          price_krw: (price_krw.to_f / days).round(2),
          price_eur: (car[:TotalCost].to_f / days).round(2),
          is_current_offer: true,
          location_code: location_code
        }

        if car[:Gearbox] == 'a'
          attributes[:transmission_id] = 1
        elsif car[:Gearbox] == 'm'
          attributes[:transmission_id] = 2
        end

        if car[:Fuel] == 'petrol'
          attributes[:engine_id] = 1
        elsif car[:Fuel] == 'diesel' 
          attributes[:engine_id] = 2
        end

        cars << ::PartnerCar.new(attributes)
      end

      cars
    end

    def convert_to_darenta_format(hash, date_from, date_to)

      days = (Date.parse(date_to.to_s) - Date.parse(date_from.to_s)).to_i

      result = []

      hash.each do |partner_car|
        price_rub = PartnerCar.new.rub_from_eur(partner_car[:TotalCost])
        price_usd = PartnerCar.new.rub_to_usd(price_rub)
        price_krw = PartnerCar.new.rub_to_krw(price_rub)

        our_car_attributes = {
          full_name: partner_car[:CarName],
          is_delivery_to_airport: false,
          is_delivery_to_you: false,
          is_baby_chair: false,
          is_wifi: false,
          is_navigator: false,
          color_id: nil,
          city_id: nil,
          body_id: nil,
          production_year: nil,
          is_current_offer: true,
          address: 'partner',
          price_rub: (price_rub.to_f / days).round(2),
          price_usd: (price_usd.to_f / days).round(2),
          price_krw: (price_krw.to_f / days).round(2),
          price_eur: (partner_car[:TotalCost].to_f / days).round(2),
          link_to_car: '',
          user_id: nil,
          link_to_car: partner_car[:RequestRate]
        }

        if partner_car[:Gearbox] == 'a'
          our_car_attributes[:transmission_id] = 1
        elsif partner_car[:Gearbox] == 'm'
          our_car_attributes[:transmission_id] = 2
        end

        if partner_car[:Fuel] == 'petrol'
          our_car_attributes[:engine_id] = 1
        elsif partner_car[:Fuel] == 'diesel' 
          our_car_attributes[:engine_id] = 2
        end

        result << ::Car.new(our_car_attributes)
 
      end

      #result.each do |t|
      #  puts(t.full_name)
      #end

      result
    end
  end
end
