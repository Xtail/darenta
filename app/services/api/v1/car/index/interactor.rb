require 'dry/transaction'

class Api::V1::Car::Index::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  AVALIABLE_LOCALES = %w(ru en ko)

  VALIDATOR = Dry::Validation.Form do
    optional(:name).maybe(:str?)
    optional(:locale).maybe(:str?)
    optional(:price_type).value(included_in?: %w(rub usd krw))
    optional(:price_gteq).maybe(:float?)
    optional(:price_lteq).maybe(:float?)
    optional(:is_current_offer).maybe(:bool?)
    optional(:is_delivery_to_airport).maybe(:bool?)
    optional(:is_delivery_to_you).maybe(:bool?)
    optional(:is_baby_chair).maybe(:bool?)
    optional(:is_wifi).maybe(:bool?)
    optional(:is_navigator).maybe(:bool?)
    optional(:body_id).maybe(:int?)
    optional(:engine_id).maybe(:int?)
    optional(:transmission_id).maybe(:int?)
    optional(:color_id).maybe(:int?)
    optional(:city_name).maybe(:str?)

    optional(:is_partner_cars).maybe(:bool?)
    optional(:date_from).maybe(:str?)
    optional(:date_to).maybe(:str?)
    optional(:platform).maybe(:str?)

    optional(:app).maybe(:str?)
  end

  TOP_RENT_A_CAR_VALIDATOR = Dry::Validation.Form do
    required(:date_from).filled(format?: ISO_8601_REGEX)
    required(:date_to).filled(format?: ISO_8601_REGEX)
    optional(:city_name).maybe(:str?)
  end

  step :merge_defaults
  step :cast
  step :validate
  step :check_partners
  step :resolve

  def merge_defaults(params)
    params[:price_type] = 'rub' unless params[:price_type].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Car::Index::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_partners(hash)
    hash = hash.to_h
    hash_tmp = hash
    hash = {}

    if not Rails.application.secrets.partners_enable
      hash_tmp[:is_partner_cars] = false
    end

    if hash_tmp[:is_partner_cars] == true
      result = TOP_RENT_A_CAR_VALIDATOR.call(hash_tmp.to_h)

      return Failure(result.errors) unless result.success?
      
      provider = "Api::V1::Car::Index::Partners::TopRentACar".constantize
      cars = provider.get_cars(result[:city_name], result[:date_from], result[:date_to])
      hash[:our_cars] = hash_tmp

      if cars.present?
        hash[:partner_cars] = cars
      else
        hash[:partner_cars] = {}
      end

    else
      hash[:our_cars] = hash_tmp
      hash[:partner_cars] = {}
    end

    Success(hash)
  end

  def resolve(hash)
    hash[:our_cars] = Api::V1::Car::Index::QueryBuilder.new(::Car.where(is_moderated: true)).call(hash[:our_cars])
    Success(hash)
  end
end
