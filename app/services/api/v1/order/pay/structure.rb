require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Order::Pay::Structure < Dry::Struct
  constructor_type :schema

  attribute :Order_ID, Types::Coercible::Int.optional
  attribute :Status, Types::Coercible::String.optional
  attribute :Signature, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.default('ru')
end
