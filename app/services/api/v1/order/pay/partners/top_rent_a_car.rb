require 'httparty'

class Api::V1::Order::Pay::Partners::TopRentACar
  include HTTParty

  base_uri 'http://toprentacar.bg/xml_gate/'


  class << self
    def make_order(request_rate, order_id, driver)
      headers = { Authorization: 'Basic ZGFyZW50YTpkQHJlbnRANTYzMw=='}

      body = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
        request = xml.Request do
          xml.NewReservationRequest do
            xml.RequestRate request_rate

            parnter = xml.Partner
            parnter.ConfirmationNumber = order_id

            xml.PickupAddress '...'
            xml.ReturnAddress '...'

            xml.Driver do
              xml.LastName driver.last_name
              xml.FirstName driver.first_name

              account = ::TenantAccount.find_by(tenant_id: driver.id)
              xml.Email account.email
              xml.CellTelephoneNumber driver.telephone
              xml.BirthDate '...'
              xml.Age '...'

              xml.Address do
                xml.Street '...'
                xml.City '...'
                xml.PostalCode '...'
                xml.CountryCode '...'
              end

            end

            xml.Extras do
            end

            xml.AdditionalInformation 'darenta test request'

            flight = xml.Flight
            flight.airlineCode ''
            flight.flightNumber ''
          end
        end
        request.referenceNumber="23de0a9186adf56b3e580b7eb2c9ab45"
        request.version="3.0"
        request.xmlns="http://toprentacar.bg/xml/"
      end

      options = { headers: headers, body: body.to_xml }

      #puts("xml", options[:body])
      response = post('/', options)
      #puts("response", response)

      return nil unless response.success?
      result = parse_response(response.body)

      result
    end

    def parse_response(response)
      result = nil

      xml_doc = Nokogiri::XML(response)
      xml_doc.search('Response NewReservationResponse').each do |t|

        is_confirmed = t.at('ReservationConfirmed')
        if is_confirmed.present?
          is_confirmed = is_confirmed.text
        end
        status = t.at('ReservationStatus')
        if status.present?
          status = status.text
        end

        if ((status == 'Available') && (is_confirmed == 'true'))
          result = 'ok'
        end
      end

      result
    end
  end
end
