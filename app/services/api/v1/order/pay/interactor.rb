require 'dry/transaction'

class Api::V1::Order::Pay::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  VALIDATOR = Dry::Validation.Form do
    required(:Order_ID).filled(:int?)
    required(:Status).maybe(:str?)
    required(:Signature).maybe(:str?)
    optional(:locale).value(included_in?: %w(ru en ko))
  end

  #step :symbolize
  step :merge_defaults
  step :cast
  step :validate
  step :check_order
  step :confirm_order
  step :redirect_order
  step :create_form
  step :mail_to_tenant
  step :create_tokens
  step :mail_to_landlord
  step :mail_to_administrator

  def symbolize(params)
    Success params.symbolize_keys
  end

  def merge_defaults(params)
    params[:locale] = 'ru' unless params[:locale].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Order::Pay::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_order(hash)
    ::Order.exists?(id: hash[:Order_ID]) ? Success(hash) : Failure({ Order_ID: ['not present in database'] })
  end

  def confirm_order(hash)
    is_saved = false
    if hash[:Status] == "paid" || hash[:Status] == "authorized"
      order = ::Order.find_by(id: hash[:Order_ID])
      if ::Order.new.generate_paid_signature(order, hash[:Status]) == hash[:Signature]
        order.is_paid = true
        order.paid_at = DateTime.now
        is_saved = order.save
      end 
    end
    is_saved ? Success(hash) : Failure({ status: ['not saved']})
  end

  def redirect_order(hash)
    hash = hash.to_h
    order = ::Order.find_by(id: hash[:Order_ID])
    hash[:redirected] = false
    return Success(hash) if not order.is_need_redirect

    # redirect

    driver = ::Tenant.find_by(id: order.tenant_id)

    provider = "Api::V1::Order::Pay::Partners::TopRentACar".constantize
    result = provider.make_order(order.partner_token, order.id, driver)

    return Failure("error from partner side") unless result != nil

    hash[:redirected] = true
    Success(hash)
  end

  def create_form(hash)
    order = ::Order.find_by(id: hash[:Order_ID])

    car = ::Car.find_by(id: order.car_id)
    tenant = ::Tenant.find_by(id: order.tenant_id)
    landlord = ::Tenant.find_by(id: order.landlord_id)

    form = {
        order: order,
        car: car,
        tenant: tenant,
        landlord: landlord,
        locale: order.locale
    }

    Success(form: form, attrs: hash)
  end

  def mail_to_tenant(hash)
    form = hash[:form]
    tenant = form[:tenant]

    if tenant.present?
      unless tenant.is_email_rejected
        OrdersNotifyTenantJob.perform_later(hash[:form])
      end
    end

    Success(hash)
  end

  def create_tokens(hash)
    return Success(hash) if hash[:form][:partner_car]

    form = hash[:form]
    order = form[:order]
    landlord = form[:landlord]
    account = ::TenantAccount.find_by(tenant_id: landlord.id)

    return Failure({ 'Account': ['not present in database'] }) unless account.present?

    order.accept_token = ::Order.new.generate_accept_token(account.email)
    order.decline_token = ::Order.new.generate_decline_token(account.email)
    order.save

    Success(hash)
  end

  def mail_to_landlord(hash)
    return Success(hash) if hash[:form][:partner_car]

    OrdersNotifyLandlordJob.perform_later(hash[:form])
    Success(hash)
  end

  def mail_to_administrator(hash)
    OrdersNotifyAdministratorJob.perform_later(hash[:form])
    Success(hash)
  end

end
