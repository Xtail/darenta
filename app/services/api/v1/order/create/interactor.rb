require 'dry/transaction'

class Api::V1::Order::Create::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  VALIDATOR = Dry::Validation.Form do
    required(:car_id).filled(:int?)
    required(:tenant_email).filled(format?: ::Regex::Email.new.plain)
    required(:date_from).filled(format?: ISO_8601_REGEX)
    required(:date_to).filled(format?: ISO_8601_REGEX)
    optional(:where_to_pickup).maybe(:str?)
    optional(:where_to_leave).maybe(:str?)
    optional(:discount).maybe(:str?)
    optional(:discount_type).maybe(:str?)
    optional(:locale).value(included_in?: %w(ru en ko))
  end

  step :symbolize
  step :merge_defaults
  step :cast
  step :validate
  step :check_car
  step :check_tenant
  step :create_order
  step :create_form
  step :mail_to_tenant
  step :create_tokens
  step :mail_to_landlord
  step :mail_to_administrator

  def symbolize(params)
    Success params.symbolize_keys
  end

  def merge_defaults(params)
    params[:locale] = 'ru' unless params[:locale].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Order::Create::Structure.new(params.to_h).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_car(hash)
    ::Car.exists?(id: hash[:car_id]) ? Success(hash) : Failure({ car_id: ['not present in database'] })
  end

  def check_tenant(hash)
    account = ::TenantAccount.find_by(email: hash[:tenant_email])
    tenant = ::Tenant.find_by(id: account.tenant_id)
    tenant.locale = hash[:locale]
    tenant.save
    tenant.is_blocked ? Failure({ tenant: ['is blocked'] }) : Success(hash)
  end

  def create_order(hash)
    hash = hash.to_h

    account = ::TenantAccount.find_by(email: hash[:tenant_email])
    tenant = ::Tenant.find_by(id: account.tenant_id)

    discount_id = create_discount(hash)

    price = calculate_price(hash)

    if discount_id.present?
      price = price / 2
    end

    price_currency = "rub"
    if hash[:locale] == "ru"
      price_currency = "rub"
    elsif hash[:locale] == "en"
      price_currency = "usd"
    elsif hash[:locale] == "ko"
      price_currency = "krw"
    end

    order_params = {:car_id => hash[:car_id],
                    :tenant_id => tenant.id,
                    :discount_id => discount_id,
                    :rent_from => hash[:date_from],
                    :rent_til => hash[:date_to],
                    :where_to_pickup => hash[:where_to_pickup],
                    :where_to_leave => hash[:where_to_leave],
                    :price_value => price,
                    :price_currency => price_currency,
                    :is_paid => false,
                    :paid_at => nil
    }

    order = ::Order.create(order_params)
    order.save

    hash[:price_value] = order.price_value
    hash[:price_currency] = order.price_currency
    hash[:order_uid] = order.id
    hash[:signature] = ::Order.new.generate_create_signature(order)
    hash[:tenant_id] = tenant.id
    #hash[:signature] = ::Order.new.generate_paid_signature(order, "paid")

    Success(hash)
  end

  def create_form(hash)
    order_uid = hash[:order_uid]
    date_from = hash[:date_from]
    date_to = hash[:date_to]
    where_to_pickup = hash[:where_to_pickup]
    where_to_leave = hash[where_to_leave]
    price_value = hash[:price_value]
    price_currency = hash[:price_currency]
    car_id = hash[:car_id]
    tenant_email = hash[:tenant_email]
    emails = []
    emails << tenant_email

    form = {
      order_uid: order_uid,
      locale: hash[:locale],
      date_from: date_from.to_s,
      date_to: date_to.to_s,
      where_to_pickup: where_to_pickup,
      where_to_leave: where_to_leave,
      price_value: price_value,
      price_currency: price_currency,
      car_id: car_id,
      tenant_email: tenant_email,
      tenant_emails: emails,
      tenant_id: hash[:tenant_id]
    }
    Success(form: form, attrs: hash)
  end

  def mail_to_tenant(hash)

    form = hash[:form]
    tenant_id = form[:tenant_id]

    tenant = ::Tenant.find_by(id: tenant_id)

    if not tenant.is_email_rejected
      OrdersNotifyTenantJob.perform_later(hash[:form])
      #OrderMailer.with(form: hash[:form]).notify_tenant.deliver_later
    end

    #OrderMailer.with(form: hash[:form]).notify_tenant(true).deliver_later
    Success(hash)
  end

  def create_tokens(hash)
    form = hash[:form]

    order_uid = form[:order_uid]
    order = ::Order.find_by(id: order_uid)

    car_id = order.car_id
    car = ::Car.find_by(id: car_id)

    user_id = car.user_id
    user = ::User.find_by(id: user_id)
    user_email = user.email

    accept_token = ::Order.new.generate_accept_token(user_email)
    decline_token = ::Order.new.generate_decline_token(user_email)

    form[:accept_token] = accept_token
    form[:decline_token] = decline_token

    order.accept_token = accept_token
    order.decline_token = decline_token
    order.save

    Success(hash)
  end

  def mail_to_landlord(hash)
    OrderMailer.with(form: hash[:form]).notify_landlord.deliver_later
    #OrderMailer.with(form: hash[:form]).notify_landlord(true).deliver_later
    Success(hash)
  end

  def mail_to_administrator(hash)
    OrderMailer.with(form: hash[:form]).notify_administrator.deliver_later
    Success(hash)
  end

  private

  def create_discount(hash)
    discount_id = nil
    if hash[:discount].present? && hash[:discount_type].present?
      if ::DiscountType.where(title: hash[:discount_type]).exists?
        discount_type = ::DiscountType.find_by(title: hash[:discount_type])
        discount_params = { :type_id => discount_type.id,
                            :value => hash[:discount]
        }
        discount = ::Discount.create(discount_params)
        discount.save
        discount_id = discount.id
      end
    end
    discount_id
  end

  def calculate_price(hash)
    rent_til = hash[:date_to]
    rent_from = hash[:date_from]

    car_price = 0.0
    if hash[:locale] == "ru"
      car_price = ::Car.find_by(id: hash[:car_id]).price_rub
    elsif hash[:locale] == "en"
      car_price = ::Car.find_by(id: hash[:car_id]).price_usd
    elsif hash[:locale] == "ko"
      car_price = ::Car.find_by(id: hash[:car_id]).price_krw
    end

    til = [Date.parse(rent_from), Date.parse(rent_til)].max
    from = [Date.parse(rent_from), Date.parse(rent_til)].min

    price = (til - from) * car_price * 0.01
    price
  end
end
