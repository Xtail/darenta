require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Order::My::Structure < Dry::Struct
  constructor_type :schema

  attribute :tenant_id, Types::Coercible::Int.optional
  attribute :locale, Types::Coercible::String.default('ru')
end
