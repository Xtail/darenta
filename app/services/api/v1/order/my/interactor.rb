require 'dry/transaction'

class Api::V1::Order::My::Interactor
  include Dry::Transaction

  ISO_8601_REGEX = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$/

  VALIDATOR = Dry::Validation.Form do
    required(:tenant_id).filled(:int?)
    optional(:locale).value(included_in?: %w(ru en ko))
  end

  #step :symbolize
  step :merge_defaults
  step :cast
  step :validate
  step :check_tenant
  step :get_orders

  def symbolize(params)
    Success params.symbolize_keys
  end

  def merge_defaults(params)
    params[:locale] = 'ru' unless params[:locale].present?
    Success params
  end

  def cast(params)
    Success Api::V1::Order::My::Structure.new(params).to_h
  end

  def validate(hash)
    result = VALIDATOR.call(hash)
    result.success? ? Success(result) : Failure(result.errors)
  end

  def check_tenant(hash)
    ::Tenant.exists?(id: hash[:tenant_id]) ? Success(hash) : Failure({ tenant_id: ['not present in database'] })
  end

  def get_orders(hash)
    tenant = ::Tenant.find_by(id: hash[:tenant_id])

    orders = ::Order.where(tenant_id: tenant.id)
    Success(orders: orders, attrs: hash)
  end

end
