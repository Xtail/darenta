require 'dry-types'
require 'dry-struct'

Dry::Types.load_extensions(:maybe)
module Types
  include Dry::Types.module
end

class Api::V1::Order::CreateCarRequest::Structure < Dry::Struct
  constructor_type :schema

  attribute :car_id, Types::Coercible::Int.optional
  attribute :tenant_email, Types::Coercible::String.optional
  attribute :date_from, Types::Coercible::String.optional
  attribute :date_to, Types::Coercible::String.optional
  attribute :where_to_pickup, Types::Coercible::String.optional
  attribute :where_to_leave, Types::Coercible::String.optional
  attribute :discount, Types::Coercible::String.optional
  attribute :discount_type, Types::Coercible::String.optional
  attribute :locale, Types::Coercible::String.default('ru')
end
