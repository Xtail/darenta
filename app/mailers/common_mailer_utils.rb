class CommonMailerUtils
    def self.resolve_source(raw_source, locale)
        if raw_source == 'site'
            if locale == 'ru'
                return 'c сайта'
            elsif locale == 'en'
                return 'site'
            elsif locale == 'ko'
                return '사이트'
            end
        elsif raw_source == 'api'
            if locale == 'ru'
                return 'от приложения'
            elsif locale == 'en'
                return 'application'
            elsif locale == 'ko'
                return '응용'
            end
        else
            if locale == 'ru'
                return 'с сайта'
            elsif locale == 'en'
                return 'site'
            elsif locale == 'ko'
                return '사이트'
            end
        end

        return 'site'
    end 
    
    def self.resolve_price_rub_by_locale(price_rub, locale)
        if locale == 'ru'
            return price_rub
        end

        if locale == 'en'
            return ::Currency.new.rub_to(price_rub, 'usd')
        end

        if locale == 'ko'
            return ::Currency.new.rub_to(price_rub, 'krw')
        end
    end

    def self.resolve_price_currency_by_locale(locale)
        if locale == 'ru'
            return 'rub'
        end

        if locale == 'en'
            return 'usd'
        end

        if locale == 'ko'
            return 'krw'
        end
    end
end