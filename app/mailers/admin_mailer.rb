class AdminMailer < ApplicationMailer
  ADMINISTRATOR_EMAIL = Rails.application.secrets.admin_email.freeze #info@darenta.ru

  def register_tenant
    @form = params[:form]
    @tenant_account = @form[:tenant_account]
    @tenant = @tenant_account.tenant
    @l = 'ru'
    @device = @form[:device]

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = ::MailMessage.find_by(key: 'admin_mailer.register_renter')
    subject = @message.pull_title_block(@l, 0)

    mail(to: ADMINISTRATOR_EMAIL, subject: subject)
  end

  def tenant_confirmed
    @form = params[:form]
    @tenant_account = @form[:tenant_account]
    @l = 'ru'

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = ::MailMessage.find_by(key: 'admin_mailer.renter_confirmed')
    subject = @message.pull_title_block(@l, 0)

    mail(to: ADMINISTRATOR_EMAIL, subject: subject)
  end

  def tenant_add_car_request
    @form = params[:form]
    @car = @form[:car]
    @tenant = @car.tenant
    @moderation_request_at = @form[:moderation_request_at]
    @l = 'ru'

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @link_to_car = Rails.application.secrets.domain_name + 'admin/cars/' + @car.id.to_s + '/edit'

    @message = ::MailMessage.find_by(key: 'admin_mailer.car_owner_add_car_request')
    subject = @message.pull_title_block(@l, 0)

    mail(to: ADMINISTRATOR_EMAIL, subject: subject)
  end

  def car_rent_feedback_notify_admin
    @form = params[:form]
    @send_at = @form[:send_at]
    @l = 'ru'

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = MailMessage.find_by(key: 'admin_mailer.car_rent_feedback_notify_admin')
    subject = @message.pull_title_block(@l, 0)

    mail(to: ADMINISTRATOR_EMAIL, subject: subject)
  end
end
