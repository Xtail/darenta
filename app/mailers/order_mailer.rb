class OrderMailer < ApplicationMailer
  ADMINISTRATOR_EMAIL = Rails.application.secrets.admin_email.freeze #info@darenta.ru
  TOP_RENT_A_CAR_EMAIL = 'alexey.toprentacar@gmail.com'


  def notify_tenant
    @form = params[:form]
    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = @form[:locale]

    @source = CommonMailerUtils.resolve_source(@order.source, @l)

    city = ::City.find_by(id: @car.city_id)
    if city.present?
      @city = city
    end

    if @order.price_currency == 'usd'
      @deposit = @car.deposit_usd
    else
      if @order.price_currency == 'eur'
        @deposit = @car.deposit_eur
      else
        if @order.price_currency == 'krw'
          @deposit = @car.deposit_krw
        else
          @deposit = @car.deposit_rub
        end
      end
    end

    @message = MailMessage.find_by(key: 'order_mailer.notify_renter')
    subject = @message.pull_title_block(@l, 0)

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def notify_landlord
    @form = params[:form]
    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = @form[:locale]
 
    @source = CommonMailerUtils.resolve_source(@order.source, @l)
    @deposit = @car.deposit_rub

    @message = MailMessage.find_by(key: 'order_mailer.notify_car_owner')
    subject = @message.pull_title_block(@l, 0)
 
    @decline_link_title = 'Decline'
    @accept_link_title = 'Accept'
    @days_count = 'days'
    if @l == 'ru'
      @decline_link_title = 'Отказать'
      @accept_link_title = 'Принять'
      @days_count = 'сут.'
    elsif @l == 'ko'
      @decline_link_title = '거절하다'
      @accept_link_title = '받아 들인다'
      @days_count = '일'
    end

    mail(to: @landlord.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def notify_administrator
    @form = params[:form]

    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = 'ru'
    @landlord_profile_link = Rails.application.secrets.domain_name + 'admin/renters/' + @tenant.id.to_s + '/edit'

    @source = CommonMailerUtils.resolve_source(@order.source, @l)

    city = ::City.find_by(id: @car.city_id)
    if city.present?
      @city = city
    end

    if @order.price_currency == 'usd'
      @deposit = @car.deposit_usd
    else
      if @order.price_currency == 'eur'
        @deposit = @car.deposit_eur
      else
        if @order.price_currency == 'krw'
          @deposit = @car.deposit_krw
        else
          @deposit = @car.deposit_rub
        end
      end
    end

    @message = MailMessage.find_by(key: 'order_mailer.notify_administrator')
    subject = @message.pull_title_block(@l, 0)

    mail(to: ADMINISTRATOR_EMAIL, subject: subject)
  end

  def notify_tenant_accept_order
    @form = params[:form]
    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = @form[:locale]
    @deposit = @car.deposit_rub

    @source = CommonMailerUtils.resolve_source(@order.source, @l)

    @message = ::MailMessage.find_by(key: 'order_mailer.notify_renter.accept_order')

    subject = @message.pull_title_block(@l, 0)

    @reject_link = @tenant.reject_link

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def notify_tenant_decline_order
    @form = params[:form]
    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = @form[:locale]

    @source = CommonMailerUtils.resolve_source(@order.source, @l)
    @message = ::MailMessage.find_by(key: 'order_mailer.notify_renter.decline_order')

    subject = @message.pull_title_block(@l, 0)

    @reject_link = @tenant.reject_link

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def notify_landlord_accept_order
    @form = params[:form]
    @order = @form[:order]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @landlord = @form[:landlord]
    @l = @form[:locale]

    raw_source = @order.source
    if raw_source == 'site'
      if @l == 'ru'
        @source = "сайта"
      elsif @l == 'en'
        @source = "site"
      elsif @l == 'ko'
        @source = "사이트"
      end
    elsif raw_source == 'api'
      if @l == 'ru'
        @source = "приложения"
      elsif @l == 'en'
        @source = "application"
      elsif @l == 'ko'
        @source = "응용"
      end
    else
      if @l == 'ru'
        @source = "сайта"
      elsif @l == 'en'
        @source = "site"
      elsif @l == 'ko'
        @source = "사이트"
      end
    end

    @message = ::MailMessage.find_by(key: 'order_mailer.notify_car_owner.accept_order')

    subject = @message.pull_title_block(@l, 0)

    @days_count = 'days'
    if @l == 'ru'
      @days_count = 'сут.'
    elsif @l == 'ko'
      @days_count = '일'
    end

    @reject_link = @landlord.reject_link
    @deposit = @car.deposit_rub

    mail(to: @landlord.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

end
