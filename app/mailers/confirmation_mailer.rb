class ConfirmationMailer < Devise::Mailer
  #default from: 'app@darenta.ru'
  default from: Rails.application.secrets.smtp_login
  ADMINISTRATOR_EMAIL = Rails.application.secrets.admin_email.freeze #info@darenta.ru

  def confirmation_instructions(record, token, opts={})
    @token = token
    @message = MailMessage.find_by(key: 'confirmation_mailer.notify_renter')

    tenant = Tenant.find_by(id: record.tenant_id)
    @name = tenant.t_first_name
    @reject_link = tenant.reject_link
    @l = tenant.locale

    raw_source = opts[:source]
    opts.delete(:source)
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @confirm_link_title = 'Confirmation link'
    if @l == 'ru'
      @confirm_link_title = 'Подтвердить адрес'
    elsif @l == 'ko'
      @confirm_link_title = '주소 확인'
    end

    opts[:subject] = @message.pull_title_block(@l, 0)
    opts[:bcc] = ADMINISTRATOR_EMAIL

    if record.last_confirmation_sent_at != nil
      # сколько времени прошло с последней отправки подтверждения
      delay_since_last_confirmation = DateTime.now.to_time - record.last_confirmation_sent_at.to_time

      # если прошло меньше 30 минут, не отправляем письмо
      if delay_since_last_confirmation < 1800
        return
      else
        record.last_confirmation_sent_at = DateTime.now
        record.save
      end
    end

    devise_mail(record, :confirmation_instructions, opts)
  end

  def add_sevice_instructions(record, token, opts={})
    @token = token
    @message = MailMessage.find_by(key: 'confirmation_mailer.add_service.notify_renter')

    tenant = Tenant.find_by(id: record.tenant_id)
    @name = tenant.t_first_name
    @provider = record.provider
    @reject_link = tenant.reject_link
    @l = tenant.locale

    raw_source = opts[:source]
    opts.delete(:source)
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @confirm_link_title = 'Confirmation link'
    if @l == 'ru'
      @confirm_link_title = 'Подтвердить адрес'
    elsif @l == 'ko'
      @confirm_link_title = '주소 확인'
    end

    opts[:subject] = @message.pull_title_block(@l, 0)
    opts[:bcc] = ADMINISTRATOR_EMAIL

    if record.last_confirmation_sent_at != nil
      # сколько времени прошло с последней отправки подтверждения
      delay_since_last_confirmation = DateTime.now.to_time - record.last_confirmation_sent_at.to_time

      # если прошло меньше 30 минут, не отправляем письмо
      if delay_since_last_confirmation < 1800
        return
      else
        record.last_confirmation_sent_at = DateTime.now
        record.save
      end
    end

    devise_mail(record, :add_service_instructions, opts)
  end

  def welcome_instructions
    @form = params[:form]
    @tenant_account = @form[:tenant_account]
    @tenant = @tenant_account.tenant
    @l = @tenant.locale

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = ::MailMessage.find_by(key: 'confirmation_mailer.welcome_instructions.notify_renter')

    subject = @message.pull_title_block(@l, 0)

    @reject_link = @tenant.reject_link

    mail(to: @tenant_account.email, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

end