class FeedbackMailer < ApplicationMailer
  RECIPIENT = Rails.application.secrets.admin_email.freeze

  def notify
    @form = params[:form]

    @form[:locale] = I18n.locale.to_s

    @device_type = DeviceType.find_by(id: @form[:device_type_id])
    @message = MailMessage.find_by(key: 'feedback_mailer.notify')
    @l = @form[:locale]
  
    @source = CommonMailerUtils.resolve_source('api', @l)

    subject = @message.pull_title_block(@l, 0)

    mail(to: RECIPIENT, subject: subject, content_type: "text/html")
  end
end
