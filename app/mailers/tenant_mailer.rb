class TenantMailer < ApplicationMailer
  ADMINISTRATOR_EMAIL = Rails.application.secrets.admin_email.freeze #info@darenta.ru

  def reset_password_notify
    @form = params[:form]

    recipient = @form[:recipient]
    @l = @form[:locale]
    @reset_password_link = @form[:reset_password_link]

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = MailMessage.find_by(key: 'renter_mailer.reset_password')
    subject = @message.pull_title_block(@l, 0)

    mail(to: recipient, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def update_personal_info_notify
    @form = params[:form]
    @tenant = @form[:tenant]
    recipient = @form[:recipient]
    @l = @form[:locale]

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @message = MailMessage.find_by(key: 'renter_mailer.personal_info_updated')
    subject = @message.pull_title_block(@l, 0)

    mail(to: recipient, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def update_car_notify
    @form = params[:form]
    @car = @form[:car]
    @tenant = @car.tenant
    @l = @tenant.locale

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @reject_link_title = 'Unsubscribe'
    if @l == 'ru'
      @reject_link_title = 'Отказаться от рассылки'
    elsif @l == 'ko'
      @reject_link_title = '메일 링리스트 구독 취소'
    end

    @message = ::MailMessage.find_by(key: 'renter_mailer.update_car.notify_car_owner')
    subject = @message.pull_title_block(@l, 0)

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def car_moderation_accepted
    @form = params[:form]
    @car = @form[:car]
    @tenant = @car.tenant
    @l = @tenant.locale

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @reject_link_title = 'Unsubscribe'
    if @l == 'ru'
      @reject_link_title = 'Отказаться от рассылки'
    elsif @l == 'ko'
      @reject_link_title = '메일 링리스트 구독 취소'
    end

    @message = MailMessage.find_by(key: 'renter_mailer.car_moderation_accepted.notify_car_owner')
    subject = @message.pull_title_block(@l, 0)

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def car_rent_tenant_feedback
    @form = params[:form]
    @car = @form[:car]
    @tenant = @form[:tenant]
    @l = @tenant.locale

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @reject_link_title = 'Unsubscribe'
    if @l == 'ru'
      @reject_link_title = 'Отказаться от рассылки'
    elsif @l == 'ko'
      @reject_link_title = '메일 링리스트 구독 취소'
    end

    @message = MailMessage.find_by(key: 'renter_mailer.car_rent_renter_feedback')
    subject = @message.pull_title_block(@l, 0)

    mail(to: @tenant.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end

  def car_rent_landlord_feedback
    @form = params[:form]
    @car = @form[:car]
    @landlord = @car.tenant
    @l = @landlord.locale
    @order = @form[:order]

    raw_source = @form[:source]
    @source = CommonMailerUtils.resolve_source(raw_source, @l)

    @reject_link_title = 'Unsubscribe'
    if @l == 'ru'
      @reject_link_title = 'Отказаться от рассылки'
    elsif @l == 'ko'
      @reject_link_title = '메일 링리스트 구독 취소'
    end

    @message = MailMessage.find_by(key: 'renter_mailer.car_rent_car_owner_feedback')
    subject = @message.pull_title_block(@l, 0)

    mail(to: @landlord.emails, subject: subject, bcc: ADMINISTRATOR_EMAIL)
  end
end
