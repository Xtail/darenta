class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.secrets.smtp_login
  layout 'mailer'
end
