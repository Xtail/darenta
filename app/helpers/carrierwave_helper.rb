module CarrierwaveHelper
  def absolute_image_link(relative_path)
    base_uri = "#{request.protocol}#{request.host_with_port}"
    relative_path.present? ? "#{base_uri}#{relative_path}" : nil
  end
end
