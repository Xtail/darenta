class Admin::SamplerDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def edit_link_icon
    link = link_to (fa_stacked_icon('fa-circle-thin pencil', base: 'circle')), edit_admin_sampler_path(self.id), title: 'Редактировать'
    content_tag :div, link, class: 'manage-icon inline'
  end

  def destroy_link_icon
    link = link_to (fa_stacked_icon('fa-circle-thin trash', base: 'circle')), admin_sampler_path(self.id),
                   method: :delete, title: 'Удалить',
                   data: { confirm: 'Вы точно хотите удалить данную модель автомобилей?' }
    content_tag :div, link, class: 'manage-icon inline'
  end
end