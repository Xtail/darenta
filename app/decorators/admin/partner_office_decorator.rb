class Admin::PartnerOfficeDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def edit_link_icon
    link = link_to (fa_stacked_icon('fa-circle-thin pencil', base: 'circle')), edit_admin_partner_office_path(self.id), title: 'Редактировать'
    content_tag :div, link, class: 'manage-icon inline'
  end

  def destroy_link_icon
    link = link_to (fa_stacked_icon('fa-circle-thin trash', base: 'circle')), admin_partner_office_path(self.id),
                   method: :delete, title: 'Удалить',
                   data: { confirm: 'Вы точно хотите удалить данный Офис?' }
    content_tag :div, link, class: 'manage-icon inline'
  end

  def check_boolean(attr_name)
    icon_name = send(attr_name) ? 'check-square' : 'minus-square'
    color = send(attr_name) ? 'green' : 'red'
    fa_icon(icon_name, class: color)
  end
end
