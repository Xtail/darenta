json.array! @colors do |color|
  json.id color.id
  json.name do
    json.ru color.name_ru
    json.en color.name_en
    json.ko color.name_ko
  end
end
