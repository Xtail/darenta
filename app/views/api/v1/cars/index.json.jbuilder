json.array! @cars do |car|
  json.type 'our'
  json.id car.id
  json.full_name car.full_name
  json.mark_name car.mark_name
  json.sampler_name car.sampler_name
  json.production_year car.production_year
  json.dynamic_link car.dynamic_link
  json.price do
    json.rub car.price_rub
    json.usd car.price_usd
    json.krw car.price_krw
    json.eur car.price_eur
  end
  json.detailed_price do
    json.day do
      json.rub car.price_rub
      json.usd car.price_usd
      json.krw car.price_krw
      json.eur car.price_eur
    end
    json.few_days do
      json.rub car.price_few_days_rub
      json.usd car.price_few_days_usd
      json.krw car.price_few_days_krw
      json.eur car.price_few_days_eur
    end
    json.week do
      json.rub car.price_week_rub
      json.usd car.price_week_usd
      json.krw car.price_week_krw
      json.eur car.price_week_eur
    end
    json.month do
      json.rub car.price_month_rub
      json.usd car.price_month_usd
      json.krw car.price_month_krw
      json.eur car.price_month_eur
    end
  end
  json.deposit do
    json.rub car.deposit_rub
    json.usd car.deposit_usd
    json.krw car.deposit_krw
    json.eur car.deposit_eur
  end
  json.image car.first_image
  json.address do
    json.latitude car.latitude
    json.longitude car.longitude
  end
  json.is_current_offer car.is_current_offer
  json.is_delivery_to_airport car.is_delivery_to_airport
  json.is_delivery_to_you car.is_delivery_to_you
  json.is_baby_chair car.is_baby_chair
  json.is_wifi car.is_wifi
  json.is_navigator car.is_navigator
  json.relationships do
    json.owner do
      json.id car.tenant.id
      json.avatar absolute_image_link(car&.tenant&.avatar&.url(:small))
      json.avatar_detailed do
        json.original absolute_image_link(car&.tenant&.avatar&.url)
        json.small absolute_image_link(car&.tenant&.avatar&.url(:small))
        json.medium absolute_image_link(car&.tenant&.avatar&.url(:medium))
        json.big absolute_image_link(car&.tenant&.avatar&.url(:big))
      end
      json.full_name do
        json.ru car.tenant.t_first_name_ru&.strip
        json.en car.tenant.t_first_name_en&.strip
        json.ko car.tenant.t_first_name_ko&.strip
      end
    end
    json.city do
      json.id car.city.id
      json.name do
        json.ru car.city.name_ru
        json.en car.city.name_en
        json.ko car.city.name_ko
      end
    end

    if car.color_id.present?
      json.color do
        json.id car.color.id
        json.name do
          json.ru car.color.name_ru
          json.en car.color.name_en
          json.ko car.color.name_ko
        end
      end
    else
      json.color nil
    end

    json.body do
      json.id car.body.id
      json.name do
        json.ru car.body.name_ru
        json.en car.body.name_en
        json.ko car.body.name_ko
      end
    end
    json.engine do
      json.id car.engine.id
      json.name do
        json.ru car.engine.name_ru
        json.en car.engine.name_en
        json.ko car.engine.name_ko
      end
    end
    json.transmission do
      json.id car.transmission.id
      json.name do
        json.ru car.transmission.name_ru
        json.en car.transmission.name_en
        json.ko car.transmission.name_ko
      end
    end
  end
  json.insurance_images car.insurance_images
  json.contract_images car.contract_images
  json.images do
    json.array! car.images
  end
  json.contracts_images do
    json.array! car.contracts
  end  
  json.insurances do
    json.array! car.get_insurances
  end
  json.comments car.comments
end

json.array! @partner_cars do |car|
  if car.location_code != ''    
    json.rate_id car.rate_id
    json.acriss do
      json.accriss_code car.acriss_code

      acriss = AcrissCode.find_by(acriss_code: car.acriss_code)
      if acriss.present?
        json.group acriss.group
        json.type_of_car acriss.type_of_car
        json.year_of_production acriss.year_of_production
        json.seats acriss.seats
        json.doors acriss.doors
        json.air_condition acriss.air_condition
        json.deposit acriss.deposit
        json.guaranteed acriss.guaranteed
        json.deposit_eur acriss.deposit_eur
        json.cross_border_fee acriss.cross_border_fee
        json.image absolute_image_link(acriss.image_url)
      end
      
    end
    json.availability car.availability
    json.net_rate car.net_rate
    json.delivery car.delivery
    json.out_of_hours car.out_of_hours
    json.total_cost car.total_cost
    json.currency_code car.currency_code
    json.car_name car.car_name
    json.seats car.seats
    json.doors car.doors
    json.gearbox car.gearbox
    json.bags_big car.bags_big
    json.air_con car.air_con
    json.wheel_drive car.wheel_drive
    json.fuel car.fuel
    json.request_rate car.request_rate

    json.type 'partner'
    json.id Random.new.rand(1000..2000).to_i
    json.full_name car.car_name
    json.token car.request_rate


    acriss = AcrissCode.find_by(acriss_code: car.acriss_code)
    if acriss.present?
      year = acriss.year_of_production.to_i
      if year > 0
        json.production_year year
      else
        json.production_year Time.current.year
      end
      json.image absolute_image_link(acriss.image_url)
      json.deposit do
        deposit_eur = acriss.deposit_eur.to_f
        deposit_rub = PartnerCar.new.rub_from_eur(deposit_eur)
        json.rub deposit_rub
        json.usd PartnerCar.new.rub_to_usd(deposit_rub.to_f)
        json.krw PartnerCar.new.rub_to_krw(deposit_rub.to_f)
        json.eur deposit_eur
      end
    else
      json.production_year Time.current.year
      json.image nil
      json.deposit do
        json.rub nil
        json.usd nil
        json.krw nil
        json.eur nil
      end
    end
    json.price do
      json.rub car.price_rub
      json.usd car.price_usd
      json.krw car.price_krw
      json.eur car.price_eur
    end
    json.is_current_offer true
    json.is_delivery_to_airport false
    json.is_delivery_to_you false
    json.is_baby_chair false
    json.is_wifi false
    json.is_navigator true
    json.relationships do
      json.owner do
        user = Tenant.find_by(TenantAccount.find_by(email: "alexey.toprentacar@gmail.com").tenant_id)
        json.id user.id
        json.avatar absolute_image_link(user.avatar_url)
        json.full_name do
          json.ru user.t_first_name_ru&.strip
          json.en user.t_first_name_en&.strip
          json.ko user.t_first_name_ko&.strip
        end
      end
      city = City.find_by(location_code: car.location_code)
      if city.present?
        json.city do
          json.id city.id
          json.name do
            json.ru city.name_ru
            json.en city.name_en
            json.ko city.name_ko
          end
        end
        json.address do
          json.latitude city.latitude
          json.longitude city.longitude
        end
      else
        json.city do
          json.id nil
          json.name do
            json.ru nil
            json.en nil
            json.ko nil
          end
        end
        json.address do
          json.latitude '1'
          json.longitude '1'
        end
      end

      json.color nil

      json.body do
        body = Body.find_by(id: 1)
        json.id body.id
        json.name do
          json.ru body.name_ru
          json.en body.name_en
          json.ko body.name_ko
        end
      end
      
      json.engine do
        json.id car.engine_id
        engine = Engine.find_by(id: car.engine_id)
        if engine.present?
          json.name do
            json.ru engine.name_ru
            json.en engine.name_en
            json.ko engine.name_ko
          end
        end
      end
      
      json.transmission do
        json.id car.transmission_id
        transmission = Transmission.find_by(id: car.transmission_id)
        if transmission.present?
          json.name do
            json.ru transmission.name_ru
            json.en transmission.name_en
            json.ko transmission.name_ko
          end
        end
      end
    end
  end
end
