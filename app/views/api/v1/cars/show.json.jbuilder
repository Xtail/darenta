json.id @car.id
json.full_name @car.full_name
json.mark_name @car.mark_name
json.sampler_name @car.sampler_name
json.production_year @car.production_year
json.dynamic_link @car.dynamic_link
json.is_moderated @car.is_moderated
json.price do
  json.rub @car.price_rub
  json.usd @car.price_usd
  json.krw @car.price_krw
  json.eur @car.price_eur
end
json.detailed_price do
  json.day do
    json.rub @car.price_rub
    json.usd @car.price_usd
    json.krw @car.price_krw
    json.eur @car.price_eur
  end
  json.few_days do
    json.rub @car.price_few_days_rub
    json.usd @car.price_few_days_usd
    json.krw @car.price_few_days_krw
    json.eur @car.price_few_days_eur
  end
  json.week do
    json.rub @car.price_week_rub
    json.usd @car.price_week_usd
    json.krw @car.price_week_krw
    json.eur @car.price_week_eur
  end
  json.month do
    json.rub @car.price_month_rub
    json.usd @car.price_month_usd
    json.krw @car.price_month_krw
    json.eur @car.price_month_eur
  end
end
json.deposit do
  json.rub @car.deposit_rub
  json.usd @car.deposit_usd
  json.krw @car.deposit_krw
  json.eur @car.deposit_eur
end
json.image @car.first_image
json.address do
  json.latitude @car.latitude
  json.longitude @car.longitude
end
json.is_current_offer @car.is_current_offer
json.is_delivery_to_airport @car.is_delivery_to_airport
json.is_delivery_to_you @car.is_delivery_to_you
json.is_baby_chair @car.is_baby_chair
json.is_wifi @car.is_wifi
json.is_navigator @car.is_navigator
json.relationships do
  json.owner do
    json.id @car.tenant.id
    json.avatar absolute_image_link(@car&.tenant&.avatar&.url(:small))
    json.avatar_detailed do
      json.original absolute_image_link(@car&.tenant&.avatar&.url)
      json.small absolute_image_link(@car&.tenant&.avatar&.url(:small))
      json.medium absolute_image_link(@car&.tenant&.avatar&.url(:medium))
      json.big absolute_image_link(@car&.tenant&.avatar&.url(:big))
    end
    json.full_name do
      json.ru @car.tenant.t_first_name_ru&.strip
      json.en @car.tenant.t_first_name_en&.strip
      json.ko @car.tenant.t_first_name_ko&.strip
    end
  end
  json.city do
    json.id @car.city.id
    json.name do
      json.ru @car.city.name_ru
      json.en @car.city.name_en
      json.ko @car.city.name_ko
    end
  end

  if @car.color_id.present?
    json.color do
      json.id @car.color.id
      json.name do
        json.ru @car.color.name_ru
        json.en @car.color.name_en
        json.ko @car.color.name_ko
      end
    end
  else
    json.color nil
  end

  json.body do
    json.id @car.body.id
    json.name do
      json.ru @car.body.name_ru
      json.en @car.body.name_en
      json.ko @car.body.name_ko
    end
  end
  json.engine do
    json.id @car.engine.id
    json.name do
      json.ru @car.engine.name_ru
      json.en @car.engine.name_en
      json.ko @car.engine.name_ko
    end
  end
  json.transmission do
    json.id @car.transmission.id
    json.name do
      json.ru @car.transmission.name_ru
      json.en @car.transmission.name_en
      json.ko @car.transmission.name_ko
    end
  end
end
json.insurance_images @car.insurance_images
json.contract_images @car.contract_images
json.images do
  json.array! @car.images
end
json.contracts_images do
  json.array! @car.contracts
end  
json.insurances do
  json.array! @car.get_insurances
end
json.comments @car.comments