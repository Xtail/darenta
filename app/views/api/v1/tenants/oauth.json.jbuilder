json.provider @current_account.provider
json.id @user.id
json.email @current_account.email
json.unconfirmed_email @current_account.unconfirmed_email
json.first_name @user.t_first_name
json.last_name @user.t_last_name
json.middle_name @user.t_middle_name
json.gender @user.gender
json.telephone @user.telephone
json.darenta_uid @user.darenta_uid
json.locale @user.locale
json.avatar do
  json.original absolute_image_link(@user&.avatar&.url)
  json.small absolute_image_link(@user&.avatar&.url(:small))
  json.medium absolute_image_link(@user&.avatar&.url(:medium))
  json.big absolute_image_link(@user&.avatar&.url(:big))
end
json.email_verify @current_account.active_for_authentication?
json.tenant_emails do
    json.array! @accounts.each do |a|
        json.provider a.provider
        json.email a.email
        json.verify a.active_for_authentication?
    end
end
