json.provider @current_account.provider
json.id @tenant.id
json.email @current_account.email
json.unconfirmed_email @current_account.unconfirmed_email
json.first_name @tenant.t_first_name
json.last_name @tenant.t_last_name
json.middle_name @tenant.t_middle_name
json.gender @tenant.gender
json.telephone @tenant.telephone
json.darenta_uid @tenant.darenta_uid
json.locale @tenant.locale
json.avatar do
  json.original  absolute_image_link(@tenant&.avatar&.url)
  json.small absolute_image_link(@tenant&.avatar&.url(:small))
  json.medium absolute_image_link(@tenant&.avatar&.url(:medium))
  json.big absolute_image_link(@tenant&.avatar&.url(:big))
end
json.email_verify @current_account.active_for_authentication?
json.tenant_emails do
    json.array! @accounts.each do |a|
        json.provider a.provider
        json.email a.email
        json.verify a.active_for_authentication?
        json.unconfirmed_email a.unconfirmed_email
    end
end
