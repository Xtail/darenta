json.tenant_orders do
    json.array! @orders.each do |order|
        json.order_id order.id
        json.car_id order.car_id

        car = Car.find_by(id: order.car_id)
        json.car_name car.full_name

        json.price_value order.price_value
        json.price_currency order.price_currency
        json.where_to_pickup order.where_to_pickup
        json.where_to_leave order.where_to_leave
        json.date_from order.rent_from.to_s
        json.date_to order.rent_til.to_s
        json.is_paid order.is_paid
        json.is_accepted order.is_accepted
        json.is_declined order.is_declined
    end
end
