json.array! @engines do |engine|
  json.id engine.id
  json.name do
    json.ru engine.name_ru
    json.en engine.name_en
    json.ko engine.name_ko
  end
end
