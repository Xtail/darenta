json.id @res.id
json.url absolute_image_link(@res&.file&.url(:medium))
json.size do
    json.original absolute_image_link(@res&.file&.url)
    json.thumb absolute_image_link(@res&.file&.url(:medium))
    json.medium absolute_image_link(@res&.file&.url(:medium))
end
json.identifier @res.file_identifier
