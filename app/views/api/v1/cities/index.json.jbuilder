json.array! @cities do |city|
  json.id city.id
  json.name do
    json.ru city.name_ru
    json.en city.name_en
    json.ko city.name_ko
  end
end
