json.array! @samplers do |sampler|
  json.id sampler.id
  json.mark_id sampler.mark_id
  json.name sampler.name
end
