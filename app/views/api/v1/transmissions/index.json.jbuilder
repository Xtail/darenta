json.array! @transmissions do |transmission|
  json.id transmission.id
  json.name do
    json.ru transmission.name_ru
    json.en transmission.name_en
    json.ko transmission.name_ko
  end
end
