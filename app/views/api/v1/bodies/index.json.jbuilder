json.array! @bodies do |body|
  json.id body.id
  json.name do
    json.ru body.name_ru
    json.en body.name_en
    json.ko body.name_ko
  end
end
