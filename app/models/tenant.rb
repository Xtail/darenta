class Tenant < ApplicationRecord
  translates :t_first_name, :t_last_name, :t_middle_name, :unconfirmed_t_first_name, :unconfirmed_t_last_name, :unconfirmed_t_middle_name, :gender, :unconfirmed_gender
  globalize_accessors locales: I18n.available_locales, attributes: [:t_first_name, :t_last_name, :t_middle_name, :unconfirmed_t_first_name, :unconfirmed_t_last_name, :unconfirmed_t_middle_name, :gender, :unconfirmed_gender]
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_many :cars, dependent: :destroy
  has_many :tenant_accounts, dependent: :destroy
  has_many :tenant_translations, dependent: :delete_all

  validates :t_first_name, presence: true
  #validates :t_last_name, presence: true

  mount_uploader :avatar, AvatarUploader

  def find_by_client_and_token(client, token)
    tenant = nil
    Tenant.find_each do |t|
      tokens = t.tokens
      tokens = tokens.to_h

      if tokens.has_key?(client)
        if BCrypt::Password.new(tokens[client]["token"]) == token
          tenant = t
          break
        end
      end
    end
    tenant
  end

  def generate_reject_token(emails)
    token = "reject_email" + emails.join('&')
    token = Digest::MD5.hexdigest(token).upcase
    token
  end

  def reject_link

    emails = []
    accounts = TenantAccount.where(tenant_id: self.id)
    accounts.each do |account|
      emails << account.email
    end

    token = generate_reject_token(emails)

    tenant = Tenant.find_by(id: self.id)
    tenant.email_reject_token = token
    tenant.save

    link = Rails.application.secrets.domain_name + "renters/reject_email?id=" + self.id.to_s + "&token=" + token
    link
  end

  def last_name
    self.t_last_name
  end

  def first_name
    self.t_first_name
  end

  def middle_name
    self.t_middle_name
  end

  def emails
    accounts = TenantAccount.where(tenant_id: self.id)
    emails = []
    accounts.each do |account|
      unless emails.include? account
        emails << account.email
      end
    end
    emails
  end

  def emails_string
    emails = self.emails
    emails.join(', ')
  end

  def contacts(locale)
    result = ''

    locale = locale.downcase
    case locale
    when 'en'
      tenant_emails = emails
      if tenant_emails.length > 1
        result += 'email addresses: ' + emails_string
      else
        if tenant_emails.length == 1
          result += 'email address: ' + emails_string
        end
      end

      if self.telephone.present?
        result += ' or phone ' + self.telephone
      end
    when 'ko'
      tenant_emails = emails
      if tenant_emails.length > 1
        result += '이메일 주소 ' + emails_string
      else
        if tenant_emails.length == 1
          result += '이메일 주소 ' + emails_string
        end
      end

      if self.telephone.present?
        result += ' 전화 중 하나 ' + self.telephone
      end
    else
      tenant_emails = emails
      if tenant_emails.length > 1
        result += 'адреса электронной почты: ' + emails_string
      else
        if tenant_emails.length == 1
          result += 'адрес электронной почты: ' + emails_string
        end
      end

      if self.telephone.present?
        result += ' или телефон ' + self.telephone
      end
    end
    result
  end

  def name_with_email
    accounts = TenantAccount.where(tenant_id: self.id)
    emails = []
    accounts.each do |account|
      emails << account.email
    end
    emails = emails.join(',')
    "#{t_first_name} [#{emails}]"
  end

  def name_with_email_and_provider
    accounts = TenantAccount.where(tenant_id: self.id)
    emails = []
    accounts.each do |account|
      confirmed = ''
      if account.confirmed_at == nil
        confirmed = 'Н'
      else
        confirmed = 'П'
      end
      emails << account.email + '(' + confirmed+ ', ' +  account.provider + ')'
    end
    emails = emails.join(', ')
    "#{t_first_name} #{t_middle_name} #{t_last_name} [#{emails}]"
  end

  def emails_with_provider
    accounts = TenantAccount.where(tenant_id: self.id)
    addresses = []
    accounts.each do |account|
      confirmed = ""
      if account.confirmed_at == nil
        confirmed = "Н"
      else
        confirmed = "П"
      end
      addresses << "#{account.email}" + " (" + confirmed+ ", " + "#{account.provider}" + ")"
    end
    addresses.join(', ')
  end

  def full_name
    "#{t_first_name} #{t_middle_name} #{t_last_name}"
  end

  def update_last_sign_in
    accounts = ::TenantAccount.where(tenant_id: self.id)

    accounts.each do |account|
      account.sign_in_count = account.sign_in_count + 1
      account.last_sign_in_at = DateTime.now
      account.save
    end
  end

  def last_sign_in
    accounts = ::TenantAccount.where(tenant_id: self.id)

    sign_in_times = []
    accounts.each do |account|
      if account.sign_in_count > 0
        sign_in_times << account.last_sign_in_at
      end
    end

    sign_in_times = sign_in_times.sort
    return sign_in_times.last
  end

end
