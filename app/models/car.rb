class Car < ApplicationRecord
  validates :mark_name, presence: true
  validates :sampler_name, presence: true
  #validates :address, presence: true
  validates :engine_id, presence: true, numericality: true
  validates :transmission_id, presence: true, numericality: true
  validates :city_id, presence: true, numericality: true
  validates :tenant_id, presence: true, numericality: true
  validates :production_year, presence: true, numericality: true
  validates :price_rub, presence: true, numericality: true

  belongs_to :color, optional: true
  belongs_to :engine
  belongs_to :transmission
  belongs_to :body
  belongs_to :city
  belongs_to :tenant

  before_save :resolve_full_name
  after_save :resolve_dynamic_link

  geocoded_by :address, lookup: :yandex
  after_validation :geocode, if: -> (obj) { obj.latitude.blank? && obj.longitude.blank? }

  # deprecated
  mount_uploader :image, CarImageUploader
  mount_uploaders :insurance_images, CarSupportImagesUploader
  mount_uploaders :contract_images, CarSupportImagesUploader

  def first_image
    imgs = images
    if imgs.empty?
      return nil
    end

    first_image = imgs.first
    unless first_image.present?
      return nil
    end
    
    return first_image
  end

  def images
    images_by_field("images_ids")
  end

  def contracts
    images_by_field("contract_images_ids") 
  end

  def get_insurances
    result = []

    raw_insurances = self.insurances
    unless raw_insurances.present?
      return result
    end

    unless raw_insurances.kind_of?(Array)
      return []
    end


    raw_insurances.each do |insurance|
      images = []

      images_ids = insurance["images_ids"]
      if images_ids.present? && images_ids.kind_of?(Array)
        images_ids.each do |id|
          res = ::Resource.find_by(id: id)
          if res.present?
            img = {
              id: res.id,
              url: Rails.application.secrets.full_host + res&.file&.url(:medium),
              original: Rails.application.secrets.full_host + res&.file&.url,
              medium: Rails.application.secrets.full_host + res&.file&.url(:medium)
            }
            images << img
          end 
        end
      end

      insurance_item = {
        name: insurance["name"],
        images: images
      }  
      result << insurance_item
    end

    return result
  end

  def friendly_price(currency = 'rub')
    price_origin = send("price_#{currency}")
    price = price_origin * 100
    if price % 100 == 0
      return price_origin.to_i
    else
      return price_origin
    end
  end

  def price_by_dates(date_from, date_to, currency = 'rub')
    days_count = (Date.parse(date_to.to_s) - Date.parse(date_from.to_s)).to_i

    if days_count >= 30
      return days_count * send("price_month_#{currency}")
    end

    if days_count >= 7
      return days_count * send("price_week_#{currency}")
    end

    if days_count >= 2
      return days_count * send("price_few_days_#{currency}")
    end

    days_count * send("price_#{currency}")
  end

  def daily_price_by_dates(date_from, date_to, currency = 'rub')
    days_count = (Date.parse(date_to.to_s) - Date.parse(date_from.to_s)).to_i

    if days_count >= 30
      return send("price_month_#{currency}")
    end

    if days_count >= 7
      return send("price_week_#{currency}")
    end

    if days_count >= 2
      return send("price_few_days_#{currency}")
    end

    send("price_#{currency}")
  end

  def convert_price(price, origin_currency, target_currency)
    origin_currency = origin_currency.upcase
    target_currency = target_currency.upcase

    price_rub = rub_from(price, origin_currency)
    target_price = rub_to(price_rub, target_currency)
    return target_price
  end

  private

  def resolve_full_name
    self.full_name = "#{mark_name} #{sampler_name}"
  end

  def resolve_dynamic_link
    if (not self.dynamic_link.present?) || (self.dynamic_link.empty?)
      car_id = self.id
      dynamic_link = build_dynamic_link(car_id)
      self.dynamic_link = dynamic_link
      self.save
    end
  end

  def build_dynamic_link(car_id)
    api_key = Rails.application.secrets.dynamic_link_api_key
    base_uri = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=' + api_key

    require 'httparty'
    require 'json'

    body = {
        dynamicLinkInfo: {
            dynamicLinkDomain: "es5zs.app.goo.gl",
            link: Rails.application.secrets.full_host + "/cars/" + car_id.to_s + "/?id=" + car_id.to_s,
            androidInfo: {
                androidPackageName: "com.www.darenta",
                androidFallbackLink: "https://play.google.com/store/apps/details?id=com.www.darenta",
                androidMinPackageVersionCode: "0"
            },
            iosInfo: {
                iosBundleId: "ru.darenta.main",
                iosFallbackLink: "https://itunes.apple.com/app/apple-store/id963427615",
                iosCustomScheme: "",
                iosIpadFallbackLink: "",
                iosIpadBundleId: "",
                iosAppStoreId: "963427615"
            },
            navigationInfo: {
                enableForcedRedirect: false
            },
            socialMetaTagInfo: {
                socialTitle: "Darenta",
                socialDescription: "Аренда автомобилей от частных лиц",
                socialImageLink: ""
            }
        },
        suffix: {
            option: "SHORT"
        }
    }

    response = HTTParty.post(base_uri, :headers => {'Content-Type'=>'application/json'}, :body => body.to_json).body
    response_hash = JSON.parse(response).to_h
    dynamic_link = response_hash["shortLink"]
    if dynamic_link.present?
      return dynamic_link
    else
      return 'empty'
    end
  end

  def images_by_field(field_name)
    result = []

    #puts("field", self."#{ field_name }")
    raw_ids = send("#{field_name}")
    unless raw_ids.present?
      return []
    end

    unless raw_ids.kind_of?(Array)
      return []
    end

    raw_ids.each do |id|
      res = ::Resource.find_by(id: id)
      if res.present?
        img = {
          id: res.id,
          url: Rails.application.secrets.full_host + res&.file&.url(:medium),
          original: Rails.application.secrets.full_host + res&.file&.url,
          medium: Rails.application.secrets.full_host + res&.file&.url(:medium)
        }

        result << img
      end
    end

    return result
  end

  def rub_from(value, currency_code)
    ::Currency.new.rub_from(value, currency_code)
  end

  def rub_to(value, currency_code)
    ::Currency.new.rub_to(value, currency_code)
  end
end
