class Resource < ApplicationRecord
  mount_uploader :file, ResourceUploader

  def ids_to_json_array(ids_arr)
    begin
      result = JSON.parse(ids_arr)
      unless result.kind_of?(Array)
        return []
      end
      result.to_json
    rescue JSON::ParserError
      result = []  
    end

    return result
  end
end