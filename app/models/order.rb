class Order < ActiveRecord::Base

    def generate_create_signature(order)
      create_signature(order.id, order.price_value)
    end

    def create_signature(order_id, order_price)
      shop_idp = Rails.application.secrets.Shop_IDP
      order_idp = order_id
      subtotal_p = order_price.round(2)
      mean_type = ""
      e_money_type = ""
      lifetime = ""
      customer_idp = ""
      card_idp = ""
      i_data = ""
      pt_code = ""
      password = Rails.application.secrets.password

      subtotal_p = convert_cell(subtotal_p)

      signature_src = [
          Digest::MD5.hexdigest(shop_idp),
          Digest::MD5.hexdigest(order_idp.to_s),
          Digest::MD5.hexdigest(subtotal_p.to_s),
          Digest::MD5.hexdigest(mean_type),
          Digest::MD5.hexdigest(e_money_type),
          Digest::MD5.hexdigest(lifetime),
          Digest::MD5.hexdigest(customer_idp),
          Digest::MD5.hexdigest(card_idp),
          Digest::MD5.hexdigest(i_data),
          Digest::MD5.hexdigest(pt_code),
          Digest::MD5.hexdigest(password)
      ]

      signature = signature_src.join('&')


      signature = Digest::MD5.hexdigest(signature).upcase
      signature
    end

    def generate_paid_signature(order, status)
        order_idp = order.id.to_s
        password = Rails.application.secrets.password

        signature = order_idp + status + password
        signature = Digest::MD5.hexdigest(signature).upcase
        signature
    end

    def convert_cell(cell)
        if cell.is_a?(Float)
          i = cell.to_i
          cell == i.to_f ? i : cell
        else
          cell
        end
    end

    def generate_accept_token(email)
      time = DateTime.now.to_s
      token = "accept" + time + email
      token = Digest::MD5.hexdigest(token).upcase
      token
    end

    def generate_decline_token(email)
      time = DateTime.now.to_s
      token = "decline" + time + email
      token = Digest::MD5.hexdigest(token).upcase
      token
    end

    def calculate_price(price, date_from, date_to)
      til = [Date.parse(date_from), Date.parse(date_to)].max
      from = [Date.parse(date_from), Date.parse(date_to)].min

      rent_price = (til - from) * price
      rent_price.round(2)
    end

    def calculate_comission(day_price, date_from, date_to)
      total_price = calculate_price(day_price, date_from, date_to)
      comission = total_price * 0.01
      comission.round(2)
    end

    def apply_discount(price)
      (price / 2).round(2)
    end

    def decorated_price_value(locale = 'ru')
      raw_price = ::CommonMailerUtils.resolve_price_rub_by_locale(self.price_value, locale)

      if(raw_price >= 1000)
        result = (raw_price / 1000).to_i.to_s + " " + (raw_price % 1000).round(2).to_s
      else
        result = raw_price.round(2).to_s
      end

      result
    end
end