class Mark < ApplicationRecord
  has_many :samplers, dependent: :destroy
end