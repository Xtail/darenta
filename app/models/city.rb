class City < ApplicationRecord
  translates :name
  globalize_accessors locales: I18n.available_locales, attributes: [:name]
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_many :cars, dependent: :destroy
end
