class User < ApplicationRecord
  translates :first_name, :last_name, :middle_name, :short_name, :full_name
  globalize_accessors locales: I18n.available_locales, attributes: [:first_name, :last_name, :middle_name, :short_name, :full_name]
  accepts_nested_attributes_for :translations, allow_destroy: true

  validates :email, presence: true, uniqueness: true

  #has_many :cars, dependent: :destroy
  has_many :user_translations, dependent: :delete_all

  before_save :resolve_short_name
  before_save :resolve_full_name

  mount_uploader :avatar, AvatarUploader

  def name_with_email
    "#{short_name} [#{email}]"
  end
  
  private

  def resolve_short_name
    I18n.available_locales.each do |locale|
      self.send("short_name_#{locale}=", "#{send("last_name_#{locale}")} #{send("first_name_#{locale}")}")
    end
  end

  def resolve_full_name
    I18n.available_locales.each do |locale|
      self.send("full_name_#{locale}=", "#{send("last_name_#{locale}")} #{send("first_name_#{locale}")} #{send("middle_name_#{locale}")}")
    end
  end
end
