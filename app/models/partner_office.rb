class PartnerOffice < ApplicationRecord
  translates :name, :address, :supplier_direction_to_desk
  globalize_accessors locales: I18n.available_locales, attributes: [:name, :address, :supplier_direction_to_desk]
  accepts_nested_attributes_for :translations, allow_destroy: true
end
