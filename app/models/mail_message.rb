class MailMessage < ApplicationRecord
  validates :key, presence: true, uniqueness: true
  validates :title, presence: true

  def pull_mail_subject(key, locale)
    message = MailMessage.find_by(key: key)
    message.title
  end

  # new variant

  def pull_title_block(locale, index)
    pull_title(locale).split("§")[index]
  end

  def pull_title(locale)
    json = self.message_title

    unless json.present?
      return ""
    end

    require 'json'
    hash = JSON.parse(json)

    res = ""
    hash[locale].keys.each do |t|
      res += String(hash[locale][t]) + String("§")
    end
    res = res.first(-1)
    res
  end

  def pull_body_block(locale, index, reject_link = '')
    block = pull_body(locale).split("§")[index]

    reject_link_title = 'Unsubscribe'
    if locale == 'ru'
      reject_link_title = 'Отказаться от рассылки'
    elsif locale == 'ko'
      reject_link_title = '메일 링리스트 구독 취소'
    end
    reject_link_pattern = "<a href=\"#{reject_link}\">#{reject_link_title}</a>"

    unless block.present?
      return ""
    end

    block.gsub("cancel_subscription_key", reject_link_pattern)
  end

  def pull_body(locale)
    json = self.message_body

    unless json.present?
      return ""
    end

    require 'json'
    hash = JSON.parse(json)

    res = ""
    hash[locale].keys.each do |t|
      res += String(hash[locale][t]) + String("§")
    end
    res = res.first(-1)
    res
  end

  def pull_footer_block(locale, index)
    pull_footer(locale).split("§")[index]
  end

  def pull_footer(locale)
    json = self.message_footer

    unless json.present?
      return ""
    end

    require 'json'
    hash = JSON.parse(json)

    res = ""
    hash[locale].keys.each do |t|
      res += String(hash[locale][t]) + String("§")
    end
    res = res.first(-1)
    res
  end

end