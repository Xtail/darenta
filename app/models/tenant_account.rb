class TenantAccount < ApplicationRecord

  devise :async, :confirmable, :database_authenticatable, :registerable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User
  devise :omniauthable, omniauth_providers: %i[facebook google]

  belongs_to :tenant

  validates :email, presence: true, uniqueness: true
  validates :uid, presence: true, uniqueness: true
  validates :provider, presence: true

  def find_by_client_and_token(client, token)
    tenant_account = nil
    TenantAccount.find_each do |t|
      tokens = t.tokens
      tokens = tokens.to_h
      if tokens.has_key?(client)
        if BCrypt::Password.new(tokens[client]["token"]) == token
          tenant_account = t
          break
        end
      end
    end
    tenant_account
  end

  def verify_password?(password)
    self.valid_password?(password)
  end

  # авторизация через социальные сети
  def self.from_omniauth(auth)
    if auth[:provider] == 'google'
      info = auth[:info]
      tenant_account = TenantAccount.where(uid: info[:email]).first
    else
      tenant_account = TenantAccount.where(uid: auth[:uid]).first
    end

    unless tenant_account.present?
      info = auth[:info]
      name = info[:name]

      # check email
      existing_account = TenantAccount.find_by(email: info[:email])
      if existing_account.present?
        return nil
      end

      #create tenant
      tenant_attributes = {
          provider: auth[:provider],
          t_first_name: name.split(' ').first,
          t_last_name: name.split(' ').last,
          telephone: "",
          avatar: info[:image]
      }

      tenant = Tenant.new(tenant_attributes)
      if tenant.save
        tenant.locale = 'en'
        tenant.save
        # create tenant_account

        if auth[:provider] == 'google'
          tenant_account_attributes = {
              uid: info[:email],
              email: info[:email],
              tenant_id: tenant.id,
              provider: auth[:provider],
              password: Devise.friendly_token[0,6]
              #confirmed_at: Time.now.utc # skip confirmation
          }
        else
          tenant_account_attributes = {
              uid: auth[:uid],
              email: info[:email],
              tenant_id: tenant.id,
              provider: auth[:provider],
              password: Devise.friendly_token[0,6]
              #confirmed_at: Time.now.utc # skip confirmation
          }
        end

        tenant_account = TenantAccount.create(tenant_account_attributes)
        TenantsConfirmationJob.perform_later(tenant_account.id)

        form = {
            tenant_account: tenant_account,
            device: 'site',
            source: 'site'
        }
        AdminsRegisterTenantJob.perform_later(form)
      end
    end

    tenant_account
  end

  # добавление сервиса
  def self.from_omniauth_add_service(auth, current_account_id)

    current_tenant_account = TenantAccount.find_by(id: current_account_id)

    unless current_tenant_account.present?
      tenant_account = from_omniauth(auth)
      return tenant_account
    end

    if auth[:provider] == 'google'
      info = auth[:info]
      new_tenant_account = TenantAccount.where(uid: info[:email]).first
    else
      new_tenant_account = TenantAccount.where(uid: auth[:uid]).first
    end

    if new_tenant_account.present?

      previous_tenant_id = new_tenant_account.tenant_id

      new_tenant_account.tenant_id = current_tenant_account.tenant_id
      new_tenant_account.confirmed_at = nil
      new_tenant_account.save
      #new_tenant_account.send_reconfirmation_instructions
      TenantsConfirmationJob.perform_later(new_tenant_account.id, 'site')

      cars = Car.where(tenant_id: previous_tenant_id)
      cars.each do |car|
        car.tenant_id = current_tenant_account.tenant_id
        car.save
      end

      accounts_check = TenantAccount.where(tenant_id: previous_tenant_id)
      cars_check = Car.where(tenant_id: previous_tenant_id)

      if accounts_check.empty? && cars_check.empty?
        tenant = Tenant.find_by(id: previous_tenant_id)
        if tenant.present?
          tenant.delete
        end
      end

      return current_tenant_account
    end

    info = auth[:info]

    # check email
    existing_account = TenantAccount.find_by(email: info[:email])
    if existing_account.present?
      return nil
    end

    if auth[:provider] == 'google'
      new_tenant_account_attributes = {
          uid: info[:email],
          email: info[:email],
          tenant_id: current_tenant_account.tenant_id,
          provider: auth[:provider],
          password: Devise.friendly_token[0,6],
          confirmation_sent_from: 'site'
      }
    else
      new_tenant_account_attributes = {
          uid: auth[:uid],
          email: info[:email],
          tenant_id: current_tenant_account.tenant_id,
          provider: auth[:provider],
          password: Devise.friendly_token[0,6],
          confirmation_sent_from: 'site'
      }
    end

    new_tenant_account = TenantAccount.create(new_tenant_account_attributes)
    if new_tenant_account.present?
      TenantsConfirmationJob.perform_later(new_tenant_account.id, 'site')
    end

    current_tenant_account
  end

  # регистрация
  def self.new_with_session(params, session)
    puts("new with session")
    #puts("new with session", params, session)
    #if session["devise.tenant_account_attributes"]
    #  new(session["devise.teanant_account_attributes"], without_protection: true) do |user|
    #    tenant_attributes = {
    #        first_name: "123"
    #    }

    #   tenant = Tenant.create(tenant_attributes)
    #    if tenant.save
    #      account_attributes = {
    #          email: params.email,
    #          password: params.password,
    #          uid: params.email,
    #          tenant_id: tenant.id
    #      }
    #      user.attributes = account_attributes
    #      user.valid?
    #    end
    #  end
    #else
    #  super
    #end
  end

end
