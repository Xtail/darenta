class TenantsController < SiteController
    skip_before_action :authenticate_tenant_account!, :only => [:reject_email, :resend_confirmation]

    def index
        # stub
    end

    def new
        # stub
    end

    def show
        # stub
    end

    def edit
        # stub
    end

    def create
        # stub
    end

    def update
        @tenant = Tenant.find_by(id: current_tenant_account.tenant_id)
        new_attributes = tenant_params

        new_t_first_name = new_attributes[:t_first_name]
        if new_t_first_name.present? && @tenant.t_first_name != new_t_first_name
          @tenant.unconfirmed_t_first_name = new_t_first_name
        end

        new_t_last_name = new_attributes[:t_last_name]
        if new_t_last_name.present? && @tenant.t_last_name != new_t_last_name
          @tenant.unconfirmed_t_last_name = new_t_last_name
        end

        new_telephone = new_attributes[:telephone]
        if new_telephone.present? && @tenant.telephone != new_telephone
          @tenant.unconfirmed_telephone = new_telephone
        end

        @tenant.save

        recipient = [current_tenant_account.email]
        accounts = TenantAccount.where(tenant_id: @tenant.id)
        accounts.each do |account|
            if account.email != current_tenant_account.email
                recipient << account.email
            end
        end

        form = {
            tenant: @tenant.t_first_name,
            recipient: recipient,
            locale: @tenant.locale,
            source: 'site'
        }

        TenantMailer.with(form: form).update_personal_info_notify.deliver_later

        #@tenant.update_attributes(tenant_params)
        redirect_to request.referrer if @tenant.errors.blank?
    end

    def destroy
        # stub
    end

    def resend_confirmation
        provider = params[:provider]
        tenant_id = current_tenant_account.tenant_id

        account = TenantAccount.find_by(tenant_id: tenant_id, provider: provider)
        if account.present?
            #account.send_reconfirmation_instructions
            TenantsConfirmationJob.perform_later(account.id, 'site')
        end
        redirect_to request.referrer
    end

    def tenant_params
        params.require(:tenant).permit!.except(:id)
    end

    def reject_email
        tenant_id = params[:id]
        token = params[:token]

        tenant = Tenant.find_by(id: tenant_id)

        if tenant.email_reject_token == token
            tenant.is_email_rejected = !tenant.is_email_rejected
            tenant.save
        else
            render plain: "Error!"
        end
    end
end