class SiteController < ApplicationController
  layout 'site'
  before_action :authenticate_tenant_account!
end
