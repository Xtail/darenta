class TenantAccountsController < SiteController
  skip_before_action :authenticate_tenant_account!, :only => [:password_reset_form, :password_reset, :password_reset_successful, :password_reset_error]

  def password_reset_form
    @secret_hash = params[:token]
  end

  def password_reset
    secret_hash = params[:token]
    new_password_encrypted = params[:new_password]

    if secret_hash.present?
      tenant_account = TenantAccount.find_by(reset_password_hash: secret_hash)
      if tenant_account.present?
        tenant_account.password = new_password_encrypted
        tenant_account.reset_password_hash = nil
        tenant_account.save
        redirect_to '/password_reset_successful'
      else
        redirect_to '/password_reset_error'
      end
    else
      redirect_to '/password_reset_error'
    end
  end

  def password_reset_successful

  end

  def password_reset_error

  end
end