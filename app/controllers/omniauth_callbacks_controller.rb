class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all
    if current_tenant_account.present?
      tenant_account = TenantAccount.from_omniauth_add_service(request.env["omniauth.auth"], current_tenant_account.id)
    else
      tenant_account = TenantAccount.from_omniauth(request.env["omniauth.auth"])
    end

    if tenant_account.present?
      if tenant_account.persisted?
        sign_in tenant_account
        redirect_to '/'
      else
        super
        #session["devise.tenant_account_attributes"] = tenant_account.attributes
        #redirect_to new_tenant_account_registration_url
      end
    else
      render 'errors/email_already_exist.html'
    end
  end

    def facebook
      all
    end

    def google
      all
    end
  end
