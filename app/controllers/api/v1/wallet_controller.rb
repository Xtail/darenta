class Api::V1::WalletController < ActionController::Base
    def generate_pkpass
        puts("params", params)


        pass_json = '{
            "description": "description",
            "formatVersion": 1,
            "organizationName": "Metro",
            "passTypeIdentifier": "pass.com.metro.cc",
            "serialNumber":"' + params[:serialNumber] + '",
            "teamIdentifier": "8698LP9UTV"
        }'

        pass = Passbook::PKPass.new pass_json
        pass.addFiles [
            Rails.root.join('logo.png'),
            Rails.root.join('logo@2x.png'),
            Rails.root.join('icon.png'),
            Rails.root.join('icon@2x.png')
        ]

        #pkpass = pass.file
        #send_file pkpass.path, type: 'application/vnd.apple.pkpass', disposition: 'attachment', filename: "pass.pkpass"

        pkpass = pass.stream
        send_data pkpass.string, type: 'application/vnd.apple.pkpass', disposition: 'attachment', filename: "pass.pkpass"

        #render json: { pkpass: pkpass.string }
    end
end