class Api::V1::EnginesController < Api::V1::BaseController
  def index
    @engines = Engine.all.with_translations.order(id: :asc)
  end
end
