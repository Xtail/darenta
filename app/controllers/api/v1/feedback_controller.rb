class Api::V1::FeedbackController < Api::V1::BaseController
  before_action -> { tenant_account_token_auth(skip_confirmation: true) }, only: [:create_alt]

  def create
    Api::V1::Feedback::Create::Interactor.new.call(feedback_params) do |m|
      m.success do
        render json: { status: :ok }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def create_alt
    Api::V1::Feedback::CreateAlt::Interactor.new.call(feedback_params.to_h.merge({ tenant_account_id: current_tenant_account.id })) do |m|
      m.success do
        render json: { status: :ok }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  private

  def feedback_params
    params_ = params.permit!
    params_['app'] = request.headers['app']
    params_
  end
end
