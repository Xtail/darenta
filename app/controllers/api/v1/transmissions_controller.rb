class Api::V1::TransmissionsController < Api::V1::BaseController
  def index
    @transmissions = Transmission.all.with_translations.order(id: :asc)
  end
end
