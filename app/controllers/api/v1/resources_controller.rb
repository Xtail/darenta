class Api::V1::ResourcesController < Api::V1::BaseController
  before_action -> { tenant_account_token_auth(skip_confirmation: true) }, only: [:load_file]

  def load_file
    Api::V1::Resource::LoadFile::Interactor.new.call(_params) do |m|
      m.success do |response|
        @res = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  private

  def _params
    params.permit!
  end
end