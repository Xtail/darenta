class Api::V1::CarsController < Api::V1::BaseController
  def index
    Api::V1::Car::Index::Interactor.new.call(car_params) do |m|
      m.success do |response|
        @cars = response[:our_cars]
        @partner_cars = response[:partner_cars]
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def show
    Api::V1::Car::Show::Interactor.new.call(car_params) do |m|
      m.success do |response|
        @car = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  private

  def car_params
    params_ = params.permit!
    params_['app'] = request.headers['app']
    params_
  end
end
