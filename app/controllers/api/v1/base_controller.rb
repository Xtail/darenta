class Api::V1::BaseController < ApiController
  def current_tenant_account
    current_api_v1_tenant_account
  end

  def tenant_account_token_auth(skip_confirmation: false)
    authenticate_api_v1_tenant_account!

    unless skip_confirmation
      if current_tenant_account.present?
        error_not_confirmed unless current_tenant_account.active_for_authentication?
      end
    end
  end

  def error_not_confirmed
    render_error(423, I18n.t('devise_token_auth.sessions.not_confirmed', email: current_tenant_account.email))
  end
end
