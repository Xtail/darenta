class Api::V1::OrdersController < Api::V1::BaseController
  def create
    parameters = order_params.to_h
    parameters = parameters.merge({ tenant_email: current_tenant_account.email }) if current_tenant_account.present?

    Api::V1::Order::Create::Interactor.new.call(parameters) do |m|
      m.success do |responce|
        render json: { status: :ok }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def create_car_request
    parameters = order_params.to_h
    parameters = parameters.merge({ tenant_email: current_tenant_account.email }) if current_tenant_account.present?

    Api::V1::Order::CreateCarRequest::Interactor.new.call(parameters) do |m|
      m.success do |responce|
        render json: { status: :ok,
                       uid: responce[:order_uid],
                       price_value: responce[:price_value],
                       price_currency: responce[:price_currency],
                       signature: responce[:signature]
        }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def create_partner_car_request
    parameters = order_params.to_h
    parameters = parameters.merge({ tenant_email: current_tenant_account.email }) if current_tenant_account.present?

    Api::V1::Order::CreatePartnerCarRequest::Interactor.new.call(parameters) do |m|
      m.success do |responce|
        render json: { status: :ok,
                       uid: responce[:order_uid],
                       price_value: responce[:price_value],
                       price_currency: responce[:price_currency],
                       signature: responce[:signature]
        }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def pay
    Api::V1::Order::Pay::Interactor.new.call(order_params) do |m|
      m.success do
        render json: { status: :ok }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def my
    parameters = order_params.to_h
    parameters = parameters.merge({ tenant_id: current_tenant_account.tenant_id }) if current_tenant_account.present?

    Api::V1::Order::My::Interactor.new.call(parameters) do |m|
      m.success do |responce|
        orders = responce[:orders]
        @orders = orders.sort { |order| order.updated_at}
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  private

  def order_params
    params_ = params.permit!
    params_['app'] = request.headers['app']
    params_
  end
end
