class Api::V1::TenantsController < Api::V1::BaseController
  before_action -> { tenant_account_token_auth(skip_confirmation: true) }, :set_locale,
                only: [:me, :resend_confirmation, :add_push_token,
                       :my_cars, :create_car, :create_car2, :update_car, :update_car_2, :remove_car, :update_password]

  def me
    if request.get?
      @current_account = current_tenant_account
      @tenant = ::Tenant.find_by(id: @current_account.tenant_id)
      @accounts = ::TenantAccount.where(tenant_id: @tenant.id)
    else
      Api::V1::Tenant::Update::Interactor.new.call(tenant_params.to_h.merge({ tenant_id: current_tenant_account.tenant_id, provider: current_tenant_account.provider })) do |m|
        m.success do |account|
          @current_account = account
          @tenant = ::Tenant.find_by(id: @current_account.tenant_id)
          @accounts = ::TenantAccount.where(tenant_id: @tenant.id)
        end

        m.failure do |responce|
          render json: { status: :error, errors: responce }, status: :bad_request
        end
      end
    end
  end

  def oauth
    Api::V1::Tenant::Oauth::Interactor.new.call(tenant_params) do |m|
      m.success do |account|
        ::Rails.logger.debug "controller #{account.id}, #{account.tenant_id}"

        auth_headers = Api::V1::Tenant::Oauth::TokenManager.generate(account, request.headers['client'])
        response.headers.merge!(auth_headers)

        @user = ::Tenant.find_by(id: account.tenant_id)
        @current_account = account
        @accounts = ::TenantAccount.where(tenant_id: @user.id)
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def auth
    Api::V1::Tenant::Auth::Interactor.new.call(tenant_params) do |m|
      m.success do |account|
        auth_headers = Api::V1::Tenant::Auth::TokenManager.generate(account, request.headers['client'])
        response.headers.merge!(auth_headers)

        @user = ::Tenant.find_by(id: account.tenant_id)
        @current_account = account
        @accounts = ::TenantAccount.where(tenant_id: @user.id)
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def resend_confirmation
    Api::V1::Tenant::Resend_Confirmation::Interactor.new.call(tenant_params.to_h.merge({ tenant_id: current_tenant_account.tenant_id })) do |m|
      m.success do
        render json: { status: :ok }
      end

      m.failure do |responce|
        render json: { status: :error, errors: responce }, status: :bad_request
      end
    end
  end

  def add_push_token
    puts(params)
    render json: { status: :ok }
  end

  def my_cars
    Api::V1::Tenant::MyCars::Interactor.new.call(tenant_params.to_h.merge({ current_tenant_id: current_tenant_account.tenant_id })) do |m|
      m.success do |response|
        @cars = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def create_car
    Api::V1::Tenant::CreateCar::Interactor.new.call(tenant_params.to_h.merge({ current_tenant_id: current_tenant_account.tenant_id })) do |m|
      m.success do |response|
        @car = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def create_car_2
    Api::V1::Tenant::CreateCar2::Interactor.new.call(tenant_params.to_h.merge({ current_tenant_id: current_tenant_account.tenant_id })) do |m|
      m.success do |response|
        @car = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def update_car
    Api::V1::Tenant::UpdateCar::Interactor.new.call(tenant_params.to_h) do |m|
      m.success do |response|
        @car = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def update_car_2
    Api::V1::Tenant::UpdateCar2::Interactor.new.call(tenant_params.to_h) do |m|
      m.success do |response|
        @car = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def remove_car
    Api::V1::Tenant::RemoveCar::Interactor.new.call(tenant_params.to_h) do |m|
      m.success do |response|
        render json: { status: :ok }
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def update_password
    Api::V1::Tenant::UpdatePassword::Interactor.new.call(tenant_params.to_h.merge({ tenant_account_id: current_tenant_account.id })) do |m|
      m.success do |response|
        render json: { status: :ok }
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  def reset_password
    Api::V1::Tenant::ResetPassword::Interactor.new.call(tenant_params.to_h) do |m|
      m.success do |response|
        render json: { status: :ok }
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  private

  def tenant_params
    params_ = params.permit!
    params_['app'] = request.headers['app']
    params_
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
end
