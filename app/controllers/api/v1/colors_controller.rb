class Api::V1::ColorsController < Api::V1::BaseController
  def index
    @colors = Color.all.with_translations.order(id: :asc)
  end
end
