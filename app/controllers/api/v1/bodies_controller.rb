class Api::V1::BodiesController < Api::V1::BaseController
  def index
    @bodies = Body.all.with_translations.order(id: :asc)
  end
end
