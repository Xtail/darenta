class Api::V1::SamplersController < Api::V1::BaseController
  def index
    Api::V1::Sampler::Index::Interactor.new.call(sampler_params) do |m|
      m.success do |response|
        @samplers = response
      end

      m.failure do |response|
        render json: { status: :error, errors: response }, status: :bad_request
      end
    end
  end

  private

  def sampler_params
    params_ = params.permit!
    params_['app'] = request.headers['app']
    params_
  end
end
