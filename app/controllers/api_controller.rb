class ApiController < ApplicationController
  include DeviseTokenAuth::Concerns::SetUserByToken

  protect_from_forgery with: :null_session

  before_action :set_json_accept

  protected

  def render_error(status, message, data = nil)
    response = {
        errors: [message]
    }

    response = response.merge(data) if data
    render json: response, status: status
  end
  
  private

  def set_json_accept
    request.format = :json
  end
end
