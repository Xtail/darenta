class CarsController < SiteController
  skip_before_action :authenticate_tenant_account!, :only => [:index, :show]

  def index
    @cars = Car.where(is_moderated: true)
  end

  def new
    @car = Car.new
  end

  def show
    @car = Car.find_by(id: params[:id])
  end

  def edit
    # stub
  end

  def create
    # stub
  end

  def update
    # stub
  end

  def destroy
    # stub
  end
end
