class ConfirmationsController < Devise::ConfirmationsController

  private

  def after_confirmation_path_for(resource_name, resource)
    form = {
        tenant_account: resource,
        source: resource.confirmation_sent_from
    }
    TenantsMailerWelcomeInstructionsJob.perform_later(form)
    AdminsTenantConfirmedJob.perform_later(form)

    Rails.application.secrets.full_host + '/confirm_successful.html?locale=' + resource.tenant.locale
  end

end