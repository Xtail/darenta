class TestsController < ApplicationController
  def test_xml
    #@cars = Car.all
    #render xml: @cars

    #test = "<cars type=\"array\"><car><id type=\"integer\">14</id><image><url type=\"uploader\">/uploads/car/14/image/bfc2227b0a37847d9d80fbf668047d6f.jpg</url></image><is-delivery-to-airport type=\"boolean\">true</is-delivery-to-airport><is-delivery-to-you type=\"boolean\">true</is-delivery-to-you><is-baby-chair type=\"boolean\">false</is-baby-chair><is-wifi type=\"boolean\">false</is-wifi><is-navigator type=\"boolean\">false</is-navigator><color-id type=\"integer\">1</color-id><engine-id type=\"integer\">1</engine-id><transmission-id type=\"integer\">1</transmission-id><city-id type=\"integer\">1</city-id><user-id type=\"integer\">9</user-id><name nil=\"true\"/><mark-name>Toyota </mark-name><sampler-name>Corolla</sampler-name><full-name>Toyota  Corolla</full-name><body-id type=\"integer\">1</body-id><production-year type=\"integer\">2007</production-year><is-current-offer type=\"boolean\">true</is-current-offer><address>Преображенская площадь, Москва, Россия</address><latitude type=\"float\">1.0</latitude><longitude type=\"float\">1.0</longitude><price-rub type=\"float\">3375.0</price-rub><price-usd type=\"float\">15.0</price-usd><link-to-car/><price-krw type=\"float\">0.0</price-krw><created-at type=\"dateTime\">2018-07-05T11:28:11Z</created-at><updated-at type=\"dateTime\">2018-07-18T17:38:18Z</updated-at></car></cars>"
    #@copy = Nokogiri::XML(test)

    #@xml_doc  = Nokogiri::XML("<root><aliens><alien><name>Alf</name></alien></aliens></root>")

    #@xml_doc.xpath('//alien').each do |char_element|
    #    puts char_element.text
    #end

    #render plain: @xml_doc

    builder = Nokogiri::XML::Builder.new(:encoding => 'utf-8') do |xml|
      request = xml.Request do
        xml.ResRates do
          xml.Partner.Code="darenta"

          pickup = xml.Pickup
          pickup.dateTime="2018-07-26T10:00:00"
          pickup.locationCode="SOF"

          ret = xml.Return
          ret.dateTime="2018-07-28T10:00:00"
          ret.locationCode="SOF"
        end
      end
      request.referenceNumber="23de0a9186adf56b3e580b7eb2c9ab45"
      request.version="3.0"
      request.xmlns="http://toprentacar.bg/xml/"
    end

    #.set_attribute('Partner', 'Code=\"darenta\"')

    xml_string = builder.to_xml
    render xml: xml_string
  end

  def test_email_views

    puts("test started")

    #puts("confirmations")
    #TenantsConfirmationJob.perform_later(8)

    #puts("add service")
    #TenantsAddServiceJob.perform_later(8)

    #puts("feedback")
    #form = {
    #    device_type_id: 1,
    #    locale: 'ru',
    #    name: 'Name',
    #    email: 'test@email.ru',
    #    text: 'Hello World!'
    #}
    #FeedbackMailer.with(form: form).notify.deliver_now

    puts("order")
    order = Order.find_by(id: 44)
    form = {
        order_uid: order.id,
        car_id: order.car_id,
        car_name: order.car_name,
        tenant_id: order.tenant_id,
        tenant_full_name: order.tenant_full_name,
        tenant_first_name: order.tenant_first_name,
        tenant_last_name: order.tenant_last_name,
        tenant_email: JSON.parse(order.tenant_accounts),
        tenant_emails: JSON.parse(order.tenant_accounts),
        landlord_id: order.landlord_id,
        date_from: order.rent_from.to_s,
        date_to: order.rent_til.to_s,
        where_to_pickup: order.where_to_pickup,
        where_to_leave: order.where_to_leave,
        price_value: order.price_value,
        price_currency: order.price_currency,
        partner_car: order.is_need_redirect,
        rent_price: order.price_value,
        locale: 'ru',
        accept_token: ::Order.new.generate_accept_token('xtail1996@yandex.ru'),
        decline_token: ::Order.new.generate_decline_token('xtail1996@yandex.ru')
    }

    puts("tenant")
    OrdersNotifyTenantJob.perform_later(form)
    puts("landlord")
    OrdersNotifyLandlordJob.perform_later(form)
    #puts("admin")
    #OrdersNotifyAdministratorJob.perform_later(form)

    #puts("update personal info")
    #form = {
    #    tenant: 'Name',
    #    recipient: 'xtail1996@yandex.ru',
    #    locale: 'ru'
    #}
    #TenantMailer.with(form: form).update_personal_info_notify.deliver_later

    #puts("reconfirmation")
    #account = TenantAccount.find_by(id: 8)
    #if account.present?
    #  account.send_reconfirmation_instructions
    #end

    #puts("reset password")
    #form = {
    #    recipient: 'xtail1996@yandex.ru',
    #    locale: 'ru',
    #    reset_password_link: '/localhost:3000'
    #}
    #TenantsResetPasswordJob.perform_later(form)

    #puts("create car")
    #form = {
    #    recipient: 'xtail1996@yandex.ru',
    #    locale: 'ru',
    #    car_name: 'Renault Duster',
    #    tenant_name: 'Name'
    #}
    #TenantsUpdateCarInfoJob.perform_later(form)

    #puts("accepted order")
    #form = {
    #    car_id: 21,
    #    order_id: 44,
    #    tenant_id: 9,
    #    tenant_emails: 'xtail1996@yandex.ru',
    #    locale: 'ru'
    #}
    #OrdersNotifyTenantAcceptOrderJob.perform_later(form)

    #puts("declined order")
    #form = {
    #    car_id: 21,
    #    order_id: 44,
    #    tenant_id: 9,
    #    tenant_emails: 'xtail1996@yandex.ru',
    #    locale: 'ru'
    #}
    #OrdersNotifyTenantDeclineOrderJob.perform_later(form)

    #puts("moderation accepted")
    #form = {
    #    recipient: 'xtail1996@yandex.ru',
    #    locale: 'ru',
    #    car_name: 'Renault Duster',
    #    tenant_name: 'Name'
    #}
    #TenantsCarModerationAcceptedJob.perform_later(form)

    puts("end")

    render plain: "done"
  end
end