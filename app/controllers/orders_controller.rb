class OrdersController < SiteController
  skip_before_action :authenticate_tenant_account!, :only => [:accept, :decline]

  def create_order
    parameters = _params.to_h

    Site::V1::Order::Create::Interactor.new.call(parameters) do |m|
      m.success do |responce|
        send_data_to_payment_system(::Order.find_by(id: responce[:order_id]))
      end

      m.failure do
        render 'errors/create_order_error.html'
      end
    end
  end

  def accept
    order = Order.find_by(id: params[:uid])
    if order.present?
        token = params[:token]
        if order.accept_token == token
            order.is_declined = false
            order.is_accepted = true
            order.save

            tenant = ::Tenant.find_by(id: order.tenant_id)
            landlord = ::Tenant.find_by(id: order.landlord_id)
            car = ::Car.find_by(id: order.car_id)

            form = {
                order: order,
                car: car,
                tenant: tenant,
                landlord: landlord,
                locale: order.locale,
                source: 'site'
            }

            feedback_date = ((Date.parse(order.rent_til.to_s) - Date.today).abs).to_i + 1
            tenant_feedback_send = false
            landlord_feedback_send = false

            unless tenant.is_email_rejected
              OrdersNotifyTenantAcceptOrderJob.perform_later(form)
              TenantMailer.with(form: form).car_rent_tenant_feedback.deliver_later(wait_until: feedback_date.days.from_now)
              tenant_feedback_send = true
            end

            unless landlord.is_email_rejected
              OrdersNotifyLandlordAcceptOrderJob.perform_later(form)
              TenantMailer.with(form: form).car_rent_landlord_feedback.deliver_later(wait_until: feedback_date.days.from_now)
              landlord_feedback_send = true
            end

            if tenant_feedback_send && landlord_feedback_send
              admin_form = {
                  send_at: feedback_date.days.from_now.strftime('%d.%m.%Y %I:%M%p')
              }
              AdminMailer.with(form: form).car_rent_feedback_notify_admin.deliver_later(wait_until: feedback_date.days.from_now)
            end

        else
           render plain: "Error!" 
        end
    else
        render plain: "Error!"
    end
  end

  def decline
    order = Order.find_by(id: params[:uid])
    if order.present?
        token = params[:token]
        if order.decline_token == token
            order.is_declined = true;
            order.is_accepted = false;
            order.save

            tenant = ::Tenant.find_by(id: order.tenant_id)
            landlord = ::Tenant.find_by(id: order.landlord_id)
            car = ::Car.find_by(id: order.car_id)

            form = {
                order: order,
                car: car,
                tenant: tenant,
                landlord: landlord,
                locale: order.locale
            }

            unless tenant.is_email_rejected
              OrdersNotifyTenantDeclineOrderJob.perform_later(form)
              #OrderMailer.with(form: form).notify_tenant_decline_order.deliver_now
            end
        else
           render plain: "Error!" 
        end
    else
        render plain: "Error!"
    end
  end

  private

  def _params
    params.permit!
  end

  def send_data_to_payment_system(order)
    if order.present?
      url = 'https://wpay.uniteller.ru/pay/'

      shop_idp = Rails.application.secrets.Shop_IDP

      callback = Rails.application.secrets.domain_name + 'cars/' +  order.car_id.to_s

      order_price_value = ::Order.new.convert_cell(order.price_value)

      form_data = {
          Shop_IDP: shop_idp,
          Order_IDP: order.id,
          Subtotal_P: order_price_value,
          Signature: ::Order.new.create_signature(order.id, order_price_value),
          URL_RETURN: callback,
          Language: order.locale,
          Currency: 'RUB'
      }

      #require 'httparty'
      #response = HTTParty.post(url, :headers => {}, :body => form_data.to_param)
      #puts(response)
      #render html: response
      redirect_to url + '?' + form_data.to_param
    else
      render 'errors/create_order_error.html'
    end
  end
end