class Admin::CarsController < AdminController
  def index
    # stub
  end

  # Открывает страницу создания нового автомобиля
  def new
    @car = Car.new
  end

  # Открывает страницу редактирования автомобиля
  def edit
    @car = Car.find_by(id: params[:id])
  end

  # Создает новый автомобиль в базе
  def create
    @car = Car.new(car_params)
    redirect_to edit_admin_car_path(@car.id), notice: 'Автомобиль успешно создан.' if @car.save
  end

  # Обновляет автомобиль в базе
  def update
    @car = Car.find_by(id: params[:id])
    if(!@car.changed?)
      @car.updated_at = DateTime.now
    end
    @car.update_attributes(car_params)

    if @car.is_notifications_enabled
      need_notify = params[:need_notify]
      if need_notify.present? && @car.is_moderated
        form = {
            car: @car,
            source: 'site'
        }
        
        TenantsCarModerationAcceptedJob.perform_later(form)
      end
    end

    redirect_to edit_admin_car_path(@car.id), notice: 'Автомобиль успешно обновлен.' if @car.errors.blank?
  end

  # Удаляет автомобиль из базы
  def destroy
    @car = Car.find_by(id: params[:id]).destroy
    redirect_to admin_cars_path, notice: 'Автомобиль успешно удален.'
  end

  private

  # Возвращает все передаваемые параметры, кроме id
  def car_params
    params.require(:car).permit!.except(:id)
  end
end
