class Admin::TenantsController < AdminController
  def index
    # stub
  end

  def new
    @tenant = Tenant.new
    @accounts = []
  end

  def edit
    @tenant = Tenant.find_by(id: params[:id])

    @accounts = TenantAccount.where(tenant_id: @tenant.id)
  end

  def create
    form = params

    @tenant = Tenant.new(tenant_params)
    if @tenant.save
      if form[:email].present?
        account = add_account(@tenant.id, form, @tenant.is_email_rejected)
        if account.present?
          if account.confirmed_at == nil
            TenantsConfirmationJob.perform_later(account.id)
          end
          redirect_to edit_admin_tenant_path(@tenant.id), notice: 'Арендатор успешно создан.'
        else
          @tenant.delete
          redirect_to admin_tenants_path, notice: 'Не возможно создать аредатора или его аккаунт.'
        end
      end
    end
  end

  def update
    @tenant = Tenant.find_by(id: params[:id])
    @tenant.update_attributes(tenant_params)
    redirect_to edit_admin_tenant_path(@tenant.id), notice: 'Арендатор успешно обновлен.' if @tenant.errors.blank?
  end

  def destroy
    @tenant = Tenant.find_by(id: params[:id]).destroy
    redirect_to admin_tenants_path, notice: 'Арендатор успешно удален.'
  end

  def tenants_merge_form
    @ids_str = params[:ids]
    ids = @ids_str.split(',')
    @tenants = []
    ids.each do |id|
      tenant = Tenant.find_by(id: id)
      if tenant.present?
        @tenants << tenant
      end
    end

    @tenant = Tenant.new
  end

  def merge_tenants
    ids = params[:ids]

    ids = ids.split(',')
    tenants = []
    ids.each do |id|
      tenant = Tenant.find_by(id: id)
      if tenant.present?
        tenants << tenant
      end
    end

    merged_tenant = Tenant.new(tenant_params)
    unless merged_tenant.save
      render plain: 'Error!'
    end

    hash = tenant_params.to_h
    if(hash[:avatar].present?)
      merged_tenant.remote_avatar_url = hash[:avatar].to_s
      merged_tenant.save
    end

    tenants.each do |tenant|
      accounts = TenantAccount.where(tenant_id: tenant.id)
      if accounts.present?
        accounts.each do |account|
          account.tenant_id = merged_tenant.id
          account.save
        end
      end

      cars = Car.where(tenant_id: tenant.id)
      if cars.present?
        cars.each do |car|
          car.tenant_id = merged_tenant.id
          car.save
        end
      end

     tenant.delete
    end

    redirect_to edit_admin_tenant_path(merged_tenant.id)
  end

  private

  def tenant_params
    params.require(:tenant).permit!.except(:id)
  end

  def add_account(tenant_id, form, is_email_rejected)



    attributes = {
      tenant_id: tenant_id,
      uid: form[:email],
      email: form[:email],
      provider: form[:provider],
      password: Devise.friendly_token[0,6]
    }

    account = ::TenantAccount.new(attributes)
    if is_email_rejected
      account.skip_confirmation!
    end
    account.save
    account
  end
end
