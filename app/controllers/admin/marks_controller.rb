class Admin::MarksController < AdminController
  def index
    # stub
  end

  def new
    @mark = Mark.new
  end

  def edit
    @mark = Mark.find_by(id: params[:id])
  end

  def create
    @mark = Mark.new(mark_params)
    redirect_to edit_admin_mark_path(@mark.id), notice: 'Марка автомобиля успешно создана.' if @mark.save
  end

  def update
    @mark = Mark.find_by(id: params[:id])
    @mark.update_attributes(mark_params)
    redirect_to edit_admin_mark_path(@mark.id), notice: 'Марка автомобиля успешно обновлена.' if @mark.errors.blank?
  end

  def destroy
    @mark = Mark.find_by(id: params[:id]).destroy
    redirect_to admin_marks_path, notice: 'Марка автомобиля успешно удалена.'
  end

  private

  def mark_params
    params.require(:mark).permit!.except(:id)
  end
end