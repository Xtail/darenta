class Admin::ColorsController < AdminController
  def index
    @colors = Color.all.order(id: :desc)
  end

  def new
    @color = Color.new
  end

  def edit
    @color = Color.find_by(id: params[:id])
  end

  def create
    @color = Color.new(color_params)
    redirect_to edit_admin_color_path(@color.id), notice: 'Цвет успешно создан.' if @color.save
  end

  def update
    @color = Color.find_by(id: params[:id])
    @color.update_attributes(color_params)
    redirect_to edit_admin_color_path(@color.id), notice: 'Цвет успешно обновлен.' if @color.errors.blank?
  end

  def destroy
    @color = Color.find_by(id: params[:id]).destroy
    redirect_to admin_colors_path, notice: 'Цвет успешно удален.'
  end

  private

  def color_params
    params.require(:color).permit!.except(:id)
  end
end
