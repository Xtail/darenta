class Admin::OrdersController < AdminController
  def index
    # stub
  end

  def new
    @order = Order.new
  end

  def edit
    @order = Order.find_by(id: params[:id])
    if @order.landlord_id.present?
      @owner = User.find_by(id: @order.landlord_id)
    end
  end

  def create
    # stub
  end

  def update
    # stub
  end

  def destroy
    @order = Order.find_by(id: params[:id]).destroy
    redirect_to admin_orders_path, notice: 'Заказ успешно удален.'
  end
end