class Admin::UsersController < AdminController
  def index
    # stub
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find_by(id: params[:id])
  end

  def create
    @user = User.new(user_params)
    redirect_to edit_admin_user_path(@user.id), notice: 'Пользователь успешно создан.' if @user.save
  end

  def update
    @user = User.find_by(id: params[:id])
    @user.update_attributes(user_params)
    redirect_to edit_admin_user_path(@user.id), notice: 'Пользователь успешно обновлен.' if @user.errors.blank?
  end

  def destroy
    @user = User.find_by(id: params[:id]).destroy
    redirect_to admin_users_path, notice: 'Пользователь успешно удален.'
  end

  private

  def user_params
    params.require(:user).permit!.except(:id)
  end
end
