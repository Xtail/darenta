class Admin::AcrissCodesController < AdminController
  def index
    # stub
  end

  def new
    @acriss = AcrissCode.new
  end

  def edit
    @acriss = AcrissCode.find_by(id: params[:id])
  end

  def create
    @acriss = AcrissCode.new(_params)
    redirect_to edit_admin_acriss_code_path(@acriss.id), notice: 'Код успешно создан.' if @acriss.save
  end

  def update
    @acriss = AcrissCode.find_by(id: params[:id])
    @acriss.update_attributes(_params)
    redirect_to edit_admin_acriss_code_path(@acriss.id), notice: 'Код успешно обновлен.' if @acriss.errors.blank?
  end

  def destroy
    @acriss = AcrissCode.find_by(id: params[:id]).destroy
    redirect_to admin_acriss_codes_path, notice: 'Код успешно удален.'
  end

  private

  def _params
    params.require(:acriss_code).permit!.except(:id)
  end
end
