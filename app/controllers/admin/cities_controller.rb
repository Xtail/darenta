class Admin::CitiesController < AdminController
  def index
    @cities = City.all.order(id: :desc)
  end

  def new
    @city = City.new
  end

  def edit
    @city = City.find_by(id: params[:id])
  end

  def create
    @city = City.new(city_params)
    redirect_to edit_admin_city_path(@city.id), notice: 'Город успешно создан.' if @city.save
  end

  def update
    @city = City.find_by(id: params[:id])
    @city.update_attributes(city_params)
    redirect_to edit_admin_city_path(@city.id), notice: 'Город успешно обновлен.' if @city.errors.blank?
  end

  def destroy
    @city = City.find_by(id: params[:id]).destroy
    redirect_to admin_cities_path, notice: 'Город успешно удален.'
  end

  private

  def city_params
    params.require(:city).permit!.except(:id)
  end
end
