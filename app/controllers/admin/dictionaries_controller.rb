class Admin::DictionariesController < AdminController
  def update
    data = params[:dictionary]
    mark_hash = JSON.parse data

    # clear tables
    Mark.delete_all
    Sampler.delete_all

    # find all marks
    mark_hash.keys.each do |key|
      # create marks
      mark_attributes = {
          name: key
      }
      mark = Mark.new(mark_attributes)
      if mark.save
        # create samplers
        samplers = mark_hash[key]
        if samplers.is_a?(String)
          tmp = samplers
          samplers = []
          samplers << tmp
        end
        samplers.map do |s|
          sampler_attributes = {
              mark_id: mark.id,
              name: s
          }
          Sampler.create(sampler_attributes)
        end
      end

    end

    redirect_to admin_marks_path
  end

  def load

  end
end
