class Admin::TransmissionsController < AdminController
  def index
    @transmissions = Transmission.all.order(id: :desc)
  end

  def new
    @transmission = Transmission.new
  end

  def edit
    @transmission = Transmission.find_by(id: params[:id])
  end

  def create
    @transmission = Transmission.new(transmission_params)
    redirect_to edit_admin_transmission_path(@transmission.id), notice: 'Коробка передач успешно создана.' if @transmission.save
  end

  def update
    @transmission = Transmission.find_by(id: params[:id])
    @transmission.update_attributes(transmission_params)
    redirect_to edit_admin_transmission_path(@transmission.id), notice: 'Коробка передач успешно обновлена.' if @transmission.errors.blank?
  end

  def destroy
    @transmission = Transmission.find_by(id: params[:id]).destroy
    redirect_to admin_transmissions_path, notice: 'Коробка передач успешно удалена.'
  end

  private

  def transmission_params
    params.require(:transmission).permit!.except(:id)
  end
end
