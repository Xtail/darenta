class Admin::BodiesController < AdminController
  def index
    @bodies = Body.all.order(id: :desc)
  end

  def new
    @body = Body.new
  end

  def edit
    @body = Body.find_by(id: params[:id])
  end

  def create
    @body = Body.new(body_params)
    redirect_to edit_admin_body_path(@body.id), notice: 'Тип кузова успешно создан.' if @body.save
  end

  def update
    @body = Body.find_by(id: params[:id])
    @body.update_attributes(body_params)
    redirect_to edit_admin_body_path(@body.id), notice: 'Тип кузова успешно обновлен.' if @body.errors.blank?
  end

  def destroy
    @body = Body.find_by(id: params[:id]).destroy
    redirect_to admin_bodies_path, notice: 'Тип кузова успешно удален.'
  end

  private

  def body_params
    params.require(:body).permit!.except(:id)
  end
end
