class Admin::EnginesController < AdminController
  def index
    @engines = Engine.all.order(id: :desc)
  end

  def new
    @engine = Engine.new
  end

  def edit
    @engine = Engine.find_by(id: params[:id])
  end

  def create
    @engine = Engine.new(engine_params)
    redirect_to edit_admin_engine_path(@engine.id), notice: 'Тип двигателя успешно создан.' if @engine.save
  end

  def update
    @engine = Engine.find_by(id: params[:id])
    @engine.update_attributes(engine_params)
    redirect_to edit_admin_engine_path(@engine.id), notice: 'Тип двигателя успешно обновлен.' if @engine.errors.blank?
  end

  def destroy
    @engine = Engine.find_by(id: params[:id]).destroy
    redirect_to admin_engines_path, notice: 'Тип двигателя успешно удален.'
  end

  private

  def engine_params
    params.require(:engine).permit!.except(:id)
  end
end
