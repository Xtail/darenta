class Admin::TenantAccountsController < AdminController
  def index
    # stub
  end

  def new
    @tenant_account = TenantAccount.new
    @tenant_id = params[:tenant]
  end

  def edit
    @tenant_account = TenantAccount.find_by(id: params[:id])
  end

  def create
    @tenant_account = TenantAccount.new(tenant_account_params)
    redirect_to '/admin/renters/' + @tenant_account.tenant_id.to_s + '/edit' , notice: 'Арендатор успешно создан.' if @tenant_account.save
    #redirect_to edit_admin_tenant_account_path(@tenant_account.id), notice: 'Арендатор успешно создан.' if @tenant_account.save
  end

  def update
    @tenant_account = TenantAccount.find_by(id: params[:id])
    @tenant_account.update_attributes(tenant_account_params)
    redirect_to edit_admin_tenant_account_path(@tenant_account.id), notice: 'Арендатор успешно обновлен.' if @tenant_account.errors.blank?
  end

  def destroy
    @tenant_account = TenantAccount.find_by(id: params[:id]).destroy
    redirect_to admin_tenant_accounts_path, notice: 'Арендатор успешно удален.'
  end

  private

  def tenant_account_params
    params.require(:tenant_account).permit!.except(:id)
  end
end
