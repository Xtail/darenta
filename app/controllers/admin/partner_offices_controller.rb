class Admin::PartnerOfficesController < AdminController
  def index
    # stub
  end

  def new
    @office = PartnerOffice.new
  end

  def edit
    @office = PartnerOffice.find_by(id: params[:id])
  end

  def create
    @office = PartnerOffice.new(_params)
    redirect_to edit_admin_partner_office_path(@office.id), notice: 'Офис успешно создан.' if @office.save
  end

  def update
    @office = PartnerOffice.find_by(id: params[:id])
    @office.update_attributes(_params)
    redirect_to edit_admin_partner_office_path(@office.id), notice: 'Офис успешно обновлен.' if @office.errors.blank?
  end

  def destroy
    @office = PartnerOffice.find_by(id: params[:id]).destroy
    redirect_to admin_partner_offices_path, notice: 'Офис успешно удален.'
  end

  private

  def _params
    params.require(:partner_office).permit!.except(:id)
  end
end
