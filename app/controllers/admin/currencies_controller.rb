class Admin::CurrenciesController < AdminController
    def index
      @currencies = Currency.all.order(id: :desc)
    end
  
    def new
      # stub
    end
  
    def edit
      # stub
    end
  
    def create
      # stub
    end
  
    def update
      # stub
    end
  
    def destroy
      # stub
    end

    def update_all_currencies
      UpdateCurrenciesJob.perform_now
      redirect_to admin_currencies_path
    end
  
    private
  
    def _params
      params.require(:currency).permit!.except(:id)
    end
  end
  