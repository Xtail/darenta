class Admin::Datatables::TenantAccountsController < AdminController
  def index
    respond_to do |format|
      format.json { render json: TenantAccountDatatable.new(view_context) }
    end
  end
end