class Admin::Datatables::TenantsController < AdminController
  def index
    respond_to do |format|
      format.json { render json: TenantDatatable.new(view_context) }
    end
  end
end