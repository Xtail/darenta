class Admin::Datatables::UsersController < AdminController
  def index
    respond_to do |format|
      format.json { render json: UserDatatable.new(view_context) }
    end
  end
end
