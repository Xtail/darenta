class Admin::Datatables::SamplersController < AdminController
  def index
    respond_to do |format|
      format.json { render json: SamplerDatatable.new(view_context) }
    end
  end
end