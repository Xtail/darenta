class Admin::Datatables::CarsController < AdminController
  def index
    respond_to do |format|
      format.json { render json: CarDatatable.new(view_context) }
    end
  end
end
