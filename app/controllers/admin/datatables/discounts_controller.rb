class Admin::Datatables::DiscountsController < AdminController
  def index
    respond_to do |format|
      format.json { render json: DiscountDatatable.new(view_context) }
    end
  end
end