class Admin::Datatables::PartnerOfficesController < AdminController
  def index
    respond_to do |format|
      format.json { render json: PartnerOfficeDatatable.new(view_context) }
    end
  end
end
