class Admin::Datatables::OrdersController < AdminController
  def index
    respond_to do |format|
      format.json { render json: OrderDatatable.new(view_context) }
    end
  end
end