class Admin::Datatables::MailMessagesController < AdminController
  def index
    respond_to do |format|
      format.json { render json: MailMessageDatatable.new(view_context) }
    end
  end
end
