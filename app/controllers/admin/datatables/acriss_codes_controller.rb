class Admin::Datatables::AcrissCodesController < AdminController
  def index
    respond_to do |format|
      format.json { render json: AcrissCodeDatatable.new(view_context) }
    end
  end
end
