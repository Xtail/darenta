class Admin::Datatables::MarksController < AdminController
  def index
    respond_to do |format|
      format.json { render json: MarkDatatable.new(view_context) }
    end
  end
end