class Admin::DiscountsController < AdminController
  def index
    # stub
  end

  def new
    @discount = Discount.new
  end

  def edit
    @discount = Discount.find_by(id: params[:id])
  end

  def create
    # stub
  end

  def update
    # stub
  end

  def destroy
    @discount = Discount.find_by(id: params[:id]).destroy
    redirect_to admin_discounts_path, notice: 'Скидка успешно удалена.'
  end
end