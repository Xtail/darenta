class Admin::MailMessagesController < AdminController
  def index
    # stub
  end

  def new
    @message = MailMessage.new
  end

  def edit
    @message = MailMessage.find_by(id: params[:id])
  end

  def create
    @message = MailMessage.new(mail_message_params)
    redirect_to edit_admin_mail_message_path(@message.id), notice: 'Текст письма успешно создан.' if @message.save
  end

  def update
    @message = MailMessage.find_by(id: params[:id])
    @message.update_attributes(mail_message_params)
    redirect_to edit_admin_mail_message_path(@message.id), notice: 'Текст письма успешно обновлен.' if @message.errors.blank?
  end

  def destroy
    @message = MailMessage.find_by(id: params[:id]).destroy
    redirect_to admin_mail_messages_path, notice: 'Текст письма успешно удален.'
  end

  private

  def mail_message_params

    key = params[:mail_message][:key]
    title = params[:mail_message][:title]

    message_title_hash = {}
    message_body_hash = {}
    message_footer_hash = {}

    I18n.available_locales.each do |l|
      message_title_hash[l] = make_hash_for_locale(params[:mail_message]["message_title_#{l}"])
      message_body_hash[l] = make_hash_for_locale(params[:mail_message]["message_body_#{l}"])
      message_footer_hash[l] = make_hash_for_locale(params[:mail_message]["message_footer_#{l}"])
    end

    message_title_json = generate_json_by_hash(message_title_hash)
    message_body_json = generate_json_by_hash(message_body_hash)
    message_footer_json = generate_json_by_hash(message_footer_hash)

    #puts message_title_json
    #puts message_body_json
    #puts message_footer_json

    params[:mail_message] = {title: title,
                             key: key,
                             message_title: message_title_json,
                             message_body: message_body_json,
                             message_footer: message_footer_json}

    params.require(:mail_message).permit!.except(:id)
  end

  def generate_json_by_hash(hash)
    require 'json'
    hash.to_json
    #puts JSON.generate(hash)
  end

  def make_hash_for_locale(text)
    list = text.split('§');

    hash_for_locale = {}

    count = 0;
    list.each do |l|
      template = "block" + String(count)
      hash_for_locale[template] = l
      count += 1
    end

    hash_for_locale
  end

end
