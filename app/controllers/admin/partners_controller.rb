class Admin::PartnersController < AdminController
  def partners_switch_status
    value = params[:is_partners]
    if value == '1'
      Rails.application.secrets.partners_enable = true
    else
      Rails.application.secrets.partners_enable = false
    end
    
    
    redirect_to admin_cars_path
    #render plain: value
  end
end