class Admin::SamplersController < AdminController
  def index
    # stub
  end

  def new
    @sampler = Sampler.new
  end

  def edit
    @sampler = Sampler.find_by(id: params[:id])
  end

  def create
    @sampler = Sampler.new(sampler_params)
    redirect_to edit_admin_sampler_path(@sampler.id), notice: 'Модель автомобиля успешно создана.' if @sampler.save
  end

  def update
    @sampler = Sampler.find_by(id: params[:id])
    @sampler.update_attributes(sampler_params)
    redirect_to edit_admin_sampler_path(@sampler.id), notice: 'Модель автомобиля успешно обновлена.' if @sampler.errors.blank?
  end

  def destroy
    @sampler = Sampler.find_by(id: params[:id]).destroy
    redirect_to admin_samplers_path, notice: 'Модель автомобиля успешно удалена.'
  end

  private

  def sampler_params
    params.require(:sampler).permit!.except(:id)
  end
end