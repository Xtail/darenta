class Admin::DiscountTypesController < AdminController
  def index
    # stub
  end

  def new
    @discount_type = DiscountType.new
  end

  def edit
    @discount_type = DiscountType.find_by(id: params[:id])
  end

  def create
    @discount_type = DiscountType.new(discount_type_params)
    redirect_to edit_admin_discount_type_path(@discount_type.id), notice: 'Тип скидки успешно создан.' if @discount_type.save
  end

  def update
    @discount_type = DiscountType.find_by(id: params[:id])
    @discount_type.update_attributes(discount_type_params)
    redirect_to edit_admin_discount_type_path(@discount_type.id), notice: 'Тип скидки успешно обновлен.' if @discount_type.errors.blank?
  end

  def destroy
    @discount_type = DiscountType.find_by(id: params[:id]).destroy
    redirect_to admin_discount_types_path, notice: 'Тип скидки успешно удален.'
  end

  private

  def discount_type_params
    params.require(:discount_type).permit!.except(:id)
  end
end