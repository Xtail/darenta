class AdminsRegisterTenantJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:tenant_account].present? && form[:device].present? && form[:source].present?
      AdminMailer.with(form: form).register_tenant.deliver_now
    end
  end
end
