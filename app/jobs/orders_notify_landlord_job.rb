class OrdersNotifyLandlordJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:order].present? && form[:car].present? &&
        form[:tenant].present? && form[:landlord].present? && form[:locale].present?
      unless form[:landlord].is_email_rejected
        OrderMailer.with(form: form).notify_landlord.deliver_now
      end
    end
  end
end
