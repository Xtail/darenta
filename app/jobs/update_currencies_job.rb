class UpdateCurrenciesJob < ApplicationJob
    queue_as :default
  
    def perform
      ::Currency.new.resolve_currencies
    end
  end