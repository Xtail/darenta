class TenantsAddServiceJob < ApplicationJob
  queue_as :mailers

  def perform(account_id, source = 'site')
    account = TenantAccount.find_by(id: account_id)
    if account.present?
      unless account.tenant.is_email_rejected
        form = {
          source: source
        }
        ConfirmationMailer.add_sevice_instructions(account, account.confirmation_token, form).deliver_now
      end
    end
  end
end
