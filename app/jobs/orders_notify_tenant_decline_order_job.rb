class OrdersNotifyTenantDeclineOrderJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:order].present? && form[:car].present? &&
        form[:tenant].present? && form[:landlord].present? && form[:locale].present?
      unless form[:tenant].is_email_rejected
        OrderMailer.with(form: form).notify_tenant_decline_order.deliver_now
      end
    end
  end
end
