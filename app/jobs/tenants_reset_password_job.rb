class TenantsResetPasswordJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:recipient].present? && form[:reset_password_link].present? && form[:locale].present? && form[:source].present?
      TenantMailer.with(form: form).reset_password_notify.deliver_now
    end
  end
end
