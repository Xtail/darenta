class OrdersNotifyTenantAcceptOrderJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:order].present? && form[:car].present? &&
        form[:tenant].present? && form[:landlord].present? && form[:locale].present?
      OrderMailer.with(form: form).notify_tenant_accept_order.deliver_now
    end
  end
end
