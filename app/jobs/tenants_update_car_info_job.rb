class TenantsUpdateCarInfoJob < ApplicationJob
  queue_as :mailers

  def perform(form)
  	# проверяем все ли параметры формы заполнены
    if form[:car].present? && form[:source].present?
      # проверяем, не отключены ли уведомления у пользователя
      unless form[:car].tenant.is_email_rejected
      	# отправка письма
        TenantMailer.with(form: form).update_car_notify.deliver_now
      end
    end
  end
end
