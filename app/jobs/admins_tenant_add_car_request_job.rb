class AdminsTenantAddCarRequestJob < ApplicationJob
  queue_as :mailers

  def perform(form)
  	# проверяем все ли параметры формы заполнены
    if form[:car].present? && form[:source].present?
    	# отправка письма
      AdminMailer.with(form: form).tenant_add_car_request.deliver_now
    end
  end
end
