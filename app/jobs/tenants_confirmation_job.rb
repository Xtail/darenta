class TenantsConfirmationJob < ApplicationJob
  queue_as :mailers

  def perform(account_id, source = 'site')
    account = TenantAccount.find_by(id: account_id)
    if account.present?

      if account.confirmed_at != nil
        return
      end

      if account.last_confirmation_sent_at != nil
        # сколько времени прошло с последней отправки подтверждения
        delay_since_last_confirmation = DateTime.now.to_time - account.last_confirmation_sent_at.to_time

        # если прошло меньше 30 минут, не отправляем письмо
        if delay_since_last_confirmation < 1800
          return
        end
      end

      unless account.tenant.is_email_rejected
        form = {
          source: source
        }
        ConfirmationMailer.confirmation_instructions(account, account.confirmation_token, form).deliver_now
        account.last_confirmation_sent_at = DateTime.now
        account.save
      end
    end
  end
end
