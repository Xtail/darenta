class TenantsMailerWelcomeInstructionsJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:tenant_account].present? && form[:source].present?
      unless form[:tenant_account].tenant.is_email_rejected
        ConfirmationMailer.with(form: form).welcome_instructions.deliver_now
      end
    end
  end
end
