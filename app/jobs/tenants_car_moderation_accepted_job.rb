class TenantsCarModerationAcceptedJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:car].present? && form[:source].present?
      unless form[:car].tenant.is_email_rejected
        TenantMailer.with(form: form).car_moderation_accepted.deliver_now
      end
    end
  end
end
