class AdminsTenantConfirmedJob < ApplicationJob
  queue_as :mailers

  def perform(form)
    if form[:tenant_account].present? && form[:source].present?
      AdminMailer.with(form: form).tenant_confirmed.deliver_now
    end
  end
end
