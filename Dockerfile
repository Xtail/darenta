FROM ruby:2.5.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs imagemagick pngquant jpegoptim

RUN mkdir /myapp
WORKDIR /myapp

COPY ./docker-data/scripts/wait-for-it.sh /usr/local/bin/wait-for-it
RUN chmod +x /usr/local/bin/wait-for-it

COPY ./docker-data/scripts/web-up.sh /usr/local/bin/web-up
RUN chmod +x /usr/local/bin/web-up

COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock

RUN bundle install
COPY . /myapp
