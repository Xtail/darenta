class AddDeviseFieldsToTenantAccount < ActiveRecord::Migration[5.1]
  def change
    ## Rememberable
    add_column :tenant_accounts, :remember_created_at, :datetime

    ## Trackable
    add_column :tenant_accounts, :sign_in_count, :integer, :default => 0, :null => false
    add_column :tenant_accounts, :current_sign_in_at, :datetime
    add_column :tenant_accounts, :last_sign_in_at, :datetime
    add_column :tenant_accounts, :current_sign_in_ip, :string
    add_column :tenant_accounts, :last_sign_in_ip, :string
  end
end