class AddLocationCodeToPartnerCar < ActiveRecord::Migration[5.1]
  def change
    add_column :partner_cars, :location_code, :string
  end
end
