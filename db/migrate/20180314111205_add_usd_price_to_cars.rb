class AddUsdPriceToCars < ActiveRecord::Migration[5.1]
  def change
    remove_index :cars, :price
    remove_column :cars, :price

    add_column :cars, :price_rub, :float, default: 0.0
    add_column :cars, :price_usd, :float, default: 0.0

    add_index :cars, :price_rub
    add_index :cars, :price_usd
  end
end
