class MergeTenants < ActiveRecord::Migration[5.1]
  def up
    all_tenants = Tenant.all

    all_tenants.each do |tenant|

      all_tenants_tmp = Tenant.all

      all_tenants_tmp.each do |tenant_tmp|
        if tenant_tmp.t_first_name == tenant.t_first_name &&
            tenant_tmp.t_last_name == tenant.t_last_name

          # find accounts
          accounts_tmp = TenantAccount.where(tenant_id: tenant_tmp.id)
          accounts_tmp.each do |account|
            account.tenant_id = tenant.id
            account.save
          end

          # find cars
          cars_tmp = Car.where(tenant_id: tenant_tmp.id)
          cars_tmp.each do |car|
            car.tenant_id = tenant.id
            car.save
          end

          # check merge success
          accounts_check = TenantAccount.where(tenant_id: tenant_tmp.id)
          cars_check = Car.where(tenant_id: tenant_tmp.id)
          if accounts_check.empty? && cars_check.empty?
            # delete tenant
            tenant_tmp.delete
          end

        end
      end
    end
  end

  def down

  end
end
