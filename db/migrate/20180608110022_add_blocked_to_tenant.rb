class AddBlockedToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :is_blocked, :boolean, default: false
  end
end
