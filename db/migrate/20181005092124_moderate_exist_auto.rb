class ModerateExistAuto < ActiveRecord::Migration[5.1]
  def up
    cars = Car.all
    cars.each do |car|
      unless car.is_moderated
        car.is_moderated = true
        car.save
      end
    end
  end
end
