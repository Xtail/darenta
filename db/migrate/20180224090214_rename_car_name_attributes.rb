class RenameCarNameAttributes < ActiveRecord::Migration[5.1]
  def change
    remove_index :cars, :mark
    remove_index :cars, :model

    remove_column :cars, :mark
    remove_column :cars, :model

    add_column :cars, :mark_name, :string, null: false
    add_column :cars, :sampler_name, :string, null: false
    add_column :cars, :full_name, :string

    add_index :cars, :mark_name
    add_index :cars, :sampler_name
    add_index :cars, :full_name
  end
end
