class AutofillTenantsTranslations < ActiveRecord::Migration[5.1]
  def up
    all_tenants = Tenant.all

    all_tenants.each do |tenant|
      tenant.t_first_name = tenant.first_name
      tenant.t_last_name = tenant.last_name
      tenant.save
    end
  end

  def down

  end
end
