class CreateCities < ActiveRecord::Migration[5.1]
  def change
    create_table :cities do |t|
    end

    City.create_translation_table! name: :string

    add_index :city_translations, [:locale, :name], unique: true
    add_foreign_key :city_translations, :cities
  end
end
