class CreateMailMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :mail_messages do |t|
      t.string :key, null: false
      t.string :title, null: false
      t.jsonb :message_title
      t.jsonb :message_body
      t.jsonb :message_footer
    end
    add_index :mail_messages, :key, unique: true
  end
end
