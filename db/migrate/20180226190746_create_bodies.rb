class CreateBodies < ActiveRecord::Migration[5.1]
  def change
    create_table :bodies do |t|
    end

    Body.create_translation_table! name: :string

    add_index :body_translations, [:locale, :name], unique: true
    add_foreign_key :body_translations, :bodies
  end
end
