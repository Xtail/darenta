class AddLocaleToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :locale, :string, default: 'ru'
  end
end
