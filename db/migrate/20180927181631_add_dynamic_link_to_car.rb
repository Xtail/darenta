class AddDynamicLinkToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :dynamic_link, :string
  end
end
