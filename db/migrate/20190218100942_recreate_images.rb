class RecreateImages < ActiveRecord::Migration[5.1]
  def up
    Resource.all.each do |resource|
      if resource.file? 
        resource.file.recreate_versions! 
        resource.save
      end
    end
  end

  def down
  end
end
