class CopyCarImages < ActiveRecord::Migration[5.1]
  def up
    cars = ::Car.all
    cars.each do |car|
      if car.image.present?
        images_ids = []
        image = car.image
        resource_attributes = {
          file: image
        }
        resource = ::Resource.new(resource_attributes)
        resource.save
        images_ids << resource.id

        car.images_ids = images_ids
        car.save
      end
    end
  end

  def down
  end
end
