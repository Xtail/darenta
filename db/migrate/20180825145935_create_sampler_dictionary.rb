class CreateSamplerDictionary < ActiveRecord::Migration[5.1]
  def change
    create_table :samplers do |t|
      t.bigint :mark_id, null: false, index: true
      t.string :name, null: false

      t.index [:mark_id, :name], unique: true
    end
  end
end
