class CopyCarContracts < ActiveRecord::Migration[5.1]
  def up
    cars = ::Car.all
    cars.each do |car|
      if car.contract_images.present?
        contract_images_ids = []

        images = car.contract_images
        images.each do |image|
          resource_attributes = {
            file: image
          }

          resource = ::Resource.new(resource_attributes)
          resource.save

          contract_images_ids << resource.id
        end

        car.contract_images_ids = contract_images_ids
        car.save
      end
    end
  end

  def down
  end
end
