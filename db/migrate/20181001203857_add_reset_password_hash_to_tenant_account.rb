class AddResetPasswordHashToTenantAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_accounts, :reset_password_hash, :string
  end
end
