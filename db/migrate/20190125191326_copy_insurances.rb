class CopyInsurances < ActiveRecord::Migration[5.1]
  def up
    cars = ::Car.all
    cars.each do |car|
      if car.insurance_images.present?
        images_ids = []
        images = car.insurance_images
        images.each do |image|
          resource_attributes = {
            file: image
          }
          resource = ::Resource.new(resource_attributes)
          resource.save
          images_ids << resource.id
        end

        insurances = []
        insurance = {
          name: "Страховка",
          images_ids: images_ids
        }
        insurances << insurance
        car.insurances = insurances
        car.save
      end
    end
  end

  def down
  end
end
