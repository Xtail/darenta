class AddImagesIdsToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :images_ids, :jsonb
  end
end
