class MigrateUserdData < ActiveRecord::Migration[5.1]
  def up
    landlords = User.all
    landlords.each do |landlord|
        # create tenant profile
        profile_attributes = {
            t_first_name: landlord.first_name,
            t_last_name: landlord.last_name,
            telephone: nil,
            avatar: landlord.avatar,
            locale: 'ru'
        }
        tenant = Tenant.create(profile_attributes)
        tenant.save

        current_tenant_id = tenant.id

        # create tenant account
        account_attributes = {
            tenant_id: current_tenant_id,
            provider: 'email',
            email: landlord.email,
            encrypted_password: '$2y$12$U06JaP4s/6HS4Yi4vJaOduyVuXUChyioU9aDAx8W5Mmr4z1oWPbmC' #123
        }
        account = TenantAccount.create(account_attributes)
        account.save

        #update cars
        landlord_cars = Car.where(tenant_id: landlord.id)
        landlord_cars.each do |car|
            car.tenant_id = current_tenant_id
            car.save
        end
    end
  end
end
