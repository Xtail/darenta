class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :avatar
    end

    User.create_translation_table! first_name: :string, last_name: :string, middle_name: :string,
                                   short_name: :string, full_name: :string

    add_index :users, :email, unique: true
    add_foreign_key :user_translations, :users

    add_index :user_translations, :short_name
    add_index :user_translations, :full_name
  end
end
