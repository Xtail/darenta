class AddConfirmationSentFromToTenantAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_accounts, :confirmation_sent_from, :string, default: "site"
  end
end
