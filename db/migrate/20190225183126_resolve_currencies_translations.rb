class ResolveCurrenciesTranslations < ActiveRecord::Migration[5.1]
  def up
    currencies = ::Currency.all
    currencies.each do |currency|
      if currency.code == 'rub'
        currency.name_ru = 'руб'
        currency.name_en = 'rub'
        currency.name_ko = '루블'
        currency.save
      else
        currency.name_ru = currency.code
        currency.name_en = currency.code
        currency.name_ko = currency.code
        currency.save
      end
    end
  end

  def down
  end
end
