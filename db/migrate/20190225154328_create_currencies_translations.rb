class CreateCurrenciesTranslations < ActiveRecord::Migration[5.1]
  def change
    create_table :currency_translations do |t|
      t.integer "currency_id", null: false
      t.string "locale", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "name"
    end
    add_index :currency_translations, :currency_id
    add_index :currency_translations, [:locale, :name]
    add_index :currency_translations, :locale
  end
end
