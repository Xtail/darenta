class DropInsurancesTable < ActiveRecord::Migration[5.1]
  def up
    drop_table :insurances
  end

  def down
  end
end
