class AddImagesToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :images, :jsonb
  end
end
