class AddLocaleToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :locale, :string, default: 'ru'
  end
end
