class AddCreatedAtAndUpdatedAtToCurrency < ActiveRecord::Migration[5.1]
  def change
    add_column :currencies, :created_at, :datetime, null: false, default: DateTime.now
    add_column :currencies, :updated_at, :datetime, null: false, default: DateTime.now
  end
end
