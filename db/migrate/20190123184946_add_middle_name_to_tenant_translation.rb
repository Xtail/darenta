class AddMiddleNameToTenantTranslation < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_translations, :t_middle_name, :string
    add_column :tenant_translations, :unconfirmed_t_middle_name, :string, default: ""
  end
end
