class AcceptAndDeclineOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :is_accepted, :boolean, default: false
    add_column :orders, :is_declined, :boolean, default: false
    add_column :orders, :accept_token, :string, default: ""
    add_column :orders, :decline_token, :string, default: ""
  end
end
