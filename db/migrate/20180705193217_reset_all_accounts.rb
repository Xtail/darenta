class ResetAllAccounts < ActiveRecord::Migration[5.1]
  def change
     all_accounts = TenantAccount.all

     all_accounts.each do |account|
        tenant_id = account.tenant_id
        provider = account.provider
        email = account.email
        unconfirmed_email = account.unconfirmed_email
        confirmed_at = account.confirmed_at
        confirmation_sent_at = account.confirmation_sent_at
        confirmation_token = account.confirmation_token
        uid = account.uid

        account.delete

        account_attributes = {
          tenant_id: tenant_id,
          provider: provider,
          email: email,
          unconfirmed_email: unconfirmed_email,
          confirmed_at: confirmed_at,
          confirmation_sent_at: confirmation_sent_at,
          confirmation_token: confirmation_token,
          uid: uid
        }

        account = TenantAccount.create(account_attributes)
        account.save
     end
  end
end
