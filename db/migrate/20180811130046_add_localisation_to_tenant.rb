class AddLocalisationToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :t_first_name, :string
    add_column :tenants, :t_last_name, :string

    create_table :tenant_translations do |t|
        t.integer "tenant_id", null: false
        t.string "locale", null: false
        t.datetime "created_at", null: false
        t.datetime "updated_at", null: false
        t.string "t_first_name"
        t.string "t_last_name"
    end

    add_index :tenant_translations, :tenant_id, unique: false
    add_index :tenant_translations, :locale, unique: false
  end
end