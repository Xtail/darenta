class ApplyLastConfirmationSentAtToExistingTenantAccount < ActiveRecord::Migration[5.1]
  def up
    accounts = ::TenantAccount.all
    accounts.each do |account|
      account.last_confirmation_sent_at = account.confirmation_sent_at
      account.save
    end
  end

  def down
  end
end
