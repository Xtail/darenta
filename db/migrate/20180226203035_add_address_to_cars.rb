class AddAddressToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :address_street, :string
    add_column :cars, :address_house, :string
    add_column :cars, :address_latitude, :float
    add_column :cars, :address_longitude, :float
  end
end
