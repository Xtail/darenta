class UpdateCarIdSettingsInOrder < ActiveRecord::Migration[5.1]
  def change
    change_column_null :orders, :car_id, true
  end
end
