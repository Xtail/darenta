class RemoveEmptyTenants < ActiveRecord::Migration[5.1]
  def up
    all_tenants = Tenant.all

    all_tenants.each do |tenant|
      accounts = TenantAccount.where(tenant_id: tenant.id)
      cars = Car.where(tenant_id: tenant.id)
      if accounts.empty? && cars.empty?
        tenant.delete
      end
    end
  end

  def down

  end
end
