class RemoveCarImages < ActiveRecord::Migration[5.1]
  def change
    remove_column :cars, :images, :jsonb
  end
end
