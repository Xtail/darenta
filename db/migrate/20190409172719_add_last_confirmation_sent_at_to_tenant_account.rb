class AddLastConfirmationSentAtToTenantAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_accounts, :last_confirmation_sent_at, :datetime
  end
end
