class AddIsEmailRejectedToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :is_email_rejected, :boolean, default: false
    add_column :tenants, :email_reject_token, :string, default: ""
  end
end
