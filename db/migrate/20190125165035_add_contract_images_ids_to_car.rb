class AddContractImagesIdsToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :contract_images_ids, :jsonb
  end
end
