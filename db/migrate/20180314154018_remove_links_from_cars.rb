class RemoveLinksFromCars < ActiveRecord::Migration[5.1]
  def change
    remove_column :cars, :link_to_rental_permit
    remove_column :cars, :link_to_rental_ban
  end
end
