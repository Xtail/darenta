class AddLandlordIdToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :landlord_id, :bigint
  end
end
