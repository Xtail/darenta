class AddEuroPriceToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :price_eur, :float, default: 0.0
    add_index :cars, :price_eur
  end
end
