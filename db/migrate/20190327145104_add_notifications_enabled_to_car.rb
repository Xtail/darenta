class AddNotificationsEnabledToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :is_notifications_enabled, :boolean, default: true
  end
end
