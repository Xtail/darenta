class AddCarFullNameToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :car_name, :string
    add_column :orders, :car_price, :float
    add_column :orders, :tenant_full_name, :string
    add_column :orders, :tenant_accounts, :string
    add_column :orders, :tenant_phone, :string
    add_column :orders, :discount_type, :string
    add_column :orders, :discount_value, :string
  end
end
