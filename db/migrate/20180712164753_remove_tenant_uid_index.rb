class RemoveTenantUidIndex < ActiveRecord::Migration[5.1]
  def up
    change_column :tenants, :uid, :string, :null => true, :default => ""
    remove_index :tenants, [:uid, :provider]
  end

  def down
    change_column :tenants, :uid, :string, :null => false, :default => ""
    add_index :tenants, [:uid, :provider], :unique => true
  end
end
