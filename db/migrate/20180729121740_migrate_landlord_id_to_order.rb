class MigrateLandlordIdToOrder < ActiveRecord::Migration[5.1]
  def change
    orders = Order.all

    orders.each do |order|
        if order.car_id.present?
            car = Car.find_by(id: order.car_id)
            if car.present?
                order.landlord_id = car.user_id
                order.save
            end
        end
    end
  end
end
