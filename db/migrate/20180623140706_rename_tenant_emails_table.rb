class RenameTenantEmailsTable < ActiveRecord::Migration[5.1]
  def change
    rename_table :tenant_emails, :tenant_accounts
    add_column :tenant_accounts, :uid, :string
    add_column :tenant_accounts, :tokens, :json
    add_index :tenant_accounts, :uid
  end
end
