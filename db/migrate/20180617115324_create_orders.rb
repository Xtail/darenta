class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.bigint :tenant_id, null: false
      t.bigint :car_id, null: false
      t.bigint :discount_id
      t.datetime :rent_from
      t.datetime :rent_til
      t.string :where_to_pickup
      t.string :where_to_leave
      t.float :price_value, default: 0.0
      t.string :price_currency, default: "rub"
      t.boolean :is_paid, default: false
      t.datetime :paid_at

      t.timestamps
    end

    create_table :discount_types do |t|
      t.string :title, null: false, index: true, unique: true
    end

    create_table :discounts do |t|
      t.bigint :type_id, null:false, index: true
      t.string :value
    end
  end
end
