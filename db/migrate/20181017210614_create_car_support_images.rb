class CreateCarSupportImages < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :insurance_images, :jsonb
    add_column :cars, :contract_images, :jsonb
  end
end
