class CreatePartnerOffices < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_offices do |t|
        t.string :location_id, null: false, index: true
        t.string :telephone_number
        t.time :opening_from
        t.time :opening_to
        t.string :gps_coordinates
        t.float :latitude
        t.float :longitude
    end

    PartnerOffice.create_translation_table! name: :string, address: :string, supplier_direction_to_desk: :string

    add_index :partner_office_translations, [:locale, :name], unique: true
    add_foreign_key :partner_office_translations, :partner_offices
  end
end
