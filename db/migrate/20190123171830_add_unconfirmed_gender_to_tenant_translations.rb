class AddUnconfirmedGenderToTenantTranslations < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_translations, :unconfirmed_gender, :string, default: ""
  end
end
