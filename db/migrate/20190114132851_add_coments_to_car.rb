class AddComentsToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :comments, :string, default: ""
  end
end
