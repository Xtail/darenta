class CreateAccountsForExistentTenants < ActiveRecord::Migration[5.1]
  def up
    all_tenants = Tenant.all

    all_tenants.each do |tenant|
        accounts = TenantAccount.where(tenant_id: tenant.id)
        if accounts.empty?
            account_attributes = {
                tenant_id: tenant.id,
                provider: tenant.provider,
                email: tenant.email,
                unconfirmed_email: tenant.unconfirmed_email,
                confirmed_at: tenant.confirmed_at,
                confirmation_sent_at: tenant.confirmation_sent_at,
                confirmation_token: tenant.confirmation_token,
                uid: tenant.uid,
                tokens: tenant.tokens
            }

            new_account = TenantAccount.create(account_attributes)
            new_account.save
        end
    end
  end

  def down
    all_accounts = TenantAccount.all
    all_accounts.each do |account|
        tenant = Tenant.find_by(id: account.tenant_id)
        if tenant.present?
            tenant.provider = account.provider
            tenant.email = account.email
            tenant.unconfirmed_email = account.unconfirmed_email
            tenant.confirmed_at = account.confirmed_at
            tenant.confirmation_sent_at = account.confirmation_sent_at
            tenant.confirmation_token = account.confirmation_token
            tenant.uid = account.uid
            tenant.tokens = account.tokens
            tenant.save
        end

        account.delete
    end
  end
end
