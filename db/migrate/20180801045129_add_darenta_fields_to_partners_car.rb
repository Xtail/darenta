class AddDarentaFieldsToPartnersCar < ActiveRecord::Migration[5.1]
  def change
    add_column :partner_cars, :engine_id, :bigint
    add_column :partner_cars, :transmission_id, :bigint
    add_column :partner_cars, :is_current_offer, :boolean, default: false
    add_column :partner_cars, :price_rub, :float, default: 0.0
    add_column :partner_cars, :price_usd, :float, default: 0.0
    add_column :partner_cars, :price_krw, :float, default: 0.0
    add_column :partner_cars, :price_eur, :float, default: 0.0
  end
end
