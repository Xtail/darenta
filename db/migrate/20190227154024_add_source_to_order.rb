class AddSourceToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :source, :string, null: false, default: "undefined"
  end
end
