class AddBodyIdToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :body_id, :integer
    add_index :cars, :body_id
    add_foreign_key :cars, :bodies
  end
end
