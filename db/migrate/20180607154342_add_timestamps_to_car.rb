class AddTimestampsToCar < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :cars, default: DateTime.now
    change_column_default :cars, :created_at, nil
    change_column_default :cars, :updated_at, nil
    add_index :cars, :created_at
    add_index :cars, :updated_at
  end
end
