class RemoveAddressInCars < ActiveRecord::Migration[5.1]
  def change
    remove_column :cars, :address_house
    rename_column :cars, :address_street, :address
  end
end
