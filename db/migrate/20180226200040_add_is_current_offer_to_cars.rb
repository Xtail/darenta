class AddIsCurrentOfferToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :is_current_offer, :boolean, default: false
    add_index :cars, :is_current_offer
  end
end
