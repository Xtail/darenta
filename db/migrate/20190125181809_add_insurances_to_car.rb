class AddInsurancesToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :insurances, :jsonb
  end
end
