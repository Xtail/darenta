class AddFirstAndLastNameToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :tenant_first_name, :string
    add_column :orders, :tenant_last_name, :string
  end
end
