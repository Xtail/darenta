class ReplaceUserIdToTenantIdInCars < ActiveRecord::Migration[5.1]
  def change
    rename_column :cars, :user_id, :tenant_id
  end
end
