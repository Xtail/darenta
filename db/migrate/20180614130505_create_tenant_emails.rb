class CreateTenantEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :tenant_emails do |t|
      t.bigint :tenant_id, null: false
      t.string :provider
      t.string :email
      t.string :unconfirmed_email
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string :confirmation_token

      t.timestamps
    end

    add_index :tenant_emails, [:email, :provider],  unique: true
    add_index :tenant_emails, :confirmation_token, unique: true
  end
end