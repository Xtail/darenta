class AddLinksToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :link_to_car, :string
    add_column :cars, :link_to_rental_permit, :string
    add_column :cars, :link_to_rental_ban, :string
  end
end
