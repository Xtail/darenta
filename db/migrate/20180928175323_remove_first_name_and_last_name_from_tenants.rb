class RemoveFirstNameAndLastNameFromTenants < ActiveRecord::Migration[5.1]
  def change
    remove_column :tenants, :t_last_name, :string
    remove_column :tenants, :t_first_name, :string
  end
end
