class AddLocationCodeToCity < ActiveRecord::Migration[5.1]
  def change
    add_column :cities, :location_code, :string
    add_column :cities, :latitude, :float, default: 0.0
    add_column :cities, :longitude, :float, default: 0.0
  end
end
