class AddKoreanPriceToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :price_krw, :float, default: 0.0
    add_index :cars, :price_krw
  end
end
