class CreateTransmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :transmissions do |t|
    end

    Transmission.create_translation_table! name: :string

    add_index :transmission_translations, [:locale, :name], unique: true
    add_foreign_key :transmission_translations, :transmissions
  end
end
