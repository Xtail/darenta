class AddUnconfirmedEmailToTenants < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :unconfirmed_email, :string
  end
end
