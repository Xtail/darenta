class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :mark, null: false
      t.string :model, null: false
      t.string :image
      t.integer :price, default: 0

      t.boolean :is_delivery_to_airport, null: false, default: false
      t.boolean :is_delivery_to_you, null: false, default: false
      t.boolean :is_baby_chair, null: false, default: false
      t.boolean :is_wifi, null: false, default: false
      t.boolean :is_navigator, null: false, default: false

      t.belongs_to :color, index: true
      t.belongs_to :engine, index: true
      t.belongs_to :transmission, index: true
      t.belongs_to :city, index: true
      t.belongs_to :user, index: true
    end

    add_index :cars, :is_delivery_to_airport
    add_index :cars, :is_delivery_to_you
    add_index :cars, :is_baby_chair
    add_index :cars, :is_wifi
    add_index :cars, :is_navigator
    add_index :cars, :mark
    add_index :cars, :model
    add_index :cars, :price

    add_foreign_key :cars, :colors
    add_foreign_key :cars, :engines
    add_foreign_key :cars, :transmissions
    add_foreign_key :cars, :cities
    add_foreign_key :cars, :users
  end
end
