class RemoveInsuranceNameFromCar < ActiveRecord::Migration[5.1]
  def change
    remove_column :cars, :insurance_name, :string
  end
end
