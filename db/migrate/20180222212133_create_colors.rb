class CreateColors < ActiveRecord::Migration[5.1]
  def change
    create_table :colors do |t|
    end

    Color.create_translation_table! name: :string

    add_index :color_translations, [:locale, :name], unique: true
    add_foreign_key :color_translations, :colors
  end
end
