class CreateInsurances < ActiveRecord::Migration[5.1]
  def change
    create_table :insurances do |t|
      t.bigint :car_id, null: false
      t.string :name, null: false
      t.jsonb :images
    end
  end
end
