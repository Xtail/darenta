class MigrateOrderData < ActiveRecord::Migration[5.1]
  def change
    orders = Order.all

    orders.each do |order|
        if order.car_id.present?
            car = Car.find_by(id: order.car_id)
            if car.present?
                order.car_name = car.full_name
                order.car_price = car.price_rub
            end
        end

        tenant = Tenant.find_by(id: order.tenant_id)
        if tenant.present?
            order.tenant_full_name = tenant.first_name + ' ' + tenant.last_name
            order.tenant_phone = tenant.telephone

            emails = ''
            accounts = TenantAccount.where(tenant_id: tenant.id)
            if accounts.present?
                accounts.each do |account|
                    emails += account.email + ' '
                end
            end
            order.tenant_accounts = emails
        end

        if order.discount_id.present?
            discount = Discount.find_by(id: order.discount_id)
            discount_type = DiscountType.find_by(id: discount.type_id)
            order.discount_type = discount_type.title
            order.discount_value = discount.value
        end

        order.save
    end

  end
end
