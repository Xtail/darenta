class CreateAcrissCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :acriss_codes do |t|
        t.string :acriss_code, null: false, index: true
        t.string :group
        t.string :type_of_car
        t.string :year_of_production
        t.bigint :seats
        t.bigint :doors
        t.boolean :air_condition
        t.bigint :deposit
        t.string :guaranteed
        t.string :deposit_eur
        t.bigint :cross_border_fee
    end
  end
end
