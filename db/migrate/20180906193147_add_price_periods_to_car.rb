class AddPricePeriodsToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :price_few_days_rub, :float, default: 0.0
    add_column :cars, :price_few_days_usd, :float, default: 0.0
    add_column :cars, :price_few_days_krw, :float, default: 0.0
    add_column :cars, :price_few_days_eur, :float, default: 0.0

    add_column :cars, :price_week_rub, :float, default: 0.0
    add_column :cars, :price_week_usd, :float, default: 0.0
    add_column :cars, :price_week_krw, :float, default: 0.0
    add_column :cars, :price_week_eur, :float, default: 0.0

    add_column :cars, :price_month_rub, :float, default: 0.0
    add_column :cars, :price_month_usd, :float, default: 0.0
    add_column :cars, :price_month_krw, :float, default: 0.0
    add_column :cars, :price_month_eur, :float, default: 0.0

    add_column :cars, :deposit_rub, :float, default: 0.0
    add_column :cars, :deposit_usd, :float, default: 0.0
    add_column :cars, :deposit_krw, :float, default: 0.0
    add_column :cars, :deposit_eur, :float, default: 0.0
  end
end
