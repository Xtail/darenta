class AddProductionYearToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :production_year, :integer
    add_index :cars, :production_year
  end
end
