class AddDarentaUidToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenants, :darenta_uid, :string
  end
end
