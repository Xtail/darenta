class AddPasswordToAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_accounts, :encrypted_password, :string, default: "", null: false

    add_index :tenant_accounts, [:email, :encrypted_password], unique: false
  end
end
