class UnconfirmedFieldsForTenantTranslations < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_translations, :unconfirmed_t_first_name, :string, default: ""
    add_column :tenant_translations, :unconfirmed_t_last_name, :string, default: ""
    add_column :tenants, :unconfirmed_telephone, :string, default: ""
  end
end
