class CreateEngines < ActiveRecord::Migration[5.1]
  def change
    create_table :engines do |t|
    end

    Engine.create_translation_table! name: :string

    add_index :engine_translations, [:locale, :name], unique: true
    add_foreign_key :engine_translations, :engines
  end
end
