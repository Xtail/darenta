class AddGenderToTenant < ActiveRecord::Migration[5.1]
  def change
    add_column :tenant_translations, :gender, :string, default: ""
  end
end
