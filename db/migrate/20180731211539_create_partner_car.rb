class CreatePartnerCar < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_cars do |t|
        t.string :rate_id
        t.string :acriss_code
        t.string :availability
        t.float :net_rate
        t.float :delivery
        t.float :out_of_hours
        t.float :total_cost
        t.string :currency_code
        t.string :car_name
        t.bigint :seats
        t.bigint :doors
        t.string :gearbox
        t.bigint :bags_big
        t.boolean :air_con
        t.string :wheel_drive
        t.string :fuel
        t.string :request_rate
    end
  end
end
