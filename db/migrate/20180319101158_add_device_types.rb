class AddDeviceTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :device_types do |t|
      t.string :name, null: false
    end

    add_index :device_types, :name, unique: true

    DeviceType.create(name: 'iOS')
    DeviceType.create(name: 'Android')
  end
end
