class AddInsuranceNameToCar < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :insurance_name, :string
  end
end
