class RenameAddressColumnsInCars < ActiveRecord::Migration[5.1]
  def change
    rename_column :cars, :address_latitude, :latitude
    rename_column :cars, :address_longitude, :longitude
  end
end
