class AddEmailRejectTokensForExistentTenants < ActiveRecord::Migration[5.1]
  def up
    all_tenants = Tenant.all
    all_tenants.each do |tenant|
        emails = []
        accounts = TenantAccount.where(tenant_id: tenant.id)
        accounts.each do |account|
            emails << account.email
        end
        tenant.email_reject_token = Tenant.new.generate_reject_token(emails)
        tenant.save
    end
  end

  def down
    all_tenants = Tenant.all
    all_tenants.each do |tenant|
        tenant.email_reject_token = ""
        tenant.save
    end
  end
end
