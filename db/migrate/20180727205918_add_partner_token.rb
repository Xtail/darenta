class AddPartnerToken < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :is_need_redirect, :bool, default: false
    add_column :orders, :partner_token, :string, default: ""
  end
end
