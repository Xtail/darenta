class AddIsModeratedToCars < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :is_moderated, :boolean, default: false
  end
end
