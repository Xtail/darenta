class SwapTNameAndName < ActiveRecord::Migration[5.1]
  def change
    remove_column :tenants, :first_name, :string
    remove_column :tenants, :last_name, :string

    #rename_column :tenants, :t_first_name, :first_name
    #rename_column :tenants, :t_last_name, :last_name

    #rename_column :tenant_translations, :t_first_name, :first_name
    #rename_column :tenant_translations, :t_last_name, :last_name
  end
end
