# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190409173018) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acriss_codes", force: :cascade do |t|
    t.string "acriss_code", null: false
    t.string "group"
    t.string "type_of_car"
    t.string "year_of_production"
    t.bigint "seats"
    t.bigint "doors"
    t.boolean "air_condition"
    t.bigint "deposit"
    t.string "guaranteed"
    t.string "deposit_eur"
    t.bigint "cross_border_fee"
    t.string "image"
    t.index ["acriss_code"], name: "index_acriss_codes_on_acriss_code"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
  end

  create_table "bodies", force: :cascade do |t|
  end

  create_table "body_translations", force: :cascade do |t|
    t.integer "body_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["body_id"], name: "index_body_translations_on_body_id"
    t.index ["locale", "name"], name: "index_body_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_body_translations_on_locale"
  end

  create_table "cars", force: :cascade do |t|
    t.string "image"
    t.boolean "is_delivery_to_airport", default: false, null: false
    t.boolean "is_delivery_to_you", default: false, null: false
    t.boolean "is_baby_chair", default: false, null: false
    t.boolean "is_wifi", default: false, null: false
    t.boolean "is_navigator", default: false, null: false
    t.bigint "color_id"
    t.bigint "engine_id"
    t.bigint "transmission_id"
    t.bigint "city_id"
    t.bigint "tenant_id"
    t.string "name"
    t.string "mark_name", null: false
    t.string "sampler_name", null: false
    t.string "full_name"
    t.integer "body_id"
    t.integer "production_year"
    t.boolean "is_current_offer", default: false
    t.string "address"
    t.float "latitude"
    t.float "longitude"
    t.float "price_rub", default: 0.0
    t.float "price_usd", default: 0.0
    t.string "link_to_car"
    t.float "price_krw", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "price_eur", default: 0.0
    t.float "price_few_days_rub", default: 0.0
    t.float "price_few_days_usd", default: 0.0
    t.float "price_few_days_krw", default: 0.0
    t.float "price_few_days_eur", default: 0.0
    t.float "price_week_rub", default: 0.0
    t.float "price_week_usd", default: 0.0
    t.float "price_week_krw", default: 0.0
    t.float "price_week_eur", default: 0.0
    t.float "price_month_rub", default: 0.0
    t.float "price_month_usd", default: 0.0
    t.float "price_month_krw", default: 0.0
    t.float "price_month_eur", default: 0.0
    t.float "deposit_rub", default: 0.0
    t.float "deposit_usd", default: 0.0
    t.float "deposit_krw", default: 0.0
    t.float "deposit_eur", default: 0.0
    t.string "dynamic_link"
    t.boolean "is_moderated", default: false
    t.jsonb "insurance_images"
    t.jsonb "contract_images"
    t.string "comments", default: ""
    t.jsonb "images_ids"
    t.jsonb "contract_images_ids"
    t.jsonb "insurances"
    t.boolean "is_notifications_enabled", default: true
    t.index ["body_id"], name: "index_cars_on_body_id"
    t.index ["city_id"], name: "index_cars_on_city_id"
    t.index ["color_id"], name: "index_cars_on_color_id"
    t.index ["created_at"], name: "index_cars_on_created_at"
    t.index ["engine_id"], name: "index_cars_on_engine_id"
    t.index ["full_name"], name: "index_cars_on_full_name"
    t.index ["is_baby_chair"], name: "index_cars_on_is_baby_chair"
    t.index ["is_current_offer"], name: "index_cars_on_is_current_offer"
    t.index ["is_delivery_to_airport"], name: "index_cars_on_is_delivery_to_airport"
    t.index ["is_delivery_to_you"], name: "index_cars_on_is_delivery_to_you"
    t.index ["is_navigator"], name: "index_cars_on_is_navigator"
    t.index ["is_wifi"], name: "index_cars_on_is_wifi"
    t.index ["mark_name"], name: "index_cars_on_mark_name"
    t.index ["price_eur"], name: "index_cars_on_price_eur"
    t.index ["price_krw"], name: "index_cars_on_price_krw"
    t.index ["price_rub"], name: "index_cars_on_price_rub"
    t.index ["price_usd"], name: "index_cars_on_price_usd"
    t.index ["production_year"], name: "index_cars_on_production_year"
    t.index ["sampler_name"], name: "index_cars_on_sampler_name"
    t.index ["tenant_id"], name: "index_cars_on_tenant_id"
    t.index ["transmission_id"], name: "index_cars_on_transmission_id"
    t.index ["updated_at"], name: "index_cars_on_updated_at"
  end

  create_table "cities", force: :cascade do |t|
    t.string "location_code"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
  end

  create_table "city_translations", force: :cascade do |t|
    t.integer "city_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["city_id"], name: "index_city_translations_on_city_id"
    t.index ["locale", "name"], name: "index_city_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_city_translations_on_locale"
  end

  create_table "color_translations", force: :cascade do |t|
    t.integer "color_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["color_id"], name: "index_color_translations_on_color_id"
    t.index ["locale", "name"], name: "index_color_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_color_translations_on_locale"
  end

  create_table "colors", force: :cascade do |t|
  end

  create_table "currencies", force: :cascade do |t|
    t.string "code", null: false
    t.float "nominal"
    t.float "value_in_rub"
    t.datetime "created_at", default: "2019-02-01 19:24:19", null: false
    t.datetime "updated_at", default: "2019-02-01 19:24:19", null: false
    t.index ["code"], name: "index_currencies_on_code", unique: true
  end

  create_table "currency_translations", force: :cascade do |t|
    t.integer "currency_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["currency_id"], name: "index_currency_translations_on_currency_id"
    t.index ["locale", "name"], name: "index_currency_translations_on_locale_and_name"
    t.index ["locale"], name: "index_currency_translations_on_locale"
  end

  create_table "device_types", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_device_types_on_name", unique: true
  end

  create_table "discount_types", force: :cascade do |t|
    t.string "title", null: false
    t.index ["title"], name: "index_discount_types_on_title"
  end

  create_table "discounts", force: :cascade do |t|
    t.bigint "type_id", null: false
    t.string "value"
    t.index ["type_id"], name: "index_discounts_on_type_id"
  end

  create_table "engine_translations", force: :cascade do |t|
    t.integer "engine_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["engine_id"], name: "index_engine_translations_on_engine_id"
    t.index ["locale", "name"], name: "index_engine_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_engine_translations_on_locale"
  end

  create_table "engines", force: :cascade do |t|
  end

  create_table "mail_messages", force: :cascade do |t|
    t.string "key", null: false
    t.string "title", null: false
    t.jsonb "message_title"
    t.jsonb "message_body"
    t.jsonb "message_footer"
    t.index ["key"], name: "index_mail_messages_on_key", unique: true
  end

  create_table "marks", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_marks_on_name", unique: true
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.bigint "car_id"
    t.bigint "discount_id"
    t.datetime "rent_from"
    t.datetime "rent_til"
    t.string "where_to_pickup"
    t.string "where_to_leave"
    t.float "price_value", default: 0.0
    t.string "price_currency", default: "rub"
    t.boolean "is_paid", default: false
    t.datetime "paid_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_accepted", default: false
    t.boolean "is_declined", default: false
    t.string "accept_token", default: ""
    t.string "decline_token", default: ""
    t.boolean "is_need_redirect", default: false
    t.string "partner_token", default: ""
    t.string "car_name"
    t.float "car_price"
    t.string "tenant_full_name"
    t.string "tenant_accounts"
    t.string "tenant_phone"
    t.string "discount_type"
    t.string "discount_value"
    t.bigint "landlord_id"
    t.string "locale", default: "ru"
    t.string "tenant_first_name"
    t.string "tenant_last_name"
    t.string "source", default: "undefined", null: false
  end

  create_table "partner_cars", force: :cascade do |t|
    t.string "rate_id"
    t.string "acriss_code"
    t.string "availability"
    t.float "net_rate"
    t.float "delivery"
    t.float "out_of_hours"
    t.float "total_cost"
    t.string "currency_code"
    t.string "car_name"
    t.bigint "seats"
    t.bigint "doors"
    t.string "gearbox"
    t.bigint "bags_big"
    t.boolean "air_con"
    t.string "wheel_drive"
    t.string "fuel"
    t.string "request_rate"
    t.bigint "engine_id"
    t.bigint "transmission_id"
    t.boolean "is_current_offer", default: false
    t.float "price_rub", default: 0.0
    t.float "price_usd", default: 0.0
    t.float "price_krw", default: 0.0
    t.float "price_eur", default: 0.0
    t.string "location_code"
  end

  create_table "partner_office_translations", force: :cascade do |t|
    t.integer "partner_office_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "address"
    t.string "supplier_direction_to_desk"
    t.index ["locale", "name"], name: "index_partner_office_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_partner_office_translations_on_locale"
    t.index ["partner_office_id"], name: "index_partner_office_translations_on_partner_office_id"
  end

  create_table "partner_offices", force: :cascade do |t|
    t.string "location_id", null: false
    t.string "telephone_number"
    t.time "opening_from"
    t.time "opening_to"
    t.string "gps_coordinates"
    t.float "latitude"
    t.float "longitude"
    t.index ["location_id"], name: "index_partner_offices_on_location_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "file"
  end

  create_table "samplers", force: :cascade do |t|
    t.bigint "mark_id", null: false
    t.string "name", null: false
    t.index ["mark_id", "name"], name: "index_samplers_on_mark_id_and_name", unique: true
    t.index ["mark_id"], name: "index_samplers_on_mark_id"
  end

  create_table "tenant_accounts", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "provider"
    t.string "email"
    t.string "unconfirmed_email"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "confirmation_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uid"
    t.json "tokens"
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "reset_password_hash"
    t.string "confirmation_sent_from", default: "site"
    t.datetime "last_confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_tenant_accounts_on_confirmation_token", unique: true
    t.index ["email", "encrypted_password"], name: "index_tenant_accounts_on_email_and_encrypted_password"
    t.index ["email", "provider"], name: "index_tenant_accounts_on_email_and_provider", unique: true
    t.index ["uid"], name: "index_tenant_accounts_on_uid"
  end

  create_table "tenant_translations", force: :cascade do |t|
    t.integer "tenant_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "t_first_name"
    t.string "t_last_name"
    t.string "unconfirmed_t_first_name", default: ""
    t.string "unconfirmed_t_last_name", default: ""
    t.string "gender", default: ""
    t.string "unconfirmed_gender", default: ""
    t.string "t_middle_name"
    t.string "unconfirmed_t_middle_name", default: ""
    t.index ["locale"], name: "index_tenant_translations_on_locale"
    t.index ["tenant_id"], name: "index_tenant_translations_on_tenant_id"
  end

  create_table "tenants", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: ""
    t.string "encrypted_password", default: "", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "telephone"
    t.string "avatar"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "unconfirmed_email"
    t.boolean "is_blocked", default: false
    t.boolean "is_email_rejected", default: false
    t.string "email_reject_token", default: ""
    t.string "locale", default: "ru"
    t.string "unconfirmed_telephone", default: ""
    t.string "darenta_uid"
    t.index ["confirmation_token"], name: "index_tenants_on_confirmation_token", unique: true
    t.index ["email"], name: "index_tenants_on_email", unique: true
  end

  create_table "transmission_translations", force: :cascade do |t|
    t.integer "transmission_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["locale", "name"], name: "index_transmission_translations_on_locale_and_name", unique: true
    t.index ["locale"], name: "index_transmission_translations_on_locale"
    t.index ["transmission_id"], name: "index_transmission_translations_on_transmission_id"
  end

  create_table "transmissions", force: :cascade do |t|
  end

  create_table "user_translations", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "middle_name"
    t.string "short_name"
    t.string "full_name"
    t.index ["full_name"], name: "index_user_translations_on_full_name"
    t.index ["locale"], name: "index_user_translations_on_locale"
    t.index ["short_name"], name: "index_user_translations_on_short_name"
    t.index ["user_id"], name: "index_user_translations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "avatar"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "body_translations", "bodies"
  add_foreign_key "cars", "bodies"
  add_foreign_key "cars", "cities"
  add_foreign_key "cars", "colors"
  add_foreign_key "cars", "engines"
  add_foreign_key "cars", "tenants"
  add_foreign_key "cars", "transmissions"
  add_foreign_key "city_translations", "cities"
  add_foreign_key "color_translations", "colors"
  add_foreign_key "engine_translations", "engines"
  add_foreign_key "partner_office_translations", "partner_offices"
  add_foreign_key "transmission_translations", "transmissions"
  add_foreign_key "user_translations", "users"
end
