Admin.create(email: 'admin@darenta.ru', password: '12345678')

City.create(name_ru: 'Москва', name_en: 'Moscow', name_ko: '모스크바')
City.create(name_ru: 'Санкт-Петербург', name_en: 'St. Petersburg', name_ko: '상트 페테르부르크')

Color.create(name_ru: 'Черный', name_en: 'Black', name_ko: '블랙')
Color.create(name_ru: 'Красный', name_en: 'Red', name_ko: '빨간색')
Color.create(name_ru: 'Желтый', name_en: 'Yellow', name_ko: '옐로우')
Color.create(name_ru: 'Белый', name_en: 'White', name_ko: '화이트')

Transmission.create(name_ru: 'АКПП', name_en: 'Automatic', name_ko: '오토매틱')
Transmission.create(name_ru: 'Механическая', name_en: 'Mechanics', name_ko: '기계')

Engine.create(name_ru: 'Бензиновый', name_en: 'Petrol', name_ko: '가솔린')
Engine.create(name_ru: 'Дизельный', name_en: 'Diesel', name_ko: '디젤 엔진')

Body.create(name_ru: 'Седан', name_en: 'Sedan', name_ko: '세단 형 자동차')
Body.create(name_ru: 'Хэтчбек', name_en: 'Hatchback', name_ko: '해치백')

#User.create(email: 'test1@example.com',
#            first_name_ru: 'Иван',
#            middle_name_ru: 'Иванович',
#            last_name_ru: 'Иванов',
#            first_name_en: 'Ivan',
#            middle_name_en: 'Ivanovich',
#            last_name_en: 'Ivanov',
#            first_name_ko: '이반',
#            middle_name_ko: '이바노 비치',
#            last_name_ko: '이바노프')

#User.create(email: 'test2@example.com',
#            first_name_ru: 'Кирилл',
#            middle_name_ru: 'Кириллович',
#            last_name_ru: 'Кириллов',
#            first_name_en: 'Kirill',
#            middle_name_en: 'Kirillovich',
#            last_name_en: 'Kirillov',
#            first_name_ko: '시릴',
#            middle_name_ko: '키릴로 비치',
#            last_name_ko: '키릴 로프')

#Car.create(mark_name: 'Kia', sampler_name: 'Rio', production_year: 2008, price_rub: 1500, price_usd: 2700, price_krw: 1000,
#           city_id: City.first.id,
#           engine_id: Engine.first.id,
#           body_id: Body.first.id,
#           transmission_id: Transmission.first.id,
#           color_id: Color.first.id,
#           user_id: User.first.id,
#           is_delivery_to_airport: true,
#           is_wifi: true,
#           is_navigator: true,
#           address: 'Ленина')

#Car.create(mark_name: 'Kia', sampler_name: 'Optima', production_year: 2012, price_rub: 2700, price_usd: 2700, price_krw: 1000,
#           city_id: City.first.id,
#           engine_id: Engine.first.id,
#           body_id: Body.first.id,
#           transmission_id: Transmission.first.id,
#           color_id: Color.last.id,
#           user_id: User.last.id,
#           is_wifi: true,
#           is_navigator: true,
#           address: 'Ленина')

MailMessage.create(key: 'order_mailer.notify_tenant',
                   title: 'Заказ. Сообщение для арендатора.',
                   message_title: '{
                                    "block0": {
                                                "ru": "Здравствуйте! Вы сделали запрос на аренду автомобиля",
                                                 "en": "Hello! You made a request for a car\'s rental",
                                                 "ko": ""
                                              }
                                   }',
                   message_body: '{
                                    "block0" : {
                                                  "ru": "Если у вас есть вопросы по сделке или сервису, то просто напишите в ответ на данное письмо.",
                                                  "en": "If you have questions about the transaction or service, then just write in response to this letter.",
                                                  "ko": ""
                                               }
                                  }',
                   message_footer: '{
                                     "block0" : {
                                                  "ru": "С уважением, команда каршеринга "Darenta",
                                                  "en": "Yours faithfully, team of Darenta",
                                                  "ko": ""
                                                }
                                    }')

MailMessage.create(key: 'order_mailer.notify_landlord',
                   title: 'Заказ. Сообщение для автовладельца.',
                   message_title: '{
                                    "block0" : {
                                                "ru": "Здравствуйте!",
                                                "en": "Hello!",
                                                "ko": ""
                                               }
                                   }',
                   message_body: '{
                                    "block0": {
                                                "ru": "Пользователь-арендатор хочет арендовать ваш автомобиль",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block1": {
                                                "ru": "с",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block2": {
                                                "ru": "по",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block3": {
                                                "ru": "Сумма сделки составит",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block4": {
                                                "ru": "рублей. Эти средства вы получите на встрече с арендатором. Мы рекомендуем вам составить договор с арендатором и распечатать в двух экземплярах, чтобы подписать с арендатором на встрече.
                                          Контакты данного арендатора вы получите бесплатно, то есть, мы не берем с вас комиссию. Комиссию нам заплатит арендатор. Вы должны получить ту сумму, которую указывали в анкете автомобиля.<br><br>
                                          Для подтверждения аренды, вам нужно в течение часа ответным письмом связаться с нами. В противном случае, аренда будет аннулирована. <br><br>
                                          Если у вас есть вопросы по сделке или сервису, то просто напишите в ответ на данное письмо.",
                                                "en": "",
                                                "ko": ""
                                              }
                                  }',
                   message_footer: '{
                                      "block0": {
                                                "ru": "С уважением, команда каршеринга Darenta",
                                                "en": "Yours faithfully, team of Darenta",
                                                "ko": ""
                                              }
                                    }')

MailMessage.create(key: 'order_mailer.notify_administrator',
                   title: 'Заказ. Сообщение для администратора.',
                   message_title: '{
                                    "block0": {
                                                "ru": "Новая заявка.",
                                                "en": "",
                                                "ko": ""
                                              }
                                   }',
                   message_body: '{
                                    "block0": {
                                                "ru": "Aрендатор",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block1": {
                                                "ru": " отправил запрос на аренду автовладельцу",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block2": {
                                                "ru": "Ссылка на автомобиль",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block3": {
                                                "ru": "Время аренды с ",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block4": {
                                                "ru": "по",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block5": {
                                                "ru": "Сумма сделки составит",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block6": {
                                                "ru": "рублей."
                                                "en": "",
                                                "ko": ""
                                              }
                                  }',
                   message_footer: '{
                                      "block0": {
                                                "ru": "С уважением, команда каршеринга Darenta",
                                                "en": "Yours faithfully, team of Darenta",
                                                "ko": ""
                                              }
                                    }')
MailMessage.create(key: 'feedback_mailer.notify',
                   title: 'Обратная связь',
                   message_title: '{
                                    "block0": {
                                                "ru": "Вам было отправлено сообщение через форму обратной связи",
                                                "en": "",
                                                "ko": ""
                                              }
                                   }',
                   message_body: '{
                                    "block0": {
                                                "ru": "Автор:",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block1": {
                                                "ru": "E-mail автора:",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block2": {
                                                "ru": "Тип устройства:",
                                                "en": "",
                                                "ko": ""
                                              },
                                    "block3": {
                                                "ru": "Сообщение:",
                                                "en": "",
                                                "ko": ""
                                              }
                                  }',
                   message_footer: '{
                                      "block0": {
                                                  "ru": "С уважением, команда каршеринга Darenta",
                                                  "en": "Yours faithfully, team of Darenta",
                                                  "ko": ""
                                                }
                                    }')