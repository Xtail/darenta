#!/bin/bash
cd /myapp
rm tmp/pids/server.pid || true
wait-for-it -t 0 postgres:5432
wait-for-it -t 0 redis:6379
bundle exec foreman start
