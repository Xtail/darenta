Rails.application.routes.draw do
  
  devise_for :admins
  devise_for :tenant_accounts,
             :sign_out_via => [ :delete, :get ],
             controllers: {
                 omniauth_callbacks: 'omniauth_callbacks',
                 confirmations: 'confirmations'
             }

  get '/', to: 'cars#index'
  resources :cars

  put 'tenants/update', to: 'tenants#update'
  get 'resend_confirmation', to: 'tenants#resend_confirmation'
  get 'renters/reject_email', to: 'tenants#reject_email'

  post 'create_order', to: 'orders#create_order'
  get 'orders/accept', to: 'orders#accept'
  get 'orders/decline', to: 'orders#decline'

  get 'password_reset_form', to: 'tenant_accounts#password_reset_form'
  post 'password_reset', to: 'tenant_accounts#password_reset'
  get 'password_reset_successful', to: 'tenant_accounts#password_reset_successful'
  get 'password_reset_error', to: 'tenant_accounts#password_reset_error'

  #get 'tests/xml', to: 'tests#test_xml'
  #get 'tests/emails', to: 'tests#test_email_views'

  namespace :admin do
    root to: redirect('/admin/cars')

    resources :colors
    resources :engines
    resources :transmissions
    resources :bodies
    resources :cities
    resources :users
    resources :tenants, path: :renters
    resources :cars
    resources :mail_messages
    resources :orders
    resources :discounts
    resources :discount_types
    resources :tenant_accounts
    resources :acriss_codes
    resources :partner_offices
    resources :marks
    resources :samplers
    resources :currencies

    post 'partners/switch', to: 'partners#partners_switch_status'
    post 'tenants/merge', to: 'tenants#merge_tenants'
    get 'renters_merge_form', to: 'tenants#tenants_merge_form'

    namespace :datatables do
      resources :users,  only: %i[index]
      resources :tenants, path: :renters,  only: %i[index]
      resources :cars,   only: %i[index]
      resources :mail_messages, only: %i[index]
      resources :orders, only: %i[index]
      resources :discounts, only: %i[index]
      resources :discount_types, only: %i[index]
      resources :tenant_accounts, only: %i[index]
      resources :acriss_codes, only: %i[index]
      resources :partner_offices, only: %i[index]
      resources :marks, only: %i[index]
      resources :samplers, only: %i[index]
    end

    post 'dictionaries/update', to: 'dictionaries#update'
    get 'dictionaries/load', to: 'dictionaries#load'

    post 'currencies/update_all_currencies', to: 'currencies#update_all_currencies'
    get 'rules_for_robots', to: 'utils#robots'
  end

  namespace :api do
    namespace :v1 do
      post 'wallet/pkpass', to: 'wallet#generate_pkpass'

      mount_devise_token_auth_for 'TenantAccount', at: 'auth', skip: [:omniauth_callbacks]

      resources :cars, only: %i[index show]
      resources :bodies, only: %i[index]
      resources :engines, only: %i[index]
      resources :transmissions, only: %i[index]
      resources :colors, only: %i[index]
      resources :cities, only: %i[index]
      resources :feedback, only: %i[create]
      post 'feedback/create_alt', to: 'feedback#create_alt'

      resources :orders, only: %i[create]
      post 'orders/pay', to: 'orders#pay'
      post 'orders/create_car_request', to: 'orders#create_car_request'
      post 'orders/create_partner_car_request', to: 'orders#create_partner_car_request'
      get 'orders/my', to: 'orders#my'
      resources :tenants, only: %i[] do
        collection do
          get  :me
          put  :me
          post :oauth
          post :auth
          
          get :my_cars
          post :create_car
          post :create_car_2
          put :update_car
          put :update_car_2
          delete :remove_car
        end
      end
      post 'tenants/resend_confirmation', to: 'tenants#resend_confirmation'
      post 'tenants/add_push_token', to: 'tenants#add_push_token'
      post 'tenants/update_password', to: 'tenants#update_password'
      post 'tenants/reset_password', to: 'tenants#reset_password'
      resources :marks, only: %i[index]
      resources :samplers, only: %i[index]
      post 'load_file', to: 'resources#load_file'
    end
  end
end
