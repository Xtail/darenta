server '212.109.223.166', user: 'deploy', roles: %w[web app]

role :app, %w[deploy@212.109.223.166]
role :web, %w[deploy@212.109.223.166]
role :db,  %w[deploy@212.109.223.166]

set :branch, 'master'
set :deploy_to, '/home/deploy/api.darenta.ru'
