redis_url = Rails.env.production? ? 'redis://localhost:6379/12' : ENV['JOB_WORKER_URL']
sidekiq_config = { url: redis_url }

Sidekiq.configure_server do |config|
  config.redis = sidekiq_config
end

Sidekiq.configure_client do |config|
  config.redis = sidekiq_config
end

#Sidekiq.configure_server do |config|
#  config.server_middleware do |chain|
#    chain.add Middleware::Server::RetryJobs, :max_retries => 7
#  end
#end