# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.

Rails.application.config.assets.precompile += %w( admin.js admin.css )
Rails.application.config.assets.precompile += %w( pages/main.js pages/slideout.min.js pages/html5shiv.js pages/jquery.magnific-popup.min.js pages/jquery-3.3.1.min.js pages/datepicker.min.js pages/date.js)
Rails.application.config.assets.precompile += %w( pages/main.css.erb pages/normalize.css pages/magnific-popup.css pages/datepicker.min.css )
Rails.application.config.assets.precompile += %w( pages/main.css )