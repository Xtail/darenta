# config valid for current version and patch releases of Capistrano
lock '~> 3.10.1'

set :application, 'darenta'

set :repo_url, 'git@gitlab.it-machine.ru:darenta/Darenta-Rails.git'

# puma
set :puma_preload_app, true
set :puma_init_active_record, true

# Default value for :linked_files is []
append :linked_files, 'config/database.yml', 'config/secrets.yml', 'config/sidekiq.yml'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'public/uploads'

# Sidekiq config file
set :sidekiq_config, -> { File.join(shared_path, 'config', 'sidekiq.yml') }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
end
