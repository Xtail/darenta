source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.5'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'

gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'

gem 'activemodel-serializers-xml'
gem 'globalize', github: 'globalize/globalize'
gem 'globalize-accessors'
gem 'globalize3_helpers', github: 'amprsnd/globalize3_helpers'

gem 'ajax-datatables-rails'
gem 'jquery-datatables-rails'

gem 'jquery-rails'
gem 'kaminari'
gem 'sprockets-es6'
gem 'bootstrap_form'
gem 'font-awesome-rails'
gem 'carrierwave', '~> 1.0'
gem 'devise'
gem 'devise-i18n'
gem 'devise-async'
gem 'devise_token_auth'
gem 'mini_magick'
gem 'dry-transaction'
gem 'dry-validation'
gem 'dry-types'
gem 'dry-struct'
gem 'draper'
gem 'jbuilder'
gem 'yajl-ruby', require: 'yajl'
gem 'geocoder'
gem 'sidekiq'
gem 'httparty'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'foreman'
end

group :development, :gitlab_ci do
  # Deployment tools
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-rvm'
  gem 'capistrano-db-tasks', '0.4', require: false
  gem 'capistrano-faster-assets', '~> 1.0'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq', github: 'seuros/capistrano-sidekiq'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'passbook'